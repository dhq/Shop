<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageHeader">
    <form id="pagerForm" method="post" action="Main/BrandTypeStatistics/main" onsubmit="return navTabSearch(this);">
        <div class="searchBar">
            <table class="searchContent">
                <tr>
                    <td>年份：</td>
                    <td>
                        <input type="text" id="year" name="year" class="required date" style="width:50px;" minlength="4" value="${year}" datefmt="yyyy"> </input>
                        <select name="type" style="width:90px;">
                            <option value="0" <c:if test="${type == 0 }">selected</c:if>>全年</option>
                            <option value="13" <c:if test="${type == 13 }">selected</c:if>>第一季度</option>
                            <option value="14" <c:if test="${type == 14 }">selected</c:if>>第二季度</option>
                            <option value="15" <c:if test="${type == 15 }">selected</c:if>>第三季度</option>
                            <option value="16" <c:if test="${type == 16 }">selected</c:if>>第四季度</option>
                            <option value="1" <c:if test="${type== 1 }">selected</c:if>> 01月</option>
                            <option value="2" <c:if test="${type== 2 }">selected</c:if>> 02月</option>
                            <option value="3" <c:if test="${type== 3 }">selected</c:if>> 03月</option>
                            <option value="4" <c:if test="${type== 4 }">selected</c:if>> 04月</option>
                            <option value="5" <c:if test="${type== 5 }">selected</c:if>> 05月</option>
                            <option value="6" <c:if test="${type== 6 }">selected</c:if>> 06月</option>
                            <option value="7" <c:if test="${type== 7 }">selected</c:if>> 07月</option>
                            <option value="8" <c:if test="${type== 8 }">selected</c:if>> 08月</option>
                            <option value="9" <c:if test="${type== 9 }">selected</c:if>> 09月</option>
                            <option value="10" <c:if test="${type== 10 }">selected</c:if>> 10月</option>
                            <option value="11" <c:if test="${type== 11 }">selected</c:if>> 11月</option>
                            <option value="12" <c:if test="${type== 12 }">selected</c:if>> 12月</option>
                        </select>
                    </td>
                    <td>商品品牌：</td>
                    <td>
                        <select name="brandId" class="combox">
                            <option value="">-请选择-</option>
                            <c:forEach items="${brandList }" var="t">
                                <option value="${t.id }" <c:if test="${t.id == brandId }">selected</c:if>>${t.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>商品类型：</td>
                    <td>
                        <select name="goodsType" class="combox">
                            <option value="">-请选择-</option>
                            <c:forEach items="${goodsTypeList }" var="t">
                                <option value="${t.id }" <c:if test="${t.id == goodsType }">selected</c:if>>${t.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><div class="buttonActive"><div class="buttonContent"><button type="submit">查询</button></div></div></td>
                </tr>
            </table>
        </div>
    </form>
</div>
<div class="pageContent">
    <div class="panelBar">
        <ul class="toolBar">
            <%--<form method="post" id="exportForm" action="Main/LoadingUnLoading/exportLoadingUnLoading" onsubmit="return validateCallback(this, xxxAjaxDone)">
                <input type="hidden" name="year" value="${year}" />
                <input type="hidden" name="type" value="${type}" />
                <button id="exportLoadingUnLoading"  style="display: none" type="submit">导出excel</button>
            </form>
          <c:if test="${pert:hasperti(applicationScope.exportLoadingUnLoading, loginModel.limit)}">
                <li><a class="excel" href = "javascript:exportSubmit('exportLoadingUnLoading')" title="导出报表"><span>导出报表</span></a></li>
            </c:if>--%>
            <li class="line">line</li>
        </ul>
    </div>
    <table class="table" width="100%" layoutH="112">
        <thead>
        <tr align="center">
            <th>月份</th>
            <th>总销售额</th>
            <th>总原价</th>
            <th>总销量</th>
        </tr>
        </thead>
        <tbody>
        <c:choose>
            <c:when test="${! empty list}">
                <c:forEach items="${list}" var="o">
                    <tr align="center">
                        <td>${o.month}</td>
                        <td>${o.totalSales}</td>
                        <td>${o.totalPrice}</td>
                        <td>${o.total}</td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr align="center">
                    <td colspan="4" style="color: red;">无任何记录！</td>
                </tr>
            </c:otherwise>
        </c:choose>
        </tbody>
    </table>
</div>
