<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<script type="text/javascript">
//    $(function(){
//        <!--控制分类下拉框-->
//        $("#classify-opt").change(function(){
//            var classify_val = $(this).children('option:selected').val();
//            switch (classify_val) {
//                case "BY_BRAND":
//                    $(".select-brand").show();
//                    $(".select-seller").hide();
//                    break;
//                case "BY_SELLER":
//                    $(".select-brand").hide();
//                    $(".select-seller").show();
//                    break;
//                default:
//                    break;
//            }
//        });
//    });
</script>

<div class="pageHeader">
    <form id="pagerForm" method="post" action="Main/BrandTypeStatistics/main" onsubmit="return navTabSearch(this);">
        <div class="searchBar">
            <table class="searchContent">
                <tr>

                    <td>起始日期：</td>
                    <td>
                        <input type="text" id="selectStart" name="selectStart" class="required date" style="width:80px;" minlength="4" value="${selectStart}" datefmt="yyyy-MM-dd"> </input>
                    </td>
                    <td>终止日期：</td>
                    <td>
                        <input type="text" id="selectEnd" name="selectEnd" class="required date" style="width:80px;" minlength="4" value="${selectEnd}" datefmt="yyyy-MM-dd"> </input>
                    </td>

            </table>
        </div>
    </form>
</div>
<div class="pageContent">
    <div class="panelBar">
        <ul class="toolBar">
            <li class="line">line</li>
        </ul>
    </div>
    <table class="table" width="100%" layoutH="112">


        <c:choose>
            <c:when test="${empty list}">
                <tr align="center">
                    <td colspan="4" style="color: red;">无任何记录！</td>
                </tr>

            </c:when>
            <c:when test="${classifyOpt.equals('BY_BRAND')}">
                <thead>
                <tr align="center">
                    <th>品牌名</th>
                    <th>总销售额</th>
                    <th>总提成</th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach items="${list}" var="o">
                        <tr align="center">
                            <td>${o.get("brand_name")}</td>
                            <td>${o.get("total_sales")}</td>
                            <td>${o.get("total_commission")}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </c:when>
            <c:when test="${classifyOpt.equals('BY_SELLER')}">
                <thead>
                <tr align="center">
                    <th>销售员</th>
                    <th>总销售额</th>
                    <th>总提成</th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach items="${list}" var="o">
                        <tr align="center">
                            <td>${o.get("salesman_name")}</td>
                            <td>${o.get("total_sales")}</td>
                            <td>${o.get("total_commission")}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </c:when>
            <c:otherwise>
                <tr align="center">
                    <td colspan="4" style="color: red;">传值异常！</td>
                </tr>
            </c:otherwise>
        </c:choose>


    </table>
</div>
