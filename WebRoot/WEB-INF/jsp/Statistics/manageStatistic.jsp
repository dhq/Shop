<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<script type="text/javascript">
    $(function(){
        <!--控制分类下拉框-->
        $("#classify-opt").change(function(){
            var classify_val = $(this).children('option:selected').val();
            switch (classify_val) {
                case "BY_BRAND":
                    $(".s-child").hide();
                    $(".s-brand").show();
                    break;
                case "BY_SELLER":
                    $(".s-child").hide();
                    $(".s-seller").show();
                    break;
                case "BY_MONTH_SALES":
                    $(".s-child").hide();
                    $(".s-m-sales").show();
                    break;
                default:
                    break;
            }
        });
    });
</script>

<div class="pageHeader">
    <form id="pagerForm" method="post" action="Main/BrandTypeStatistics/main" onsubmit="return navTabSearch(this);">
        <div class="searchBar">
            <table class="searchContent">
                <tr>
                    <td>起始日期：</td>
                    <td>
                        <input type="text" id="selectStart" name="selectStart" class="required date" style="width:80px;" minlength="4" value="${selectStart}" datefmt="yyyy-MM-dd"> </input>
                    </td>
                    <td>终止日期：</td>
                    <td>
                        <input type="text" id="selectEnd" name="selectEnd" class="required date" style="width:80px;" minlength="4" value="${selectEnd}" datefmt="yyyy-MM-dd"> </input>
                    </td>

                    <td>分类方式：</td>
                    <td>
                        <select id="classify-opt" name="classifyOpt" class="combox">
                            <option value="BY_BRAND"  <c:if test="${classifyOpt.equals('BY_BRAND')}">selected</c:if>>按品牌</option>
                            <option value="BY_SELLER" <c:if test="${classifyOpt.equals('BY_SELLER')}">selected</c:if>>按销售员</option>
                            <option value="BY_MONTH_SALES" <c:if test="${classifyOpt.equals('BY_MONTH_SALES')}">selected</c:if>>按每月的总销售额</option>
                        </select>
                    </td>


                    <td class="s-brand s-child" <c:if test="${!classifyOpt.equals('BY_BRAND')}">style="display: none;"</c:if>>商品品牌：</td>
                    <td class="s-brand s-child" <c:if test="${!classifyOpt.equals('BY_BRAND')}">style="display: none;"</c:if>>
                    <select name="brandNumber" class="combox">
                        <option value="">-请选择-</option>
                        <c:forEach items="${brandList}" var="b">
                            <option value="${b.number}" <c:if test="${b.number == brandNumber}">selected</c:if>>${b.name}</option>
                        </c:forEach>
                    </select>
                    </td>

                    <td class="s-seller s-child" <c:if test="${!classifyOpt.equals('BY_SELLER')}">style="display: none;"</c:if>>销售员：</td>
                    <td class="s-seller s-child" <c:if test="${!classifyOpt.equals('BY_SELLER')}">style="display: none;"</c:if>>
                    <select name="sellerNumber" class="combox">
                        <option value="">-请选择-</option>
                        <c:forEach items="${sellerList}" var="t">
                            <option value="${t.staff_id}" <c:if test="${t.staff_id == sellerNumber}">selected</c:if>>${t.name}</option>
                        </c:forEach>
                    </select>
                    </td>

                    <td class="s-m-sales s-child" <c:if test="${!classifyOpt.equals('BY_MONTH_SALES')}">style="display: none;"</c:if>>统计年份：</td>
                    <td class="s-m-sales s-child" <c:if test="${!classifyOpt.equals('BY_MONTH_SALES')}">style="display: none;"</c:if>>
                        <input type="text" id="selectYear" name="selectYear" class="required date" style="width:40px;" minlength="4" value="${selectYear}" datefmt="yyyy"> </input>
                    </td>

                    <td><div class="buttonActive"><div class="buttonContent"><button type="submit">查询</button></div></div></td>
                </tr>
            </table>
        </div>
    </form>
</div>
<div class="pageContent">
    <div class="panelBar">
        <ul class="toolBar">
            <li class="line">line</li>
        </ul>
    </div>
    <table class="table" width="100%" layoutH="112">


        <c:choose>
            <c:when test="${empty list}">
                <tr align="center">
                    <td colspan="4" style="color: red;">无任何记录！</td>
                </tr>

            </c:when>
            <c:when test="${classifyOpt.equals('BY_BRAND')}">
                <thead>
                <tr align="center">
                    <th>品牌名</th>
                    <th>总销售额</th>
                    <th>总销售量</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${list}" var="o">
                    <tr align="center">
                        <td>${o.get("brand_name")}</td>
                        <td>${o.get("total_sales")}</td>
                        <td>${o.get("total_sales_volume")}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </c:when>
            <c:when test="${classifyOpt.equals('BY_SELLER')}">
                <thead>
                <tr align="center">
                    <th>销售员</th>
                    <th>总销售额</th>
                    <th>总提成</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${list}" var="o">
                    <tr align="center">
                        <td>${o.get("salesman_name")}</td>
                        <td>${o.get("total_sales")}</td>
                        <td>${o.get("total_commission")}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </c:when>
            <c:when test="${classifyOpt.equals('BY_MONTH_SALES')}">
                <thead>
                <tr align="center">
                    <th>月份</th>
                    <th>总销售额</th>
                    <th>总提成</th>
                    <th>总销售量</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${list}" var="o">
                    <tr align="center">
                        <td>${o.get("month_idx")}月</td>
                        <td>${o.get("total_sales")}</td>
                        <td>${o.get("total_commission")}</td>
                        <td>${o.get("total_sales_volume")}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </c:when>
            <c:otherwise>
                <tr align="center">
                    <td colspan="4" style="color: red;">传值异常！</td>
                </tr>
            </c:otherwise>
        </c:choose>


    </table>
</div>
