<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<form id="pagerForm" method="post" action="Main/Brand/main">
	<input type="hidden" name="pageNum" value="${page.pageNumber}" />
	<input type="hidden" name="numPerPage" value="${page.pageSize}" />
</form>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<c:if test="${pert:hasperti(applicationScope.addBrand, loginModel.limit)}">
				<li><a class="add" href="Main/Brand/addip" target="dialog" mask="true" drawable="true" resizable="false" maxable="false"
                       rel="addBrand" title="添加品牌" width="400" height="230"><span>添加</span></a></li>
			</c:if>
            <c:if test="${pert:hasperti(applicationScope.updateBrand, loginModel.limit)}">
                <li><a class="edit" href="Main/Brand/updateip/{id}" target="dialog" mask="true" drawable="true" resizable="false"
                       maxable="false" rel="updateBrand" title="修改品牌" width="400" height="230"><span>修改</span></a></li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.deleteBrand, loginModel.limit)}">
                <li><a class="delete" href="Main/Brand/delete/{id}" target="ajaxTodo" title="您确定要删除该记录吗?"><span>删除</span></a></li>
            </c:if>
            <li class="line">line</li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="75">
		<thead>
			<tr align="center">
                <th width="27px">序号</th>
				<th>品牌名称</th>
				<th>品牌编号</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty page.list}">
					<c:forEach items="${page.list}" var="o" varStatus="s">
						<tr align="center" target="id" rel="${o.id}">
                            <td>${s.count}</td>
                            <td style="text-align: left;">${o.name}</td>
							<td>${o.number}</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr align="center">
						<td colspan="4" style="color: red;">无任何记录！</td>
					</tr>	
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option <c:if test="${page.pageSize==20}">selected="selected"</c:if> value="20" >20</option>
				<option <c:if test="${page.pageSize==50}">selected="selected"</c:if> value="50">50</option>
				<option <c:if test="${page.pageSize==100}">selected="selected"</c:if> value="100">100</option>
				<option value="200" <c:if test="${page.pageSize==200}">selected="selected"</c:if>>200</option>
			</select>
			<span>条，共${page.totalRow}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${page.totalRow}" numPerPage="${page.pageSize}" pageNumShown="5" currentPage="${page.pageNumber}"></div>
	</div>
</div>
