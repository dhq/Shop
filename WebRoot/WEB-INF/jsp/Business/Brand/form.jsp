<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="pageFormContent" layoutH="60">
    <input name="t_Brand.id" type="hidden" value="${t_Brand.id}" />
    <dl>
        <dt style="width: 100px; text-align: right;">品牌名称：</dt>
        <dd>
            <input name="t_Brand.name" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Brand.name}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">品牌编号：</dt>
        <dd>
            <input name="t_Brand.number" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Brand.number}"/>
        </dd>
    </dl>
</div>
<div class="formBar">
    <ul>
        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
        <li>
            <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
        </li>
    </ul>
</div>