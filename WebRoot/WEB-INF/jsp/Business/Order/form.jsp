<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<div class="pageFormContent" layoutH="60">
    <input name="t_Order.id" type="hidden" value="${order.id}" />
    <input name="t_Order.number" type="hidden" <c:if test="${null != orderNumber}"> value="${orderNumber}"</c:if>
            <c:if test="${null == orderNumber}"> value="${order.number}"</c:if>/>
    <dl>
        <dt style="width: 100px; text-align: right;">订单编号：</dt>
        <dd style="width: 200px;">
            <c:if test="${null != orderNumber}"> ${orderNumber}</c:if>
            <c:if test="${null == orderNumber}"> ${order.number}</c:if>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">订单流水号：</dt>
        <dd style="width: 200px;">
            ${order.serial_number}
        </dd>
    </dl>
    <dl style="width:780px;">
        <dt style="width: 100px; text-align: right;">客户名称：</dt>
        <dd style="width: 650px;">
            <input name="t_Order.customer_name" class="required" type="text" style="width: 600px;"
                   size="100" maxlength="100" value="${order.customer_name}"/>
        </dd>
    </dl>
    <dl style="width:780px;">
        <dt style="width: 100px; text-align: right;">客户联系方式：</dt>
        <dd style="width: 650px;">
            <input name="t_Order.customer_contacts" class="required" type="text" style="width: 600px;"
                   size="100" maxlength="100" value="${order.customer_contacts}"/>
        </dd>
    </dl>
    <dl style="width:780px;">
        <dt style="width: 100px; text-align: right;">客户收货地址：</dt>
        <dd style="width: 650px;">
            <input name="t_Order.receiver_address" class="required" type="text" style="width: 600px;"
                   size="100" maxlength="100" value="${order.receiver_address}"/>
        </dd>
    </dl>
</div>
