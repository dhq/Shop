<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
	<form id="addOrder" method="post" action="Main/Order/add"
		class="pageForm required-validate"
		onsubmit="return validateCallback(this, dialogAjaxDone);">
        <input name="t_Order.status" id="orderStatus" type="hidden" value="1" />
        <%@ include file="form.jsp"%>
        <div class="formBar">
            <ul>
                <li>
                    <div class="buttonActive"><div class="buttonContent"><button type="button" onclick="submitOrderForm(1)">保存</button></div></div>
                </li>
                <li>
                    <div class="buttonActive"><div class="buttonContent"><button type="button" onclick="submitOrderForm(2)">提交</button></div></div>
                </li>
                <li>
                    <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                </li>
            </ul>
        </div>
	</form>
</div>

<script>
    function submitOrderForm(status){
        $("#orderStatus").val(status);
        $("#addOrder").submit();
    }
</script>
