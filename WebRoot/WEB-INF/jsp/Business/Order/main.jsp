<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<style>
    .grid .gridTbody td div a {
        color: blue;
    }
    .grid .gridTbody td div a span {
        line-height: 20px;
    }
</style>
<div class="pageHeader">
    <form id="pagerForm" method="post" action="Main/Order/main" onsubmit="return navTabSearch(this);">
        <input type="hidden" name="pageNum" value="${channelPage.pageNumber}" />
        <input type="hidden" name="numPerPage" value="${channelPage.pageSize}" />
        <div class="searchBar">
            <table class="searchContent">
                <tr>
                    <td>订单状态：</td>
                    <td>
                        <select name="status" class="combox">
                            <option value="">-请选择-</option>
                            <option value="1" <c:if test="${1 == status }">selected</c:if>>未提交</option>
                            <option value="2" <c:if test="${2 == status }">selected</c:if>>已提交</option>
                            <option value="3" <c:if test="${3 == status }">selected</c:if>>已发货</option>
                            <option value="4" <c:if test="${4 == status }">selected</c:if>>已收货</option>
                            <option value="5" <c:if test="${5 == status }">selected</c:if>>已完成</option>
                        </select>
                    </td>
                    <td>销售员：</td>
                    <td>
                        <select name="salesmanId" class="combox">
                            <option value="">-请选择-</option>
                            <c:forEach items="${salesmanList}" var="t">
                                <option value="${t.id }" <c:if test="${t.id == salesmanId }">selected</c:if>>${t.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><input type="checkbox" id="paid" name="paid" value="${paid}" <c:if test="${paid==1}"> checked="checked" </c:if> />已付款 </td>
                    <td><input type="checkbox" id="canceled" name="canceled" value="${canceled}" <c:if test="${canceled==1}"> checked="checked" </c:if> />已取消 </td>
                    <td><div class="buttonActive"><div class="buttonContent"><button type="submit">查询</button></div></div></td>
                </tr>
            </table>
        </div>
    </form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<c:if test="${pert:hasperti(applicationScope.addOrder, loginModel.limit)}">
				<li><a class="add" href="Main/Order/addip" target="dialog" mask="true" drawable="true" resizable="false" maxable="false"
                       rel="addOrder" title="添加订单" width="810" height="430"><span>添加</span></a></li>
			</c:if>
            <c:if test="${pert:hasperti(applicationScope.updateOrder, loginModel.limit)}">
                <li><a class="edit" href="Main/Order/updateip/{id}" target="dialog" mask="true" drawable="true" resizable="false"
                       maxable="false" rel="updateOrder" title="修改订单" width="810" height="430"><span>修改</span></a></li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.deleteOrder, loginModel.limit)}">
                <li><a class="delete" href="Main/Order/delete/{id}" target="ajaxTodo" title="您确定要删除该记录吗?"><span>删除</span></a></li>
            </c:if>
            <li class="line">line</li>
            <c:if test="${pert:hasperti(applicationScope.cancelOrder, loginModel.limit)}">
                <li><a class="delete" href="Main/Order/cancel/{id}" target="ajaxTodo" title="您确定要取消该订单吗?"><span>取消订单</span></a></li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.paidOrder, loginModel.limit)}">
                <li><a class="edit" href="Main/Order/paid/{id}" target="ajaxTodo" title="您确定要将该订单的付款信息改为已付款吗?"><span>已付款</span></a></li>
            </c:if>
            <li class="line">line</li>
            <c:if test="${pert:hasperti(applicationScope.deliveredOrder, loginModel.limit)}">
                <li><a class="edit" href="<%--Main/Order/delivered/{id}--%>#" target="ajaxTodo" title="您确定要将该订单的状态改为已发货吗?"><span>发货</span></a></li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.receivedOrder, loginModel.limit)}">
                <li><a class="edit" href="<%--Main/Order/received/{id}--%>#" target="ajaxTodo" title="您确定要将该订单的状态改为已收货吗?"><span>已收货</span></a></li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.finishedOrder, loginModel.limit)}">
                <li><a class="edit" href="<%--Main/Order/finished/{id}--%>#" target="ajaxTodo" title="您确定要将该订单的状态改为已完成吗?"><span>已完成</span></a></li>
            </c:if>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="75">
		<thead>
			<tr align="center">
                <th width="27px">序号</th>
				<th>订单号</th>
				<th>流水号</th>
				<th>客户姓名</th>
				<th>联系方式</th>
				<th>收货地址</th>
				<th>销售员姓名</th>
				<th>销售员工号</th>
				<th>订单状态</th>
				<th>付款信息</th>
                <c:if test="${pert:hasperti(applicationScope.orderDetail, loginModel.limit)}">
				<th>详情</th>
                </c:if>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty page.list}">
					<c:forEach items="${page.list}" var="o" varStatus="s">
						<tr align="center" target="id" rel="${o.id}">
                            <td>${s.index+1}</td>
                            <td style="text-align: left;">${o.number}</td>
                            <td style="text-align: left;">${o.serial_number}</td>
                            <td style="text-align: left;">${o.customer_name}</td>
                            <td style="text-align: left;">${o.customer_contacts}</td>
                            <td style="text-align: left;">${o.receiver_address}</td>
                            <td style="text-align: left;">${o.salesman_name}</td>
                            <td style="text-align: left;">${o.salesman_staff_id}</td>
							<td>
                                <c:choose>
                                    <c:when test="${o.status==1}">未提交</c:when>
                                    <c:when test="${o.status==2}">已提交</c:when>
                                    <c:when test="${o.status==3}">已发货</c:when>
                                    <c:when test="${o.status==4}">已收货</c:when>
                                    <c:when test="${o.status==5}">已完成</c:when>
                                </c:choose>
							</td>
							<td>
                                <c:choose>
                                    <c:when test="${o.paid==1}">已付款</c:when>
                                    <c:when test="${o.paid==0}">未付款</c:when>
                                </c:choose>
							</td>
                            <c:if test="${pert:hasperti(applicationScope.orderDetail, loginModel.limit)}">
                            <td>
                                <a href="Main/Order/detail/${o.id}" target="dialog" mask="true" drawable="true" resizable="false" maxable="false"
                                   rel="orderDetail" title="订单详情" width="760" height="500"><span>详情</span></a>
                            </td>
                            </c:if>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr align="center">
						<td colspan="4" style="color: red;">无任何记录！</td>
					</tr>	
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option <c:if test="${page.pageSize==20}">selected="selected"</c:if> value="20" >20</option>
				<option <c:if test="${page.pageSize==50}">selected="selected"</c:if> value="50">50</option>
				<option <c:if test="${page.pageSize==100}">selected="selected"</c:if> value="100">100</option>
				<option value="200" <c:if test="${page.pageSize==200}">selected="selected"</c:if>>200</option>
			</select>
			<span>条，共${page.totalRow}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${page.totalRow}" numPerPage="${page.pageSize}" pageNumShown="5" currentPage="${page.pageNumber}"></div>
	</div>
</div>

<script>
    $(document).ready(function(){
        clickCheckBox({checkBox:$("#paid"),targetObj:$("#paid")});
        clickCheckBox({checkBox:$("#canceled"),targetObj:$("#canceled")});
    });
</script>
