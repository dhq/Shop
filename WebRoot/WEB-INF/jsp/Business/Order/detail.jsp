<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
    <div class="panelBar" style="height: 50px;">
        <div style="margin: 9px;">
            <span>订单号：${order.number}　　</span>
            <span>流水号：${order.serial_number}　　</span>
            <span>客户姓名：${order.customer_name}</span>
        </div>
        <div style="margin: 9px;">

            <span>联系方式：${order.customer_contacts}　　</span>
            <span>收货地址：${order.receiver_address}</span>
        </div>
    </div>
    <table class="table" width="100%" layoutH="75">
        <thead>
        <tr align="center">
            <th width="27px">序号</th>
            <th>商品名称</th>
            <th>商品编号</th>
            <th>数量</th>
            <th>成交价</th>
            <th>总成交价</th>
        </tr>
        </thead>
        <tbody>
        <c:choose>
            <c:when test="${! empty itemList}">
                <c:forEach items="${itemList}" var="o" varStatus="s">
                    <tr align="center" target="id" rel="${o.id}">
                        <td>${s.count}</td>
                        <td style="text-align: left;">${o.goods_name}</td>
                        <td style="text-align: left;">${o.goods_number}</td>
                        <td style="text-align: left;">${o.goods_count}</td>
                        <td style="text-align: left;">${o.transaction_value}</td>
                        <td style="text-align: left;"><fmt:formatNumber value="${o.goods_count * o.transaction_value}" pattern="######.##"/></td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr align="center">
                    <td colspan="4" style="color: red;">无任何记录！</td>
                </tr>
            </c:otherwise>
        </c:choose>
        </tbody>
    </table>
</div>

