<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<div class="pageHeader">
    <form id="pagerForm" method="post" action="Main/Goods/main" onsubmit="return navTabSearch(this);">
        <input type="hidden" name="pageNum" value="${channelPage.pageNumber}" />
        <input type="hidden" name="numPerPage" value="${channelPage.pageSize}" />
        <div class="searchBar">
            <table class="searchContent">
                <tr>
                    <td>商品类型：</td>
                    <td>
                        <select name="type" class="combox">
                            <option value="">-请选择-</option>
                            <c:forEach items="${types }" var="t">
                                <option value="${t.id }" <c:if test="${t.id == type }">selected</c:if>>${t.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>商品品牌：</td>
                    <td>
                        <select name="brand" class="combox">
                            <option value="">-请选择-</option>
                            <c:forEach items="${brands }" var="t">
                                <option value="${t.id }" <c:if test="${t.id == brand }">selected</c:if>>${t.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><div class="buttonActive"><div class="buttonContent"><button type="submit">查询</button></div></div></td>
                </tr>
            </table>
        </div>
    </form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<c:if test="${pert:hasperti(applicationScope.addGoods, loginModel.limit)}">
				<li><a class="add" href="Main/Goods/addip" target="dialog" mask="true" drawable="true" resizable="false" maxable="true"
                       rel="addGoods" title="添加商品" width="900" height="650"><span>添加</span></a></li>
			</c:if>
            <c:if test="${pert:hasperti(applicationScope.updateGoods, loginModel.limit)}">
                <li><a class="edit" href="Main/Goods/updateip/{id}" target="dialog" mask="true" drawable="true" resizable="false"
                       maxable="true" rel="updateGoods" title="修改商品" width="900" height="650"><span>修改</span></a></li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.deleteGoods, loginModel.limit)}">
                <li><a class="delete" href="Main/Goods/delete/{id}" target="ajaxTodo" title="您确定要删除该记录吗?"><span>删除</span></a></li>
            </c:if>
            <li class="line">line</li>
            <c:if test="${pert:hasperti(applicationScope.batchAddGoods, loginModel.limit)}">
                <li>
                    <a class="upload" href="Main/Goods/batchAddGoodsip" target="dialog" mask="true" drawable="true" resizable="false"
                       maxable="false" rel="exportExcel" title="批量添加商品" width="500" height="200">
                        <span>批量添加</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.batchUpdateGoods, loginModel.limit)}">
                <li>
                    <a class="edit" href="Main/Goods/batchUpdateGoodsip" target="dialog" mask="true" drawable="true" resizable="false"
                       maxable="false" rel="exportExcel" title="批量修改商品" width="500" height="200">
                        <span>批量修改</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.batchDeleteGoods, loginModel.limit)}">
                <li>
                    <a class="delete" href="Main/Goods/batchDeleteGoodsip" target="dialog" mask="true" drawable="true" resizable="false"
                       maxable="false" rel="exportExcel" title="批量删除商品" width="500" height="200">
                        <span>批量删除</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.batchAddGoods, loginModel.limit)}">
                <li>
                    <a class="upload" href="Main/Goods/batchImageUploadip" target="dialog" mask="true" drawable="true" resizable="false"
                       maxable="false" rel="exportExcel" title="批量添加商品图片" width="500" height="200">
                        <span>批量添加商品图片</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${pert:hasperti(applicationScope.specialPictureView, loginModel.limit)}">
                <li>
                    <a class="upload" href="Main/Goods/specialPictureViewip" target="dialog" mask="true" drawable="true" resizable="false"
                       maxable="false" rel="SpecialPictureView" title="特殊图片" width="800" height="500">
                        <span>浏览特殊图片</span>
                    </a>
                </li>
            </c:if>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="112">
		<thead>
			<tr align="center">
                <th width="27px">序号</th>
				<th>商品名称</th>
				<th>商品编号</th>
				<th>商品品牌</th>
				<th>商品品牌编号</th>
				<th>商品型号</th>
				<th>商品简介</th>
				<th>商品参数</th>
				<th>商品标价</th>
				<th>销售提成</th>
				<th>库存量</th>
                <th>商品图片操作</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty page.list}">
					<c:forEach items="${page.list}" var="o" varStatus="s">
						<tr align="center" target="id" rel="${o.id}">
                            <td>${s.index+1}</td>
                            <td style="text-align: left;">${o.name}</td>
							<td>${o.number}</td>
                            <td style="text-align: left;">${o.brand}</td>
                            <td>${o.brand_number}</td>
                            <td style="text-align: left;">${o.model}</td>
                            <td style="text-align: left;">${o.description}</td>
                            <td style="text-align: left;">${o.parameter}</td>
                            <td style="text-align: left;">${o.price}</td>
                            <td style="text-align: left;">${o.commission}%</td>
                            <td style="text-align: left;">${o.stock}</td>
                            <td><a style="color: blue;" target="dialog" mask="true" drawable="true" resizable="false" maxable="true"
                                   rel="GoodsPictureView" title="商品图片" width="800" height="500" href="Main/Goods/goodsPictureViewip?goods_number=${o.number}">浏览</a></td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr align="center">
						<td colspan="4" style="color: red;">无任何记录！</td>
					</tr>	
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option <c:if test="${page.pageSize==20}">selected="selected"</c:if> value="20" >20</option>
				<option <c:if test="${page.pageSize==50}">selected="selected"</c:if> value="50">50</option>
				<option <c:if test="${page.pageSize==100}">selected="selected"</c:if> value="100">100</option>
				<option value="200" <c:if test="${page.pageSize==200}">selected="selected"</c:if>>200</option>
			</select>
			<span>条，共${page.totalRow}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${page.totalRow}" numPerPage="${page.pageSize}" pageNumShown="5" currentPage="${page.pageNumber}"></div>
	</div>
</div>
