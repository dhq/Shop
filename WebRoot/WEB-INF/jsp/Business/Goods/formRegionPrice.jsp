<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="pageFormContent" layoutH="60">
    <input name="t_Region_Price.id" type="hidden" value="${t_Region_Price.id}" />

    <dl>
        <dt style="width: 100px; text-align: right;">商品编号：</dt>
        <dd>
            <c:choose>
                <c:when test="${t_Region_Price == null }">
                    <select name="t_Region_Price.goods_number" class="required combox">
                        <c:forEach items="${goodses }" var="t">
                            <option value="${t.number }" <c:if test="${t.number == t_Region_Price.goods_number }">selected</c:if>>【${t.name }(${t.number })】</option>
                        </c:forEach>
                    </select>
                </c:when>
                <c:otherwise>
                    <input name="t_Region_Price.goods_number" type="hidden" value="${t_Region_Price.goods_number }" />
                    ${t_Region_Price.goods_number }
                </c:otherwise>
            </c:choose>

        </dd>
    </dl>

    <dl>
        <dt style="width: 100px; text-align: right;">地区码：</dt>
        <dd>
            <select name="t_Region_Price.r_code" class="required combox">
                <c:forEach items="${regionCodes }" var="t">
                    <option value="${t.code }" <c:if test="${t.code == t_Region_Price.r_code }">selected</c:if>>【${t.descript }(${t.code })】</option>
                </c:forEach>
            </select>
        </dd>
    </dl>

    <dl>
        <dt style="width: 100px; text-align: right;">标价：</dt>
        <dd>
            <input name="t_Region_Price.price" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="10" value="${t_Region_Price.price }"/>
        </dd>
    </dl>
</div>
<div class="formBar">
    <ul>
        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
        <li>
            <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
        </li>
    </ul>
</div>
