<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="pageFormContent" layoutH="60">
    <input name="t_Goods.id" type="hidden" value="${t_Goods.id}" />
    <dl>
        <dt style="width: 100px; text-align: right;">商品类型：</dt>
        <dd>
            <select name="t_Goods.type_id" class="required combox">
                <c:forEach items="${types }" var="t">
                    <option value="${t.id }" <c:if test="${t.id == t_Goods.type_id }">selected</c:if>>${t.name}</option>
                </c:forEach>
            </select>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">商品名称：</dt>
        <dd>
            <input name="t_Goods.name" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="100" value="${t_Goods.name}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">商品编号：</dt>
        <dd>
            <input name="t_Goods.number" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Goods.number}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">商品品牌：</dt>
        <dd>
            <select name="t_Goods.brand_id" class="required combox" class="combox">
                <c:forEach items="${brands }" var="t">
                    <option value="${t.id }" <c:if test="${t.id == t_Goods.brand_id }">selected</c:if>>${t.name}</option>
                </c:forEach>
            </select>
        </dd>
    </dl>
    <%--<dl>
        <dt style="width: 100px; text-align: right;">商品品牌：</dt>
        <dd>
            <input name="t_Goods.brand" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Goods.brand}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">商品品牌编号：</dt>
        <dd>
            <input name="t_Goods.barnd_number" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Goods.barnd_number}"/>
        </dd>
    </dl>--%>
    <dl>
        <dt style="width: 100px; text-align: right;">商品型号：</dt>
        <dd>
            <input name="t_Goods.model" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Goods.model}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">商品简介：</dt>
        <dd>
            <input name="t_Goods.description" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="100" value="${t_Goods.description}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">商品参数：</dt>
        <dd>
            <input name="t_Goods.parameter" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="100" value="${t_Goods.parameter}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">商品标价：</dt>
        <dd>
            <input name="t_Goods.price" class="required" type="text" class="number" min="0.00" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Goods.price}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">商品成本价：</dt>
        <dd>
            <input name="t_Goods.cost_price" class="required" type="text" class="number" min="0.00" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Goods.cost_price}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">销售提成：</dt>
        <dd>
            <input name="t_Goods.commission" class="required" type="text" class="number" min="0.00" style="width: 50px;"
                   size="30" maxlength="30" value="${t_Goods.commission}"/>%
        </dd>
    </dl>
    <dl style="width:800px;">
        <dt style="width: 100px; text-align: right;">库存：</dt>
        <dd>
            <input name="t_Goods.stock" class="required" type="text" class="digit" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Goods.stock}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">详细介绍：</dt>
        <dd>
            <textarea id="body" name="t_Goods.detail" class="editor"  rows="24" cols="80">${t_Goods.detail}</textarea>
        </dd>
    </dl>
</div>
<div class="formBar">
    <ul>
        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
        <li>
            <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
        </li>
    </ul>
</div>
<script>
    $('#body').xheditor({
        upLinkUrl:"Main/Goods/uploadFile",
        upLinkExt:"zip,rar,txt,xls,xlsx,doc,docx,pdf",
        upImgUrl:"Main/Goods/uploadImage",
        upImgExt:"jpg,jpeg,gif,png,bmp,ico",
        upFlashUrl:"Main/Goods/uploadFlash",
        upFlashExt:"swf",
        html5Upload:false,
        upMediaUrl:"Main/Goods/uploadMedia",
        upMediaExt:"avi"
    });
</script>
