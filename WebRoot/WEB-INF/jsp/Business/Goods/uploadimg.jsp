<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="pageContent">
    <div class="pageFormContent" layoutH="60">
        <dl>
            <dt style="text-align: right;">上传图片压缩包(.zip)：</dt>
            <dd><input type="file" name="imagezip" class="required" size="30" /></dd>
        </dl>
    </div>
    <div class="formBar">
        <ul>
            <li><div class="buttonActive"><div class="buttonContent"><button type="submit">导入</button></div></div></li>
            <li><div class="button"><div class="buttonContent"><button class="close" type="button">关闭</button></div></div></li>
        </ul>
    </div>
</div>
