<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<style type="text/css">
	.picture_container { margin: 30px 50px; overflow: auto; height: 338px; }
	.picture_info { margin: 10px 15px; width: 286px; float: left; border: 2px double grey; }
	.picture_img { height: 275px; border-bottom: 2px double grey; }
	.picture_img img { margin: 0px 25px; }
	.picture_operate { float: left; width: 36%; height: 100%; border-right: 2px double grey; }
	.picture_operate ul li { float: left; }
	.picture_operate .buttonContent { padding-top: 5px; }
	.buttonContent a { text-decoration:none; color:#183152; }
</style>
<div class="pageContent">
	<form method="post" class="pageForm required-validate"
		onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="60">
			<div class="picture_container">
				<c:choose>
					<c:when test="${! empty pictureList}">
						<c:forEach items="${pictureList}" var="o" varStatus="s">
							<div class="picture_info">
								<div class="picture_img">
									<img src="${o.url }" alt="" width="240" height="275"/>
								</div>
								<div style="height: 35px;">
									<div class="picture_operate">
										<ul style="margin: 5px 10px;">
											<li style="margin-right: 10px;">
												<div class="button">
													<div class="buttonContent">
														<a class="edit" href="Main/Goods/specialPictureUpdateip?id=${o.id }" target="dialog" mask="true" drawable="true" resizable="false"
														   maxable="true" rel="SpecialPictureUpdate" title="修改特殊图片" width="600" height="230">修改</a>
													</div>
												</div>
											</li>
											<li>
												<div class="button">
													<div class="buttonContent">
														<a class="delete" href="Main/Goods/specialPictureDelete?id=${o.id}" callback="dialogAjaxDoneFather" target="ajaxTodo" title="您确定要删除该记录吗?">删除</a>
													</div>
												</div>
											</li>
										</ul>
									</div>
									<div style="float: left; width: 63%; height: 100%;">
										<div style="margin: 4px;">
											图片类型：
											<c:choose>
												<c:when test="${o.pic_type == 'ht'}">热门</c:when>
												<c:when test="${o.pic_type == 'rd'}">推荐</c:when>
											</c:choose>
										</div>
										<div style="margin: 4px;">
											商品编号：
											${o.goods_number }
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<div style="color: red;">
							<span>无任何图片！</span>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<div class="formBar">
			<ul>
				<li>
					<div class="buttonActive">
						<div class="buttonContent" style="padding-top: 5px;">
							<a target="dialog" mask="true" drawable="true" resizable="false" maxable="true" title="上传特殊图片"
									width="600" height="250" type="button" href="Main/Goods/specialPictureUploadip">上传</a>
						</div>
					</div>
				</li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
<script type="application/javascript">
	/*在对话框中操作数据，并刷新对话框*/
	function dialogAjaxDoneFather(json) {
		DWZ.ajaxDone(json);
		if (json.statusCode == DWZ.statusCode.ok) {
			if (json.navTabId) {
				var dialog = $("body").data(json.navTabId);
				$.pdialog.reload(dialog.data("url"), { data: {}, dialogId: json.navTabId, callback: null })
			}
			if ("closeCurrent" == json.callbackType) {
				$.pdialog.closeCurrent();
			}
		}
	}
</script>