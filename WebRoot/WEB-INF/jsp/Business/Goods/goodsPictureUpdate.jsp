<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
	<form method="post" action="Main/Goods/goodsPictureUpdate"
		class="pageForm required-validate"
		onsubmit="return validateCallback(this, dialogAjaxDoneFather);">
		<div class="pageFormContent" layoutH="60">
			<input name="t_Goods_Picture.id" type="hidden" value="${t_Goods_Picture.id}" />
			<dl>
				<dt style="text-align: right;">图片类型：</dt>
				<dd>
					<select name="t_Goods_Picture.pic_type">
						<option value="zs" <c:if test="${t_Goods_Picture.pic_type == 'zs' }">selected="selected"</c:if>>展示</option>
						<option value="xq" <c:if test="${t_Goods_Picture.pic_type == 'xq' }">selected="selected"</c:if>>详情</option>
						<option value="cs" <c:if test="${t_Goods_Picture.pic_type == 'cs' }">selected="selected"</c:if>>参数</option>
						<option value="tj" <c:if test="${t_Goods_Picture.pic_type == 'tj' }">selected="selected"</c:if>>推荐</option>
						<option value="rd" <c:if test="${t_Goods_Picture.pic_type == 'rd' }">selected="selected"</c:if>>热点</option>
					</select>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
<script type="application/javascript">
	/*在对话框中操作数据，并刷新对话框*/
	function dialogAjaxDoneFather(json) {
		DWZ.ajaxDone(json);
		if (json.statusCode == DWZ.statusCode.ok) {
			if (json.navTabId) {
				var dialog = $("body").data(json.navTabId);
				$.pdialog.reload(dialog.data("url"), { data: {}, dialogId: json.navTabId, callback: null })
			}
			if ("closeCurrent" == json.callbackType) {
				$.pdialog.closeCurrent();
			}
		}
	}
</script>