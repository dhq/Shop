<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="pageFormContent" layoutH="60">
    <input name="t_Region_Code.id" type="hidden" value="${t_Region_Code.id}" />
    <dl>
        <dt style="width: 100px; text-align: right;">地区码：</dt>
        <dd>
            <input name="t_Region_Code.code" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="6" value="${t_Region_Code.code}"/>
        </dd>
    </dl>

    <dl>
        <dt style="width: 100px; text-align: right;">地区描述：</dt>
        <dd>
            <input name="t_Region_Code.descript" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Region_Code.descript}"/>
        </dd>
    </dl>
</div>
<div class="formBar">
    <ul>
        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
        <li>
            <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
        </li>
    </ul>
</div>
