<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="pageFormContent" layoutH="60">
    <input name="t_Goods_Type.id" type="hidden" value="${t_Goods_Type.id}" />
    <dl>
        <dt style="width: 100px; text-align: right;">商品类型名称：</dt>
        <dd>
            <input name="t_Goods_Type.name" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Goods_Type.name}"/>
        </dd>
    </dl>
    <dl>
        <dt style="width: 100px; text-align: right;">商品类型编号：</dt>
        <dd>
            <input name="t_Goods_Type.number" class="required" type="text" style="width: 200px;"
                   size="30" maxlength="30" value="${t_Goods_Type.number}"/>
        </dd>
    </dl>
</div>
<div class="formBar">
    <ul>
        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
        <li>
            <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
        </li>
    </ul>
</div>