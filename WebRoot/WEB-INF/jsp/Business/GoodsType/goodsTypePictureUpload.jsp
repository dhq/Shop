<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<form action="Main/GoodsType/goodsTypePictureUpload" method="post" enctype="multipart/form-data" class="pageForm required-validate"
      onsubmit="return iframeCallback(this, dialogAjaxDoneFather)">
    <div class="pageContent">
        <div class="pageFormContent" layoutH="60">
            <input name="goods_type_number" type="hidden" value="${goods_type_number }" />
            <dl style="margin-bottom: 10px;">
                <dt style="text-align: right;">上传图片文件(.jpg, .png, .bmp)：</dt>
                <dd><input type="file" name="goods_type_picture" class="required" size="30" /></dd>
            </dl>
        </div>
        <div class="formBar">
            <ul>
                <li><div class="buttonActive"><div class="buttonContent"><button type="submit">确定</button></div></div></li>
                <li><div class="button"><div class="buttonContent"><button class="close" type="button">关闭</button></div></div></li>
            </ul>
        </div>
    </div>
</form>
<script type="application/javascript">
    /*在对话框中操作数据，并刷新对话框*/
    function dialogAjaxDoneFather(json) {
        DWZ.ajaxDone(json);
        if (json.statusCode == DWZ.statusCode.ok) {
            if (json.navTabId) {
                var dialog = $("body").data(json.navTabId);
                $.pdialog.reload(dialog.data("url"), { data: {}, dialogId: json.navTabId, callback: null })
            }
            if ("closeCurrent" == json.callbackType) {
                $.pdialog.closeCurrent();
            }
        }
    }
</script>