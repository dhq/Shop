<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<form id="pagerForm" method="post" action="Main/BizLog/errInfoDownload">
    <input type="hidden" name="pageNum" value="${page.pageNumber}" />
    <input type="hidden" name="numPerPage" value="${page.pageSize}" />
</form>
<div class="pageContent">
    <table class="table" width="100%" layoutH="75">
        <thead>
        <tr align="center">
            <th width="27px">序号</th>
            <th>错误类型</th>
            <th>发生的时间</th>
            <th>存放路径</th>
            <th>下载信息</th>
        </tr>
        </thead>
        <tbody>
        <c:choose>
            <c:when test="${! empty page.list}">
                <c:forEach items="${page.list}" var="o" varStatus="s">
                    <tr align="center" target="id" rel="${o.id}">
                        <td>${s.index+1}</td>
                        <td>${o.err_type == 'bg' ? "批量商品" : "批量图片"}</td>
                        <td>${o.error_date}</td>
                        <td>${o.url}</td>
                        <td><a style="color: blue;" href="Main/BizLog/errDownload?id=${o.id}">下载</a></td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr align="center">
                    <td colspan="4" style="color: red;">无任何记录！</td>
                </tr>
            </c:otherwise>
        </c:choose>
        </tbody>
    </table>
    <div class="panelBar">
        <div class="pages">
            <span>显示</span>
            <select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
                <option <c:if test="${page.pageSize==20}">selected="selected"</c:if> value="20" >20</option>
                <option <c:if test="${page.pageSize==50}">selected="selected"</c:if> value="50">50</option>
                <option <c:if test="${page.pageSize==100}">selected="selected"</c:if> value="100">100</option>
                <option value="200" <c:if test="${page.pageSize==200}">selected="selected"</c:if>>200</option>
            </select>
            <span>条，共${page.totalRow}条</span>
        </div>
        <div class="pagination" targetType="navTab" totalCount="${page.totalRow}" numPerPage="${page.pageSize}" pageNumShown="5" currentPage="${page.pageNumber}"></div>
    </div>
</div>
