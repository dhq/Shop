<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>"/>
    <title>${system_title }--登陆</title>
    <%--<link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    <link rel="shortcut icon" type="image/x-icon" href="images/bitbug_favicon.ico" />--%>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
    <meta http-equiv="description" content="This is my page"/>

    <link href="css/login/base.css" rel="stylesheet" type="text/css">
    <link href="css/login/login.css" rel="stylesheet" type="text/css">

    <script type="text/javascript">
        <%if(request.getAttribute("mes")!=null){
            out.print(request.getAttribute("mes"));
        }%>
    </script>
</head>

<body onload="javascript:document.form.dlid.focus();" style="overflow: auto;">

<div class="login">
    <form action="Login/login" method="post" id="form">
        <div class="logo"></div>
        <div class="login_form">
            <div class="user">
                <input class="text_value" value="" name="dlid" type="text" id="dlid" >
                <input class="text_value" value="" name="pwd" value="${pwd}" type="password" id="password">
            </div>
            <button class="button" id="submit" type="submit">登录</button>
        </div>

        <div id="tip"></div>
        <div class="foot">
            Copyright © 2011-2015 <a href="#" style="color:#ffffff">登录</a>
        </div>
    </form>
</div>

<!-- Javascript -->
<script>var basedir='js/login/';</script>
<script src="js/login/do.js"></script>
<script src="js/login/config.js"></script>
<script>
    Do.ready('form', function() {
        $("#form").Validform({
            ajaxPost:true,
            postonce:true,
            tiptype:function(msg,o,cssctl){
                if(!o.obj.is("form")){
                    var objtip=o.obj.siblings(".Validform_checktip");
                    cssctl(objtip,o.type);
                    objtip.text(msg);
                }else{
                    var objtip=o.obj.find("#tip");
                    cssctl(objtip,o.type);
                    if(o.type==2){
                        window.location.href='Login/login';
                    }else{
                        objtip.text(msg);
                    }
                }
            }
        });
    });

</script>
</body>
</html>
