<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<% String basePath = request.getContextPath() + "/";%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="Pragma" content="no-cache"> 
<meta http-equiv="Cache-Control" content="no-cache"> 
<meta http-equiv="Expires" content="0"> 
<%--<link rel="icon" type="image/x-icon" href="images/bitbug_favicon.ico" />
<link rel="shortcut icon" type="image/x-icon" href="images/bitbug_favicon.ico" />--%>
<title>${system_title }</title>

<!-- ================================ css start ================================= -->
<link href="<%=basePath%>styles/dwz/themes/default/style.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>styles/dwz/themes/css/core.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>styles/dwz/themes/css/print.css" rel="stylesheet" type="text/css" media="print"/>
<link href="<%=basePath%>styles/dwz/uploadify/css/uploadify.css" rel="stylesheet" type="text/css" media="screen"/>
<!-- ztree css-->
<link href="<%=basePath%>css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<%=basePath%>css/start/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" media="screen" />

<!--[if IE]>
<link href="<%=basePath%>styles/dwz/themes/css/ieHack.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- ================================ css end ==================================== -->

<!-- ============================= javascript start ============================== -->
<script src="<%=basePath%>styles/dwz/js/speedup.js" type="text/javascript"></script>
<script src="<%=basePath%>styles/dwz/js/jquery-1.7.2.js" type="text/javascript"></script>
<script src="<%=basePath%>styles/dwz/js/jquery.cookie.js" type="text/javascript"></script>
<script src="<%=basePath%>styles/dwz/js/jquery.validate.js" type="text/javascript"></script>
<script src="<%=basePath%>styles/dwz/js/jquery.bgiframe.js" type="text/javascript"></script>
<script src="<%=basePath%>styles/dwz/xheditor/xheditor-1.1.14-zh-cn.min.js" type="text/javascript"></script>
<script src="<%=basePath%>styles/dwz/uploadify/scripts/swfobject.js" type="text/javascript"></script>
<script src="<%=basePath%>styles/dwz/uploadify/scripts/jquery.uploadify.v2.1.0.js" type="text/javascript"></script>
<script src="<%=basePath%>styles/dwz/js/dwz.min.js" type="text/javascript"></script>
<script src="<%=basePath%>styles/dwz/js/dwz.regional.zh.js" type="text/javascript"></script>
<!-- ztree js-->
<script src="<%=basePath%>js/jquery.ztree.all-3.5.min.js" type="text/javascript"></script>
<script src="<%=basePath%>js/common.js" type="text/javascript"></script>
<script type="text/javascript">

$(function(){
	DWZ.init("<%=basePath%>styles/dwz/dwz.frag.xml", {
		loginUrl:"/login",
		loginTitle:"Login",	// 弹出登录对话框
		loginUrl:"/login",	// 跳到登录页面
		debug:false,	// 调试模式 【true|false】
		callback:function(){
			initEnv();
			$("#themeList").theme({themeBase:"<%=basePath%>styles/dwz/themes"});
			setTimeout(initSider, 10);
		}
	});
});

function initSider() {
	var $a = $("#navMenu ul li:first a");
    if(undefined === $a || 0 === $a.length) {
        alert("当前用户未配置任何模块操作权限，请联系管理员!");
        return;
    }
	$.post($a.attr("href"), {}, function(html) {
		$("#sidebar").find(".accordion").remove().end().append(html).initUI();
		$a.parent().addClass("selected");
		navTab.closeAllTab();
	});
}
//打开navTab
function openTodoNavTab(url,tabid,title){
    navTab.openTab(tabid, url, { title:title, fresh:false, data:{} });
	
}

//全局设置
$.ajaxSettings.global=false;
</script>



</head>
<body scroll="no">
	<div id="layout">
		<div id="header">
			<div class="headerNav">
				<div class="logo"></div>
				<ul class="nav">
					<li><a>欢迎您，<%--${sessionScope.loginModel.depsname}--%> ${sessionScope.loginModel.userName}</a></li>
					<li><a href="Main/QxUser/pwordsetip" target="dialog" width="530" height="250" mask="true" rel="pwdset">修改密码</a></li>
					<li><a href="Login/logout">退出</a></li>
					<bgsound id="bgsound"/>
                    <audio id="music" src="" autoplay="autoplay"/>
				</ul>
				<%--<ul class="themeList" id="themeList">
					<li theme="default"><div class="selected">蓝色</div></li>					
					<li theme="green"><div>绿色</div></li>
					<li theme="purple"><div>紫色</div></li>
					<li theme="silver"><div>银色</div></li>
					<li theme="azure"><div>天蓝</div></li>
				</ul>--%>
			</div>
		
			<div id="navMenu">
				<ul>
					<c:forEach items="${moudles}" var="md" varStatus="vs">
						<li id="${md.mark}"><a href="Main/daohang/${md.m_id}" <c:if test="${vs.index==0 }">class="selected"</c:if>><span>${md.name}</span></a></li>
					</c:forEach>
				</ul>
			</div>
		</div>

		<div id="leftside"><!-- 用于定位收缩板位置 -->
			<div id="sidebar_s"><div class="collapse"><div class="toggleCollapse"><div></div></div></div></div><!--  上面的收缩功能 -->
			<div id="sidebar">
				<div class="toggleCollapse"><h2>主菜单</h2><div>收缩</div></div><!-- 显示上面的收缩栏 -->
				<div class="accordion" fillSpace="sidebar"></div> <!--收缩面板内容，这由sidebar.jsp替换 -->
			</div>
		</div>
		
		<div id="container">
			<div id="navTab" class="tabsPage">
				<div class="tabsPageHeader">
					<div class="tabsPageHeaderContent"><!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
						<ul class="navTab-tab">
							<li tabid="main" class="main"><a href="javascript:;"><span><span class="home_icon">主页</span></span></a></li>
						</ul>
					</div>
					<div class="tabsLeft">left</div><!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
					<div class="tabsRight">right</div><!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
					<div class="tabsMore">more</div>
				</div>
				<ul class="tabsMoreList">
					<li><a href="javascript:;">主页</a></li>
				</ul>
				<div class="navTab-panel tabsPageContent layoutBox">
					<div class="page unitBox" >
                        <div class="pageContent" style="margin:0 auto; overflow-x:hidden;">

						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</body>
</html>