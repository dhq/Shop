<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<%
	String basePath = request.getContextPath() + "/";
%>
<div class="accordion" fillSpace="sideBar">
	<c:forEach items="${moudles}" var="md" varStatus="vs">
		<div class="accordionHeader">
			<h2><span>Folder</span>${md.name}</h2>
		 </div>
		<div class="accordionContent">
			<ul class="tree treeFolder">
                <c:forEach items="${childMoudleMap.get(md.m_id)}" var="smd" varStatus="s">
                    <li>
                        <c:if test="${smd.openType==0}">
                            <a href="<%=basePath%>${smd.address}" target="navTab" rel="${smd.mark}">${smd.name}</a>
                        </c:if>
                        <c:if test="${smd.openType==1}">
                            <a href="http://<%=request.getServerName()%>:<%=request.getServerPort()%>/DeteSYS/${smd.address}" target="navTab" rel="${smd.mark}" external="true">${smd.name}</a>
                        </c:if>
                        <c:if test="${smd.openType==2}">
                            <a href="${smd.address}" target="navTab" rel="${smd.mark}" external="true">${smd.name}</a>
                        </c:if>
                    </li>
                </c:forEach>
			</ul>
		</div>
	</c:forEach>
</div>