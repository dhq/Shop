<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<form id="pagerForm" method="post" action="Main/Template/main">
	<input type="hidden" name="pageNum" value="${page.pageNumber}" />
	<input type="hidden" name="numPerPage" value="${page.pageSize}" />
</form>
<div class="pageContent">
	<table class="table" width="100%" layoutH="49">
		<thead>
			<tr align="center">
                <th width="27px">序号</th>
				<th style="width:50%;">模板名称</th>
				<th style="width:40%;">说明</th>
			</tr>
		</thead>
		<tbody>
            <tr align="center" target="id" rel="${o.id}">
                <td>1</td>
                <td style="text-align: left;"><a href="Main/Template/download" style="color: #0000ff;">商品导入模板</a></td>
                <td style="text-align: left;">点击模板名或右键另存为可下载模板</td>
            </tr>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option <c:if test="${page.pageSize==20}">selected="selected"</c:if> value="20" >20</option>
				<option <c:if test="${page.pageSize==50}">selected="selected"</c:if> value="50">50</option>
				<option <c:if test="${page.pageSize==100}">selected="selected"</c:if> value="100">100</option>
				<option value="200" <c:if test="${page.pageSize==200}">selected="selected"</c:if>>200</option>
			</select>
			<span>条，共${page.totalRow}1条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${page.totalRow}" numPerPage="${page.pageSize}" pageNumShown="5" currentPage="${page.pageNumber}"></div>
	</div>
</div>
