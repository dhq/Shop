<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<form id="pagerForm" method="post" action="Main/Loglogin/main">
	<input type="hidden" name="pageNum" value="${dljlPage.pageNumber}" /> <input type="hidden" name="numPerPage" value="${dljlPage.pageSize}" /> <input type="hidden" name="sdate" value="${sdate}" /> <input
		type="hidden" name="edate" value="${edate}" />
</form>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="Main/Loglogin/main" method="post">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>查询时间：<input type="text" class="date" name="sdate" readonly value="${sdate}" />
					</td>
					<td>至 &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="date" name="edate" readonly value="${edate}" />
					</td>
					<td>
						<div class="subBar">
							<ul>
								<li><div class="buttonActive">
									<div class="buttonContent">
										<button type="submit">查询</button>
									</div>
								</div></li>
							</ul>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</form>
</div>
<div class="pageContent">
	<table class="table" width="100%" layoutH="85">
		<thead>
			<tr align="center">
				<th width="100px">IP</th>
				<th width="150px">登录用户账号</th>
				<th width="150px">登录用户名</th>
				<th width="150px">登录时间</th>
				<th width="150px">注销时间</th>
				<th>登陆描述</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty dljlPage.list}">
					<c:forEach items="${dljlPage.list}" var="dljl">
						<tr align="center">
							<td>${dljl.ip}</td>
							<td>${dljl.dlid}</td>
							<td>${dljl.name}</td>
							<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${dljl.dltime}" /></td>
							<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${dljl.loDate}" /></td>
							<td>${dljl.dlms}</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr align="center">
						<td colspan="6" style="color: red;">无任何记录！</td>
					</tr>
				</c:otherwise>
			</c:choose>

		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span> <select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option <c:if test="${dljlPage.pageSize==20}">selected="selected"</c:if> value="20">20</option>
				<option <c:if test="${dljlPage.pageSize==50}">selected="selected"</c:if> value="50">50</option>
				<option <c:if test="${dljlPage.pageSize==100}">selected="selected"</c:if> value="100">100</option>
				<option value="200" <c:if test="${dljlPage.pageSize==200}">selected="selected"</c:if>>200</option>
			</select> <span>条，共${dljlPage.totalRow}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${dljlPage.totalRow}" numPerPage="${dljlPage.pageSize}" pageNumShown="5" currentPage="${dljlPage.pageNumber}"></div>
	</div>
</div>
