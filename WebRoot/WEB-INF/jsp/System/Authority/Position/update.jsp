<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
	<form method="post" action="Main/QxPosition/update" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone);">
		<input name="t_Position.id" class="required" type="hidden" value="${position.id}" />
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt style="width: 100px; text-align: right;">职位序号：</dt>
				<dd>
					<input name="t_Position.pid" class="required digits" type="text" style="width: 200px;" size="30" maxlength="30" alt="职位序号" value="${position.pid}" />
				</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">职位名称：</dt>
				<dd>
					<input name="t_Position.pname" class="required" type="text" style="width: 200px;" size="30" maxlength="30" alt="请输入角色名称" value="${position.pname}" />
				</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">启用状态：</dt>
				<dd>
					<select name="t_Position.status" class="required combox">
						<option <c:if test="${position.status==1}">selected</c:if> value="1">已启用</option>
						<option <c:if test="${position.status==0}">selected</c:if> value="0">未启用</option>
					</select>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive">
						<div class="buttonContent">
							<button type="submit">保存</button>
						</div>
					</div></li>
				<li>
					<div class="button">
						<div class="buttonContent">
							<button type="button" class="close">取消</button>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</form>
</div>
