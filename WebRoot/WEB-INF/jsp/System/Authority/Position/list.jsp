<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<form id="pagerForm" method="post" action="Main/QxPosition/main">
	<input type="hidden" name="pageNum" value="${positionPage.pageNumber}" />
	<input type="hidden" name="numPerPage" value="${positionPage.pageSize}" />
</form>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:if test="${pert:hasperti(applicationScope.qxPositionadd, loginModel.limit)}">
				<li><a class="add" href="Main/QxPosition/addip" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="positionadd" title="职位信息添加" width="600" height="250"><span>添加</span></a></li>
		</c:if>
		<c:if test="${pert:hasperti(applicationScope.qxPositionupdate, loginModel.limit)}">
			<li><a class="edit" href="Main/QxPosition/updateip/{id}" target="dialog" mask="true" 
				drawable="true" resizable="true" maxable="true" rel="positionupdate" title="职位信息修改"
				width="600" height="250"><span>修改</span></a></li>
				</c:if>
		<c:if test="${pert:hasperti(applicationScope.qxPositiondel, loginModel.limit)}">		
			<li><a class="delete" href="Main/QxPosition/del/{id}" target="ajaxTodo" title="您确定要删除该职位吗?"><span>删除</span></a></li>
		</c:if>	
		</ul>
	</div>
	<table class="table" width="100%" layoutH="75">
		<thead>
			<tr align="center">
				<th width="300px">职位序号</th>
				<th width="300px">职位名称</th>
				<th>启用状态</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty positionPage.list}">
					<c:forEach items="${positionPage.list}" var="position">
						<tr align="center" target="id" rel="${position.id}">
							<td>${position.pid}</td>
							<td >${position.pname}</td>
							<td>
							<c:if test="${position.status==1}">已启用</c:if>
							<c:if test="${position.status==0}">未启用</c:if>
							</td>
						</tr>		
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr align="center">
						<td colspan="4" style="color: red;">无任何记录！</td>
					</tr>	
				</c:otherwise>
			</c:choose>
			
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option value="20" <c:if test="${positionPage.pageSize==20}">selected="selected"</c:if>>20</option>
				<option value="50" <c:if test="${positionPage.pageSize==50}">selected="selected"</c:if>>50</option>
				<option value="100" <c:if test="${positionPage.pageSize==100}">selected="selected"</c:if>>100</option>
				<option value="200" <c:if test="${positionPage.pageSize==200}">selected="selected"</c:if>>200</option>
			</select>
			<span>条，共${positionPage.totalRow}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${positionPage.totalRow}" numPerPage="${positionPage.pageSize}" pageNumShown="5" currentPage="${positionPage.pageNumber}"></div>
	</div>
</div>
