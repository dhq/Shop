<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
	<form method="post" action="Main/QxDept/update" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt style="width: 100px; text-align: right;">部门名称：</dt>
				<dd>
					<input name="fname" class="required" type="text" style="width: 200px;" size="20" maxlength="20" alt="请输入部门名称" value="${organ.fname}"/>
					<input name="id" type="hidden" value="${organ.id}">
				</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">简称：</dt>
				<dd><input name="sname" class="required" type="text" style="width: 200px;" size="20" maxlength="20" alt="请输入部门" value="${organ.sname}"/></dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">部门状态：</dt>
				<dd>
					<select name="dbstate" class="required combox">
						<option value="">请选择</option>
						<option value="0" <c:if test="${'0'==organ.dbstate}">selected="selected"</c:if>>启用</option>
						<option value="1" <c:if test="${'1'==organ.dbstate}">selected="selected"</c:if>>禁用</option>
					</select>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
