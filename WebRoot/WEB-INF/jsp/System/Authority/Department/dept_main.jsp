<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		$("#deptBox").loadUrl("Main/QxDept/main/list/${mpId}", null, function() {
			$("#deptBox").find("[layoutH]").layoutH();
		});
	});
</script>

<div class="pageContent" style="background: #eef4f5; padding: 0;">
	<div style="">
		<div>
			<div layoutH="15" style="float: left; display: block; overflow: auto; width: 240px; border: solid 1px #CCC; line-height: 21px; background: #fff; margin: 5px;">
				<ul class="tree treeFolder expend">
					<li><a href="Main/QxDept/main/list/0" target="ajax" rel="deptBox">${system_version_company}</a> ${moudles}</li>
				</ul>
			</div>
			<div id="deptBox" class="unitBox" style="margin-left: 246px; margin: 5px;"></div>
		</div>
		<div style="clear: both;"></div>
	</div>
</div>



