<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent" style="border-left:1px #B8D0D6 solid;border-right:1px #B8D0D6 solid">
	<div class="panelBar" style="border-bottom: 0; height: 23px; line-height: 23px; font-weight: bold;">
			<c:choose>
				<c:when test="${0!=moudleId}">&nbsp;&nbsp;部门名称：${moudle.fname}</c:when>
				<c:otherwise>&nbsp;&nbsp;${system_version_company}</c:otherwise>
			</c:choose>
	</div>
	<div class="panelBar"> 
		<ul class="toolBar">
			<c:if test="${pert:hasperti(applicationScope.qxDeptadd, loginModel.limit)}"> 
				<li><a class="add" href="Main/QxDept/addip/${moudleId}" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="deptadd" title="部门添加" width="510" height="200"><span>添加</span></a></li>
    		</c:if>
   			<c:if test="${pert:hasperti(applicationScope.qxDeptupdate, loginModel.limit)}">
				<li><a class="edit" href="Main/QxDept/updateip/{dept_id}" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="deptBox" title="部门修改" width="510" height="200"><span>修改</span></a></li>
  			</c:if>			
			<c:if test="${pert:hasperti(applicationScope.qxDeptdelete, loginModel.limit)}">
				<li><a class="delete" href="Main/QxDept/delete/{dept_id}" target="ajaxTodo" callback="reloadAjaxDone" title="您确定要删除该部门信息吗?"><span>删除</span></a></li>
  			</c:if> 
		</ul>
	</div>
	<div class="panelBar" style="border-top: 0; height: 23px; line-height: 23px; font-weight: bold; text-align: center;">
			下属部门信息
	</div>
	<table class="table" width="100%" layoutH="106" rel="deptBox">
		<thead>
			<tr align="center">
				<th width="150">部门名称</th>
				<th>简称</th>
				<th width="80">当前状态</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty moudles}">
					<c:forEach items="${moudles}" var="m" varStatus="vs">
						<tr align="center" target="dept_id" rel="${m.id}">
							<td>${m.fname}</td>
							<td>${m.sname}</td>
							<td>
								<c:choose>
									<c:when test="${m.dbstate=='0'}">启用</c:when>
									<c:otherwise>禁用</c:otherwise>
								</c:choose>
							</td>
						</tr>			
					</c:forEach>		
				</c:when>
				<c:otherwise><tr align="center"><td colspan="6" style="color: red;">无任何记录！</td></tr></c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>