<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
	<form method="post" action="Main/QxDept/add/${organId}" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt style="width: 120px; text-align: right;">部门名称（全称）：</dt>
				<dd>
					<input name="fname" class="required" type="text" style="width: 200px;" size="20" maxlength="20" alt="请输入部门名称" /> <input name="dlvl" type="hidden" size="10" maxlength="5" value="2" />
				</dd>
			</dl>
			<dl>
				<dt style="width: 120px; text-align: right;">部门（简称）：</dt>
				<dd>
					<input name="sname" class="required" type="text" style="width: 200px;" size="20" maxlength="20" alt="请输入部门" />
				</dd>
			</dl>
			<dl>
				<dt style="width: 120px; text-align: right;">模块状态：</dt>
				<dd>
					<select name="dbstate" class="required combox">
						<option value="0">启用</option>
						<option value="1">禁用</option>
					</select>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive">
						<div class="buttonContent">
							<button type="submit">保存</button>
						</div>
					</div></li>
				<li><div class="button">
						<div class="buttonContent">
							<button type="button" class="close">取消</button>
						</div>
					</div></li>
			</ul>
		</div>
	</form>
</div>
