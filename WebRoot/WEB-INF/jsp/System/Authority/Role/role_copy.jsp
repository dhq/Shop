<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
	<form method="post" action="Main/QxRole/add/copy" 
		class="pageForm required-validate" 
		onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt style="width: 100px; text-align: right;">复制的角色序号：</dt>
				<dd>${role.xh}
				</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">复制的角色名称：</dt>
				<dd>${role.name}
					<input name="id" class="required" type="hidden" value="${role.id}"/>
				</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">复制的角色描述：</dt>
				<dd>${role.ms}</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">角色序号：</dt>
				<dd><input name="xh" class="required digits" type="text" style="width: 200px;" 
						size="30" maxlength="30" alt="请输入角色序号"/></dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">角色名称：</dt>
				<dd><input name="name" class="required" type="text" style="width: 200px;" 
						size="30" maxlength="30" alt="请输入角色名称"/></dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">角色描述：</dt>
				<dd><textarea name="ms" cols="50" rows="2"  maxlength="200"/></dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
