<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<form id="pagerForm" method="post" action="Main/QxRole/main">
	<input type="hidden" name="pageNum" value="${rolePage.pageNumber}" />
	<input type="hidden" name="numPerPage" value="${rolePage.pageSize}" />
</form>
<div class="pageContent" style="margin-top:-1px">
	<div class="panelBar">
		<ul class="toolBar">
			<c:if test="${pert:hasperti(applicationScope.qxRoleadd, loginModel.limit)}">
			<li><a class="add" href="Main/QxRole/addip" target="dialog" mask="true" drawable="true"
				resizable="true" maxable="true" rel="useradd" title="角色信息添加"
				width="600" height="250"><span>添加</span></a></li>
			</c:if>	
			<c:if test="${pert:hasperti(applicationScope.qxRoleupdate, loginModel.limit)}">	
			<li><a class="edit" href="Main/QxRole/updateip/{role_id}" target="dialog" mask="true" 
				drawable="true" resizable="true" maxable="true" rel="userupdate" title="角色信息修改"
				width="600" height="250"><span>修改</span></a></li>
			</c:if>	
			<c:if test="${pert:hasperti(applicationScope.qxRoledelete, loginModel.limit)}">
			<li><a class="delete" href="Main/QxRole/delete/{role_id}" target="ajaxTodo" 
				title="您确定要删除该角色信息吗?"><span>删除</span></a></li>
			</c:if>	
			<c:if test="${pert:hasperti(applicationScope.qxRoleadd, loginModel.limit)}">
			<li><a class="add" href="Main/QxRole/addip/copy/{role_id}" target="dialog" mask="true" 
				drawable="true" resizable="true" maxable="true" rel="useradd" title="角色信息复制添加"
				width="600" height="300"><span>复制</span></a></li>
			</c:if>	
			<li class="line">line</li>
			<c:if test="${pert:hasperti(applicationScope.qxRoleperset, loginModel.limit)}">	
			<li><a class="edit" href="Main/QxRole/persetip/{role_id}" target="dialog" mask="true" 
				drawable="true" resizable="true" maxable="true" rel="userperset" title="用户权限分配"
				width="600" height="460"><span style="text-align: center;">权限分配</span></a></li>
			</c:if>
		</ul>
	</div>
	<table class="table" width="98%" layoutH="73">
		<thead>
			<tr align="center">
				<th width="60">角色序号</th>
				<th width="160">角色名称</th>
				<th width="250">角色描述</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty rolePage.list}">
					<c:forEach items="${rolePage.list}" var="role">
						<tr align="center" target="role_id" rel="${role.id}">
							<td>${role.xh}</td>
							<td>${role.name}</td>
							<td>${role.ms}</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr align="center"><td colspan="4" style="color: red;">无任何记录！</td></tr>	
				</c:otherwise>
			</c:choose>
			
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option <c:if test="${rolePage.pageSize==20}">selected="selected"</c:if>
					 value="20" >20</option>
				<option <c:if test="${rolePage.pageSize==50}">selected="selected"</c:if>
					value="50">50</option>
				<option <c:if test="${rolePage.pageSize==100}">selected="selected"</c:if>
					value="100">100</option>
				<option value="200"
					<c:if test="${rolePage.pageSize==200}">selected="selected"</c:if>>200</option>
			</select>
			<span>条，共${rolePage.totalRow}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${rolePage.totalRow}" 
			numPerPage="${rolePage.pageSize}" pageNumShown="5" 
			currentPage="${rolePage.pageNumber}"></div>
	</div>
</div>
