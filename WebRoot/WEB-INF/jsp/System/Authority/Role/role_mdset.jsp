<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<%@ taglib prefix="pert" uri="/WEB-INF/permit.tld"%>

<script type="text/javascript">
	var zTree;
	var setting = {
		check: {
			enable: true
			},
		data: {
			simpleData: {
				enable: true,
				idKey: "id",
				pIdKey: "pId"
			}
		},
		callback: {
			onCheck: zTreeOnCheck
		}
	};
	function zTreeOnCheck(event, treeId, treeNode) {
	    var nodes = zTree.getCheckedNodes(true);
	    var strId = "";
	    for(var i=0; i<nodes.length; i++){
	    	strId += "_" + nodes[i].id + ",";
	    }
	    $("#cmlimit").val(strId);
	};
	
	var zNodes =[
			<c:forEach items="${moudles}" var="md" varStatus="vx">
			{ id:"${md.m_id}", pId:"${md.m_pid}", name:"${md.name}",
				<c:choose>
					<c:when test="${pert:hasperti(md.m_id, role.cmlimit)}">checked: true</c:when>
					<c:otherwise>checked: false</c:otherwise>
				</c:choose>
			}<c:if test="${fn:length(moudles)!=vx.index+1}">,</c:if>
			</c:forEach>	
	];

	$(document).ready(function(){
		$.fn.zTree.init($("#treeDemo"), setting, zNodes);
		zTree = $.fn.zTree.getZTreeObj('treeDemo');
	});
	
</script>
	
<div class="pageContent">
	<form method="post" action="Main/QxRole/mdset" 
		class="pageForm required-validate" 
		onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt>角色名称：</dt>
				<dd>${role.name}</dd>
			</dl>
			<dl>
				<dt>角色描述：</dt>
				<dd>
					${role.ms}
					<input name="id" class="required" type="hidden" value="${role.id}" />
					<input type="hidden" id="cmlimit" name="cmlimit" value="${role.cmlimit}" />
				</dd>
			</dl>	
			<dl>
				<dt>拥有控制的权限模块：</dt>
			</dl>
			<div style="height: 245px; clear: both; overflow: auto;">	
				<ul id="treeDemo" class="ztree"></ul>
			</div>	
		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
