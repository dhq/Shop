<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<div class="pageContent">
	<form method="post" action="Main/QxUser/update"  class="pageForm required-validate"
		onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt style="width: 100px; text-align: right;">用户登录ID：</dt>
				<dd style="width: 200px;">
					${user.dlid} <input name="id" class="required" type="hidden"
						value="${user.id}" />
				</dd>
			</dl>
            <dl>
                <dt style="width: 100px; text-align: right;">用户名称：</dt>
                <dd style="width: 200px;">
                    <input name="name" class="required" type="text"
                           style="width: 200px;" size="20" maxlength="20"
                           value="${user.name}" alt="请输入用户名称" />
                </dd>
            </dl>
            <dl>
                <dt style="width: 100px; text-align: right;">性别：</dt>
                <dd>
                    <input type="radio" name="sex" value="1" <c:if test="${user.sex==1}">checked="checked" </c:if> >男
                    <input type="radio" name="sex" value="2" <c:if test="${user.sex==2}">checked="checked" </c:if> >女
                </dd>
            </dl>
			<dl>
				<dt style="width: 100px; text-align: right;">用户密码：</dt>
				<dd style="width: 200px;">
					<input id="pwd" name="pwd"  type="password" style="width: 200px;"
						size="30" maxlength="30" placeholder="修改则填写"/>
				</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">确认密码：</dt>
				<dd style="width: 200px;">
					<input name="qrpwd" type="password" style="width: 200px;"
						equalTo="#pwd" size="30" maxlength="30" placeholder="修改则填写" />
				</dd>
			</dl>


			<%--<dl style="width: 460px;">
				<dt style="width: 100px; text-align: right;">所属部门：</dt>
				<dd style="width: 300px;">
					<input name="district.id" value="${user.d_id }" type="hidden" /> 
					<input class="required" name="district.districtName" type="text" readonly
						value="${deparment.sname }" style="width: 200px;" /> <a
						class="btnLook" href="Main/QxUser/gettree" lookupGroup="district"
						width="300" height="460">查找带回</a>
				</dd>
			</dl>
			<dl style="width: 460px;">
				<dt style="width: 100px; text-align: right;">所属职位：</dt>
				<dd>
					<select name="position.id" style="width: 206px">
						<c:if test="${! empty tPosition}">
						<c:forEach items="${tPosition}" var="t">
							<option value="${t.id}" <c:if test="${user.pid==t.id}">selected="selected"</c:if>>${t.pname}</option>
						</c:forEach>
						</c:if>
					</select>
				</dd>
			</dl>--%>

            <dl>
                <dt style="width: 100px; text-align: right;">工号：</dt>
                <dd>
                    <input name="staff_id" class="required" type="text" value="${user.staff_id}" style="width: 200px;" size="20" maxlength="20" />
                </dd>
            </dl>
            <dl>
                <dt style="width: 100px; text-align: right;">开户行：</dt>
                <dd>
                    <input name="bank_name" class="required" type="text" value="${user.bank_name}" style="width: 200px;" size="20" maxlength="20" />
                </dd>
            </dl>
            <dl>
                <dt style="width: 100px; text-align: right;">银行账号：</dt>
                <dd>
                    <input name="bank_account" class="required" type="text" value="${user.bank_account}" style="width: 200px;" size="20" maxlength="19" />
                </dd>
            </dl>
			<%--<dl>
				<dt style="width: 100px; text-align: right;">在职状态：</dt>
				<dd>
					<select name="lo" class="combox">
						<option value="">请选择</option>
						<option value="1" <c:if test="${user.lo==1}">selected</c:if>>在职</option>
						<option value="0" <c:if test="${user.lo==0}">selected</c:if>>离职</option>
					</select>
				</dd>
			</dl>--%>

            <dl>
                <dt style="width: 100px; text-align: right;">用户启用：</dt>
                <dd style="width: 200px;">
                    <select name="usable" class="required combox">
                        <option value="">请选择</option>
                        <option <c:if test="${user.usable==1}">selected</c:if>
                                value="true">是</option>
                        <option <c:if test="${user.usable==0}">selected</c:if>
                                value="false">否</option>
                    </select>
                </dd>
            </dl>

			<%--<dl>
				<dt style="width: 100px; text-align: right;">职位信息：</dt>
				<dd><textarea name="zwxx" cols="50" rows="3" maxlength="200">${user.zwxx}</textarea></dd>
			</dl>--%>

		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive">
						<div class="buttonContent">
							<button type="submit">保存</button>
						</div>
					</div>
				</li>
				<li>
					<div class="button">
						<div class="buttonContent">
							<button type="button" class="close">关闭</button>
						</div>
					</div></li>
			</ul>
		</div>
	</form>
</div>
