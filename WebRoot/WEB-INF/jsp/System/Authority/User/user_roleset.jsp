<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<%@ taglib prefix="pert" uri="/WEB-INF/permit.tld"%>

<div class="pageContent" style="overflow: auto;">

	<div class="panelBar" style="height: 23px; line-height: 23px;">
		用户登录ID：${user.dlid};&nbsp;&nbsp;&nbsp;&nbsp;用户名称：${user.name}
	</div>
	<table class="table" width="100%" layoutH="195">
		<thead>
			<tr align="center">
				<th>优先系数</th>
				<th>角色名称</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty userroles}">
					<c:forEach items="${userroles}" var="userrole">
						<tr align="center">
							<td>${userrole.priority}</td>
							<td>${userrole.name}</td>
							<td><a class="btnDel" href="Main/QxUser/roledelete/${userrole.id}" target="ajaxTodo" callback="reloadAjaxDone" title="您确定要删除该用户的角色信息吗?"></a></td>
						</tr>		
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr align="center">
						<td colspan="2" style="color: red;">无任何记录！</td>
					</tr>	
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div class="panelBar"></div>
	<div>
		<form method="post" action="Main/QxUser/roleset" class="pageForm required-validate" onsubmit="return validateCallback(this, reloadAjaxDone);">
			<div class="pageFormContent">
				<dl>
					<dt>优先系数：</dt>
					<dd>
						<input name="priority" class="required digits" type="text" style="width: 200px;" size="5" maxlength="5" alt="请输入优先系数"/>
						<input name="id" class="required digits" type="hidden" value="${user.id}" />
					</dd>
				</dl>
				<dl>
					<dt>系统角色：</dt>
					<dd>
						<select name="roleId" class="required digits combox">
							<option value="0">请选择</option>
							<c:forEach items="${allroles}" var="role">
								<option value="${role.id}">${role.name}</option>
							</c:forEach>
						</select>
					</dd>
				</dl>
			</div>
			<div class="formBar">
				<ul>
					<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
					<li><div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div></li>
				</ul>
			</div>
		</form>
	</div>
</div>
