<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
	<form method="post" action="Main/QxUser/add" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt style="width: 100px; text-align: right;">用户登录ID：</dt>
				<dd>
					<input name="dlid" class="required" type="text" style="width: 200px;" size="25" maxlength="25" alt="请输入用户登录ID，字母和数字组合" />
				</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">用户名称：</dt>
				<dd>
					<input name="name" class="required" type="text" style="width: 200px;" size="20" maxlength="20" />
				</dd>
			</dl>
            <dl>
                <dt style="width: 100px; text-align: right;">性别：</dt>
                <dd>
                    <input type="radio" name="sex" value="1" checked="checked">男
                    <input type="radio" name="sex" value="2">女
                </dd>
            </dl>
			<dl>
				<dt style="width: 100px; text-align: right;">用户密码：</dt>
				<dd>
					<input id="pwd" name="pwd" class="required" type="password" style="width: 200px;" size="30" maxlength="30" />
				</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">确认密码：</dt>
				<dd>
					<input name="qrpwd" class="required" type="password" style="width: 200px;" equalTo="#pwd" size="30" maxlength="30" />
				</dd>
			</dl>
			<%--<dl style="width: 460px;">
				<dt style="width: 100px; text-align: right;">所属部门：</dt>
				<dd style="width: 300px;">
					<input name="district.id" value="" type="hidden" /> <input class="required" name="district.districtName" type="text" readonly value="${deparment.sname}" style="width: 200px;" /> <a
						class="btnLook" href="Main/QxUser/gettree" lookupGroup="district" width="300" height="460">查找带回</a>
				</dd>
			</dl>
			<dl style="width: 460px;">
				<dt style="width: 100px; text-align: right;">所属职位：</dt>
				<dd>
					<select name="position.id" style="width: 206px">
						<c:if test="${! empty tPosition}">
							<c:forEach items="${tPosition}" var="t">
								<option value="${t.id}">${t.pname}</option>
							</c:forEach>
						</c:if>
					</select>
				</dd>
			</dl>--%>
            <dl>
                <dt style="width: 100px; text-align: right;">工号：</dt>
                <dd>
                    <input name="staff_id" class="required" type="text" style="width: 200px;" size="20" maxlength="20" />
                </dd>
            </dl>
            <dl>
                <dt style="width: 100px; text-align: right;">开户行：</dt>
                <dd>
                    <input name="bank_name" class="required" type="text" style="width: 200px;" size="20" maxlength="20" />
                </dd>
            </dl>
            <dl>
                <dt style="width: 100px; text-align: right;">银行账号：</dt>
                <dd>
                    <input name="bank_account" class="required" type="text" style="width: 200px;" size="20" maxlength="19" />
                </dd>
            </dl>
			<%--<dl>
				<dt style="width: 100px; text-align: right;">在职状态：</dt>
				<dd>
					<select name="lo" class="combox">
						<option value="">请选择</option>
						<option value="1" selected>在职</option>
						<option value="0">离职</option>
					</select>
				</dd>
			</dl>--%>
			<dl>
				<dt style="width: 100px; text-align: right;">用户启用：</dt>
				<dd>
					<select name="usable" class="combox">
						<option value="">请选择</option>
						<option value="true" selected>是</option>
						<option value="false">否</option>
					</select>
				</dd>
			</dl>
			<%--<dl>
				<dt style="width: 100px; text-align: right;">职位信息：</dt>
				<dd>
					<textarea name="zwxx" cols="50" rows="3" maxlength="200" />
				</dd>
			</dl>--%>
		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive">
						<div class="buttonContent">
							<button type="submit">保存</button>
						</div>
					</div></li>
				<li>
					<div class="button">
						<div class="buttonContent">
							<button type="button" class="close">关闭</button>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</form>
</div>
