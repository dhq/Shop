<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<div class="pageHeader">
	<form id="pagerForm" onsubmit="return navTabSearch(this);" action="Main/QxUser/main" method="post">
	<input type="hidden" name="pageNum" value="${userPage.pageNumber}" />
	<input type="hidden" name="numPerPage" value="${userPage.pageSize}" />
	<div class="searchBar">
		<table class="searchContent">
			<tr>
				<td align="right"><label>用户名：</label><input type="text" name="name" value="${username}" /></td>
				<%--<td align="right"><label>部门：</label></td>
				<td align="left">
					<select name="deparment_id" class="combox">
						<option value="">全部部门</option>
						<c:forEach items="${deptstrs}" var="dept">
							<option value="${dept[0]}" <c:if test="${!empty deparment_id && dept[0]==deparment_id}">selected="selected"</c:if>>${dept[1]}</option>
						</c:forEach>
					</select>
				</td>--%>
				<td>
					<div class="subBar">
						<ul><li><div class="buttonActive"><div class="buttonContent"><button type="submit">查询</button></div></div></li></ul>
					</div>
				</td>
			</tr>
		</table>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			 <%-- <c:if test="${pert:hasperti(applicationScope.qxUseradd, loginModel.limit)}">   --%>
			 <c:if test="${pert:hasperti(applicationScope.qxUseradd, loginModel.limit)}">
				<li><a class="add" href="Main/QxUser/addip" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="useradd" title="用户信息添加" width="430" height="410"><span>添加</span></a></li>
			</c:if>
			 <c:if test="${pert:hasperti(applicationScope.qxUserdel, loginModel.limit)}">
				<li><a class="delete" href="Main/QxUser/del/{user_id}" target="ajaxTodo" title="您确定要删除该用户信息吗?"><span>删除</span></a></li>
			</c:if>
			 <c:if test="${pert:hasperti(applicationScope.qxUserupdate, loginModel.limit)}">
			    <li><a class="edit" href="Main/QxUser/updateip/{user_id}" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="userupdate" title="用户信息修改" width="430" height="410"><span>修改</span></a></li>
			</c:if>
			<c:if test="${pert:hasperti(applicationScope.qxUserroleset, loginModel.limit)}">
				<li><a class="edit" href="Main/QxUser/rolesetip/{user_id}" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="userroleset" title="用户角色分配" width="600" height="335"><span style="text-align: center;">角色分配</span></a></li>
		    </c:if>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="112">
		<thead>
			<tr align="center">
				<th width="150px">用户ID</th>
				<th width="80px">用户名称</th>
				<th width="40px">性别</th>
				<th width="80px">工号</th>
				<%--<th width="80px">在职状态</th>--%>
				<th width="300px">已分配角色</th>
				<%--<th width="200px">所属部门</th>
				<th width="150px">职位</th>--%>
				<th width="80px">是否可用</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty userPage.list}">
					<c:forEach items="${userPage.list}" var="user">
						<tr align="center" target="user_id" rel="${user.id}">
							<td>${user.dlid}</td>
							<td>${user.name}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${user.sex=='1'}">男</c:when>
                                    <c:when test="${user.sex=='2'}">女</c:when>
                                    <c:otherwise></c:otherwise>
                                </c:choose>
                            </td>
                            <td>${user.staff_id}</td>
							<%--<td>
								<c:if test="${user.lo==1}">在职</c:if>
								<c:if test="${user.lo==0}">离职</c:if>
							</td>--%>
							<td>
								<c:choose>
									<c:when test="${null==rnameMap[str:append(user.id, '')]}">无</c:when>
									<c:otherwise>${rnameMap[str:append(user.id, '')]}</c:otherwise>
								</c:choose>
							</td>
							<%--<td>${user.dname }</td>
							<td>${user.pname}</td>--%>
							<td>
								<c:choose>
									<c:when test="${user.usable=='1'}">是</c:when>
									<c:otherwise>否</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise><tr align="center"><td colspan="4" style="color: red;">无任何记录！</td></tr></c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option <c:if test="${userPage.pageSize==20}">selected="selected"</c:if> value="20">20</option>
				<option <c:if test="${userPage.pageSize==50}">selected="selected"</c:if> value="50">50</option>
				<option <c:if test="${userPage.pageSize==100}">selected="selected"</c:if> value="100">100</option>
				<option <c:if test="${userPage.pageSize==200}">selected="selected"</c:if> value="200">200</option>
			</select>
			<span>条，共${userPage.totalRow}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${userPage.totalRow}" numPerPage="${userPage.pageSize}" pageNumShown="5" currentPage="${userPage.pageNumber}"></div>
	</div>
</div>
