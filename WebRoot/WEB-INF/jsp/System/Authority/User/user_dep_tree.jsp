<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<div class="pageContent">
	<div style="float:left; display:block; margin:5px; overflow:auto; width:200px; height:100%; border:solid 1px #4CA5D8;">
		<ul class="tree treeFolder treeCheck expand" oncheck="onCheck">
			<c:forEach items="${departs}" var="dep">
			<li><a >${dep.fname}</a>
				<ul>
					<c:forEach items="${users}" var="user">
						<c:if test="${user.d_id==dep.id}">
							<li><a tname="userId" tvalue="${user.id}">${user.name}(${user.dlid})</a></li>
						</c:if>
					</c:forEach>
				</ul>
			</li>
			</c:forEach>
		</ul>
	</div>
	
	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button">关闭</button></div></div></li>
		</ul>
	</div>
</div>