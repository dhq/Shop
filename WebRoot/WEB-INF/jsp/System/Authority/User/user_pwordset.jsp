<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
	
	<form method="post" action="Main/QxUser/pwordset/${user.id }" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone)">
		<div class="pageFormContent" layoutH="58">

			<div class="unit">
				<label>用户ID：</label>
				<label>${user.dlid}</label>
			</div>
				<div class="unit">
				<label>旧密码：</label>
				<input type="password"  name="oldpassword"  size="30"  class="required alphanumeric"/>
			</div>
			<div class="unit">
				<label>新密码：</label>
				<input type="password" id="cp_newPassword" name="password" customvalid="customvalidPwd(element)" title="customvalid:请输入不少于6位的密码" size="30" minlength="4" maxlength="20" class="required alphanumeric"/>
			</div>
			<div class="unit">
				<label>重复输入新密码：</label>
				<input type="password" name="rnewPassword" size="30" equalTo="#cp_newPassword" class="required alphanumeric"/>
			</div>
			
		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
	
</div>

<script type="text/javascript">
function customvalidPwd(element){
	if ($(element).val() == "111111") return false;
	return true;
}
</script>