<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<div class="pageContent" style="border-left: 1px #B8D0D6 solid; border-right: 1px #B8D0D6 solid">
	<div class="panelBar" style="border-bottom: 0; height: 23px; line-height: 23px; font-weight: bold; text-align: center;">关键字：${common.name}</div>
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="Main/Common/addip/${common.id }" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="common_add" title="参数添加" width="600" height="325"><span>添加</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="108">
		<thead>
			<tr align="center">
				<th width="5%">序号</th>
				<th width="45%">参数名称</th>
				<th width="25%">参数备注</th>
				<th width="10%">启用状态</th>
				<th width="15%">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty details}">
					<c:forEach items="${details}" var="detail" varStatus="vs">
						<tr target="id" rel="${list.id}" align="center">
							<td>${vs.count }</td>
							<td>${detail.name}</td>
							<td>${detail.remark }</td>
							<td><c:if test="${detail.status == 1}">是</c:if> <c:if test="${detail.status == 0}">否</c:if></td>
							<td><a class="btnEdit" href="Main/Common/updateip/${detail.id}" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="common_update" title="参数修改" width="600"
								height="325">修改</a>
								<a class="btnDel" href="Main/Common/delete/${detail.id}" target="ajaxTodo" title="确定删除该选项">删除</a></td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr align="center">
						<td colspan="6" style="color: red;">无记录！</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>共 <c:choose>
					<c:when test="${! empty details}">${fn:length(details)}</c:when>
					<c:otherwise>0</c:otherwise>
				</c:choose>条记录
			</span>
		</div>
	</div>
</div>