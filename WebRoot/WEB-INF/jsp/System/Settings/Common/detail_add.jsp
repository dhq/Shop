<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<div class="pageContent">
	<form method="post" action="Main/Common/add" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone);">
		<input type="hidden" name="t_Common_Detail.cid" value="${common.id }"	/>
		<div class="pageFormContent" layoutH="58">
			<table class="wordInfo" align="center" style="width: 98%">
				<tr>
					<td width="30%" class="title">关键字</td>
					<td width="70%">&nbsp;${common.name }</td>
				</tr>
				<tr>
					<td class="title">参数名称</td>
					<td><textarea name="t_Common_Detail.name" rows="7" class="required" style="width: 98%; overflow:auto"></textarea>
					</td>
				</tr>
				<tr>
					<td class="title">备注信息</td>
					<td><input type="text" name="t_Common_Detail.remark" style="width: 98%" /></td>
				</tr>
				<tr>
					<td class="title">启用状态</td>
					<td><input type="radio" name="t_Common_Detail.status" checked="checked" value="1" />是 
					<input type="radio" name="t_Common.status" value="0" />否</td>
				</tr>
			</table>
		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
