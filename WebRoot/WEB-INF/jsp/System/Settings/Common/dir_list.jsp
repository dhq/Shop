<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>
<div class="pageContent" style="border-left:1px #B8D0D6 solid;border-right:1px #B8D0D6 solid">
	<div class="panelBar" style="border-bottom: 0; height: 23px; line-height: 23px; font-weight: bold; text-align: center;">关键字目录</div>
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="Main/Common/dir_addip" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="common_add" title="关键字添加" width="420" height="225"><span>关键字添加</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="108">
		<thead>
			<tr align="center">
				<th width="5%">序号</th>
				<th width="15%">关键字名称</th>
				<th width="15%">数据库字段名</th>
				<th width="40%">备注信息</th>
				<th width="10%">启用信息</th>
				<th width="15%">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty commons}">
					<c:forEach items="${commons}" var="common" varStatus="vs">
						<tr align="center" target="cid" rel="${common.id}">
							<td>${vs.count }</td>
							<td>${common.name}</td>
							<td>${common.key}</td>
							<td>${common.remark}</td>
							<td><c:if test="${common.status == 1}">启用</c:if><c:if test="${common.status == 0}">禁用</c:if></td>
							<td>
								<a class="btnEdit" href="Main/Common/dir_updateip/${common.id}" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="common_update" title="关键字修改" width="420" height="225">修改</a>
								<a class="btnDel" href="Main/Common/dir_delete/${common.id}" target="ajaxTodo" title="确定删除该选项">删除</a>
							</td>
						</tr>			
					</c:forEach>		
				</c:when>
				<c:otherwise>
					<tr align="center">
						<td colspan="6" style="color: red;">无任何记录！</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>共
			<c:choose>
				<c:when test="${! empty commons}">${fn:length(commons)}</c:when>
				<c:otherwise>0</c:otherwise>
			</c:choose>条记录</span>
		</div>
	</div>
</div>