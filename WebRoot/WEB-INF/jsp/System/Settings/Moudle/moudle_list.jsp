<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent" style="border-left:1px #B8D0D6 solid;border-right:1px #B8D0D6 solid">
	<div class="panelBar" style="border-bottom: 0; height: 23px; line-height: 23px; font-weight: bold;">
		<c:choose>
			<c:when test="${0!=moudleId}">模块名称：${moudle.name}</c:when>
			<c:otherwise>模块名称：系统功能模块</c:otherwise>
		</c:choose>
	</div>
	<div class="panelBar">
		<ul class="toolBar">
			<c:if test="${pert:hasperti(applicationScope.saveupdate, loginModel.limit)}">
				<li><a class="add" href="Main/Moudle/saveupdateip?mpId=${moudleId}" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="useradd" title="模块信息添加" width="600" height="300"><span>子模块添加</span></a></li>
				<li><a class="edit" href="Main/Moudle/saveupdateip/{m_id}" target="dialog" mask="true" drawable="true" resizable="true" maxable="true" rel="userupdate" title="模块信息修改" width="600" height="360"><span>子模块修改</span></a></li>
			</c:if>
			<c:if test="${flag}">
				<li><a title="确定添加？" class="add" href="Main/Moudle/addFunc/${moudleId}-0" target="ajaxTodo"><span>增删改</span></a></li>
				<li><a title="确定添加？" class="add" href="Main/Moudle/addFunc/${moudleId}-1" target="ajaxTodo"><span>增删改+批删</span></a></li>
			</c:if>
		</ul>
	</div>
	<div class="panelBar" style="border-top: 0; height: 23px; line-height: 23px; font-weight: bold; text-align: center;">子功能模块信息</div>
	<table class="table" width="100%" layoutH="106" rel="jbsxBox">
		<thead>
			<tr align="center">
				<th width="60">顺序索引</th >
				<th width="100">模块标识</th>
				<th width="150">模块名称</th>
				<th>模块URL</th>
				<th width="80">当前状态</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${! empty moudles}">
					<c:forEach items="${moudles}" var="m" varStatus="vs">
						<tr align="center" target="m_id" rel="${m.m_id}">
								<td>${m.orderindex}</td>
								<td>${m.mark}</td>
								<td>${m.name}</td>
								<td>${m.address}</td>
								<td>
									<c:choose>
										<c:when test="${m.dbstate=='0'}">启用</c:when>
										<c:otherwise>禁用</c:otherwise>
									</c:choose>
								</td>
							</tr>			
					</c:forEach>		
				</c:when>
				<c:otherwise>
			<tr align="center">
				<td colspan="6" style="color: red;">无任何记录！</td>
			</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>