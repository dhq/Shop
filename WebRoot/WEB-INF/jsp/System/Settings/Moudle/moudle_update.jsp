<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/include/inc.jsp"%>

<div class="pageContent">
	<form method="post" action="Main/Moudle/saveupdate" 
		class="pageForm required-validate" 
		onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
		
			<dl>
				<dt style="width: 100px; text-align: right;">模块标识：</dt>
				<dd><input name="mark" class="required" type="text" style="width: 200px;" 
						size="30" maxlength="30" alt="请输入模块标识，字母和数字组合"
						value="${md.mark}"/>
					<input name="m_id" class="required digits" type="hidden"
						size="30" maxlength="30" value="${md.m_id}" /></dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">模块名称：</dt>
				<dd><input name="name" class="required" type="text" style="width: 200px;" 
						size="20" maxlength="20" alt="请输入模块名称"
						value="${md.name}"/></dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">模块URL：</dt>
				<dd><input name="address" type="text" style="width: 300px;"
						size="50" maxlength="50"
						value="${md.address}"/></dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">顺序索引：</dt>
				<dd><input name="orderindex" class="required digits" type="text" style="width: 200px;"
						size="10" maxlength="5" value="${md.orderindex}"/></dd>
			</dl>
			<dl>
				<dt style="width:100px; text-align: right;">模块状态：</dt>
				<dd>
					<select name="dbstate" class="required combox">
						<option value="">请选择</option>
						<option value="0" 
							<c:if test="${'0'==md.dbstate}">selected="selected"</c:if>>启用</option>
						<option value="1"
							<c:if test="${'1'==md.dbstate}">selected="selected"</c:if>>禁用</option>
					</select>
				</dd>
			</dl>
			<dl>
				<dt style="width: 100px; text-align: right;">页面链接：</dt>
				<dd>
					<select name="openType" class="required digits combox">
						<option value="">请选择</option>
						<option value="0"
							<c:if test="${0==md.openType}">selected="selected"</c:if>>内连接</option>
						<option value="1"
							<c:if test="${1==md.openType}">selected="selected"</c:if>>外连接</option>
						<option value="2"
							<c:if test="${2==md.openType}">selected="selected"</c:if>>系统外连接</option>
					</select>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
