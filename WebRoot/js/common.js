//只刷新dialog或navTab
function reloadAjaxDone(json){
	DWZ.ajaxDone(json);
	if (json.statusCode == DWZ.statusCode.ok){
		if (json.navTabId){
			navTab.reload(json.forwardUrl, {navTabId: json.navTabId});
		} 
		if ("closeCurrent" == json.callbackType) {
			$.pdialog.closeCurrent();
		} else if ("reload" == json.callbackType) {
			$.pdialog.reload(json.forwardUrl,"",json.rel);
		}
	}
}

//外部打开Iframe方式
function iframeAjaxDone(json) {
	DWZ.ajaxDone(json);
	if (json.statusCode == DWZ.statusCode.ok) {
		if (json.navTabId) {
			navTab.reloadFlag(json.navTabId);
		} else {
			var $pagerForm = $("#pagerForm", navTab.getCurrentPanel());
			var args = $pagerForm.size() > 0 ? $pagerForm.serializeArray() : {};
			navTabPageBreak(args, json.rel);
		}
		if ("closeCurrent" == json.callbackType) {
			setTimeout(function() {
				navTab.closeCurrentTab(json.navTabId);
			}, 100);
		} else if ("forward" == json.callbackType) {
			navTab.openTab(json.navTabId, json.forwardUrl, {title : json.title, external : true });
		} else if ("forwardConfirm" == json.callbackType) {
			alertMsg.confirm(json.confirmMsg || DWZ.msg("forwardConfirmMsg"), {
				okCall : function() {
					navTab.openTab(json.navTabId, json.forwardUrl, {title : json.title, external : true });
				}
			});
		} else {
			navTab.getCurrentPanel().find(":input[initValue]").each(function() {
				var initVal = $(this).attr("initValue");
				$(this).val(initVal);
			});
		}
	}
}

//打开高级查询
function showSearch(button,searchBarid,simpleid){
	var simple = "#"+simpleid;
	$(simple).val($(simple).val()=='true'?'false':'true');
	var searchBar = "#"+searchBarid;
	$this = $(button);
	var complex = searchBar+" .complex";
	var oldHeight = $(searchBar).height();
	$this.text($this.text()=='高级查询'?'简单查询':'高级查询');
    $(complex).toggle();  
    var nowHeight = $(searchBar).height();
    var $panel=$this.parents('.unitBox:first').find("[layoutH]").each(function(){  
	    $(this).attr("layoutH",parseInt($(this).attr("layoutH"))+(nowHeight-oldHeight));  
	    $(this).layoutH();  
    	});
    return false; 
}

//打开navTab
function openRWNavTab(url,tabid,title){
	navTab.openTab(tabid, url, { title:title, fresh:false, data:{} });
}

//找开dialog
function openRWDialog(url,dlgId,title,widthpx,heightpx){
	var options = {width:widthpx,height:heightpx,mask:true,resizable:false};
	$.pdialog.open(url, dlgId, title,options);
}

//关闭提示
function closeDialog(){
	return window.confirm("您确认要关闭吗！");
}

/**
 * 按钮提交
 * @param buttonId 按钮ID
 */
exportSubmit = function(buttonId){
    $("#"+buttonId).click();
}

/**
 * 判断字符串是否为空
 * @param strVal 字符串
 * @returns {boolean}
 */
function isNullOrEmpty(strVal) {
    if (strVal == '' || strVal == null || strVal == undefined) {
        return true;
    } else {
        return false;
    }
}

/**
 * 邮件左边树形刷新
 * @param json
 */
function selectedTodoBack(json){
    var $box = $("#mailTree");
    $box.ajaxUrl({
        type : "POST",
        url : "Main/mail/count",
        data : {},
        callback : function() {
            /*$box.initUI();*/
        }
    });
    DWZ.ajaxDone(json);
    if (json.statusCode == DWZ.statusCode.ok){
        if (json.navTabId){ //把指定navTab页面标记为需要“重新载入”。注意navTabId不能是当前navTab页面的
            navTab.reloadFlag(json.navTabId);
        } else { //重新载入当前navTab页面
            navTabPageBreak({}, json.rel);
        }

        if ("closeCurrent" == json.callbackType) {
            $.pdialog.closeCurrent();
        } else if ("forward" == json.callbackType) {
            navTab.reload(json.forwardUrl);
        }
    }
}

/**
 * 点击复选框时改变对应元素的值
 * @param options
 *  reverse : 是否反转
 *  checkBox : 复选框对象
 *  targetObj ：需要改变值的对象
 */
clickCheckBox = function(options){
    options.checkBox.click(function(){
        if(options.reverse == true){
            if(options.checkBox.attr("checked")=="checked") {
                options.targetObj.val("0");
                options.checkBox.attr("checked","checked");
            }
            else options.targetObj.val("1");
        }else{
            if(options.checkBox.attr("checked")=="checked") options.targetObj.val("1");
            else options.targetObj.val("0");
        }
    });
}
