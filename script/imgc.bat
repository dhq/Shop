@echo off
if exist list.txt del list.txt /q
set targetdir=%date:~0,4%%date:~5,2%%date:~8,2%%time:~0,2%%time:~3,2%%time:~6,2%
md .\%targetdir%

set /p input=Please enter target direction:
set "input=%input:"=%"

if "%input%"==":" goto input
if not exist "%input%" goto input
for %%i in ("%input%") do if /i "%%~di"==%%i goto input
pushd %cd%
cd /d "%input%">nul 2>nul || exit
set cur_dir=%cd%
popd
for /f "delims=" %%i in ('dir /b /a-d /s "%input%"') do echo %%~fi>>list.txt
for /f "delims=" %%i in ('dir /b /a-d /s "%input%"') do copy /y "%%~fi" .\%targetdir%
if not exist list.txt goto no_file
start list.txt
pause
exit

:no_file
cls
echo       %cur_dir% there arent single file
pause