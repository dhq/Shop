/*
Navicat MySQL Data Transfer

Source Server         : mysql_localhost
Source Server Version : 50521
Source Host           : localhost:3306
Source Database       : pinionmxshop

Target Server Type    : MYSQL
Target Server Version : 50521
File Encoding         : 65001

Date: 2015-06-24 23:17:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_brand
-- ----------------------------
DROP TABLE IF EXISTS `t_brand`;
CREATE TABLE `t_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '品牌名称',
  `number` varchar(255) DEFAULT NULL COMMENT '品牌编号',
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '品牌更新时间戳',
  `is_delete` varchar(1) DEFAULT '0' COMMENT '是否删除（0：正常,1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_brand
-- ----------------------------
INSERT INTO `t_brand` VALUES ('1', '酷开(coocaa)', '0101', '2015-06-17 23:51:14', '0');
INSERT INTO `t_brand` VALUES ('2', '夏普(SHARP)', '0201', '2015-06-17 23:51:14', '0');
INSERT INTO `t_brand` VALUES ('3', '美的(Midea)', '0301', '2015-06-17 23:51:14', '0');
INSERT INTO `t_brand` VALUES ('4', '奥马(aoma)', '0410', '2015-06-17 23:51:14', '0');
INSERT INTO `t_brand` VALUES ('5', '酷开(coocaa)', '0103', '2015-06-18 20:33:11', '0');
INSERT INTO `t_brand` VALUES ('12', '长虹', '1.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('13', '创维', '11.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('14', '格力', '2.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('15', '志高', '3.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('16', '美的', '4.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('17', '美菱', '5.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('18', '奥马', '10.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('19', '小天鹅', '6.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('20', '派德', '7.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('21', '格兰仕', '8.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('22', '爱德', '9.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_brand` VALUES ('23', '长虹', '1', '2015-06-22 22:40:33', '0');

-- ----------------------------
-- Table structure for t_client_update
-- ----------------------------
DROP TABLE IF EXISTS `t_client_update`;
CREATE TABLE `t_client_update` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `update_type` int(11) DEFAULT NULL COMMENT '更新类型',
  `end_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '上次更新时间的结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_client_update
-- ----------------------------

-- ----------------------------
-- Table structure for t_common
-- ----------------------------
DROP TABLE IF EXISTS `t_common`;
CREATE TABLE `t_common` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `key` varchar(100) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_common
-- ----------------------------
INSERT INTO `t_common` VALUES ('1', '关键字', null, null, '1');
INSERT INTO `t_common` VALUES ('3', '常用意见', 'opinion', '用于流程审批时填写意见', '1');

-- ----------------------------
-- Table structure for t_common_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_common_detail`;
CREATE TABLE `t_common_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_common_detail
-- ----------------------------
INSERT INTO `t_common_detail` VALUES ('1', '3', '同意。', null, '1');
INSERT INTO `t_common_detail` VALUES ('2', '3', '已核。', null, '1');
INSERT INTO `t_common_detail` VALUES ('6', '3', '已阅。', null, '1');

-- ----------------------------
-- Table structure for t_department
-- ----------------------------
DROP TABLE IF EXISTS `t_department`;
CREATE TABLE `t_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbstate` varchar(1) DEFAULT NULL COMMENT '状态 0为启用 1为不启用 第一个 要不启用的状态',
  `dlvl` int(11) DEFAULT NULL COMMENT '部门等级',
  `fname` varchar(255) DEFAULT NULL COMMENT '部门全称',
  `sname` varchar(255) DEFAULT NULL COMMENT '部门简称',
  `d_pid` int(11) DEFAULT NULL,
  `no` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `FK4D9FB7B3C3AA8525` (`d_pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_department
-- ----------------------------
INSERT INTO `t_department` VALUES ('1', '0', null, '销售部', '销售部', '0', '0');
INSERT INTO `t_department` VALUES ('2', '0', '1', '办公室', '办公室', '0', '0');

-- ----------------------------
-- Table structure for t_error_info_log
-- ----------------------------
DROP TABLE IF EXISTS `t_error_info_log`;
CREATE TABLE `t_error_info_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `err_type` varchar(2) DEFAULT NULL COMMENT '错误类型',
  `error_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '发生的时间',
  `url` varchar(256) DEFAULT NULL COMMENT '存放路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_error_info_log
-- ----------------------------
INSERT INTO `t_error_info_log` VALUES ('1', 'bg', '2015-06-18 20:33:11', 'goods/1434630790512.xls');
INSERT INTO `t_error_info_log` VALUES ('2', 'bg', '2015-06-22 22:33:05', 'goods/errorfile_bg_1434983585041.xls');
INSERT INTO `t_error_info_log` VALUES ('3', 'bg', '2015-06-22 22:35:06', 'goods/errorfile_bg_1434983704455.xls');
INSERT INTO `t_error_info_log` VALUES ('4', 'bg', '2015-06-22 22:40:33', 'goods/errorfile_bg_1434984032374.xls');

-- ----------------------------
-- Table structure for t_goods
-- ----------------------------
DROP TABLE IF EXISTS `t_goods`;
CREATE TABLE `t_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL COMMENT '商品简介',
  `model` varchar(255) DEFAULT NULL COMMENT '商品型号',
  `parameter` varchar(255) DEFAULT NULL COMMENT '参数',
  `price` double(9,2) DEFAULT NULL COMMENT '标价',
  `cost_price` double(9,2) DEFAULT NULL COMMENT '成本价',
  `commission` double(5,2) DEFAULT NULL COMMENT '提成百分比',
  `detail` mediumtext COMMENT '详细介绍',
  `stock` int(11) DEFAULT NULL COMMENT '库存量',
  `name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `number` varchar(255) DEFAULT NULL COMMENT '商品编号',
  `type_id` int(11) DEFAULT NULL COMMENT '类型ID',
  `type_name` varchar(255) DEFAULT NULL COMMENT '商品类型名称',
  `type_number` varchar(255) DEFAULT NULL COMMENT '商品类型编号',
  `brand` varchar(255) DEFAULT NULL COMMENT '品牌',
  `brand_id` int(11) DEFAULT NULL COMMENT '品牌ID',
  `brand_number` varchar(255) DEFAULT NULL COMMENT '品牌编号',
  `picture_count` int(5) DEFAULT '0' COMMENT '图片数',
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '商品更新时间戳',
  `is_delete` varchar(1) DEFAULT '0' COMMENT '是否删除（0：正常,1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_goods
-- ----------------------------
INSERT INTO `t_goods` VALUES ('1', '50英寸智能酷开系统 八核', 'K50J', '屏幕尺寸 50’ 屏幕分辨率 全高清（1920x1080）', '4799.00', '2500.00', '5.00', '我是详细介绍', '0', '创维酷开(coocaa)K50J 50英寸智能酷开系统 八核', '0101101001', '1', '电视', '01', '酷开(coocaa)', '1', '0101', '0', '2015-06-08 00:00:00', '0');
INSERT INTO `t_goods` VALUES ('7', '日本原装液晶屏，安卓4.2操作系统，应用软件随心下载，海量电影在线点播！', 'LCD-52DS52A', '尺寸：50-52英寸3D：不支持分辨率：全高清（1920*1080）', '8999.00', '3005.00', '10.00', '夏普详细介绍', '0', '夏普(SHARP)LCD-52DS52A 52英寸安卓智能液晶电视 日本原装屏', '0100201002', '1', '电视', '01', '夏普(SHARP)', '2', '0201', '0', '2015-06-08 00:00:00', '0');
INSERT INTO `t_goods` VALUES ('8', '独立三温区，节能省电！低温补偿，全年新鲜！', 'BCD-206TM(E)', '控温方式：机械控温制冷方式：直冷能效等级：一级总容积：151-210L门款式：三门', '1999.00', '988.00', '6.00', '美的详细介绍', '0', '美的（Midea） BCD-206TM(E) 206升 三门冰箱（闪白银）', '0203301001', '3', '冰箱', '02', '美的（Midea）', '3', '0301', '0', '2015-06-08 00:00:00', '0');
INSERT INTO `t_goods` VALUES ('9', '40英寸智能酷开系统 八核', 'K40J', '屏幕尺寸 40’ 屏幕分辨率 全高清（1920x1080）', '5999.00', '2500.00', '5.00', '酷开详细介绍', '12', '创维酷开(coocaa)K40J 40英寸智能酷开系统 八核', '0101101002', '1', '电视', '01', '酷开(coocaa)', '1', '0101', '0', '2015-06-08 00:00:00', '0');
INSERT INTO `t_goods` VALUES ('10', '30英寸智能酷开系统 八核', 'K30J', '屏幕尺寸 30’ 屏幕分辨率 全高清（1280x720）', '8999.00', '2000.00', '5.00', '酷开详细介绍', '30', '创维酷开(coocaa)K30J 30英寸智能酷开系统 八核', '0101101003', '1', '电视', '01', '酷开(coocaa)', '1', '0101', '0', '2015-06-08 00:00:00', '0');
INSERT INTO `t_goods` VALUES ('12', '奥马冰箱', 'K30J', '80kg很大很无敌', '11999.00', '12000.00', '5.00', '奥马详细介绍', '10', '奥马八核智能冰箱', '0410199001', '1', '冰箱', '02', '奥马(aoma)', '1', '0101', '0', '2015-06-08 00:00:00', '0');

-- ----------------------------
-- Table structure for t_goods_picture
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_picture`;
CREATE TABLE `t_goods_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_number` varchar(255) DEFAULT NULL COMMENT '商品编号',
  `url` varchar(255) DEFAULT NULL COMMENT '图片url',
  `pic_name` varchar(255) DEFAULT NULL COMMENT '图片文件名',
  `pic_type` varchar(255) DEFAULT NULL COMMENT '图片类型(展示:zs,详情:xq,参数:cs,推荐:tj,热点:rd)',
  `pic_index` int(11) DEFAULT NULL COMMENT '图片序号',
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '订单更新时间戳',
  `is_delete` varchar(1) DEFAULT '0' COMMENT '是否删除（0：正常,1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_goods_picture
-- ----------------------------
INSERT INTO `t_goods_picture` VALUES ('1', '0101101001', 'File/images/goods/0101101001_xq_1.jpg', '0101101001_xq_1.jpg', 'xq', '1', '2015-06-23 22:52:01', '0');
INSERT INTO `t_goods_picture` VALUES ('2', '0101101001', 'File/images/goods/0101101001_xq_2.jpg', '0101101001_xq_2.jpg', 'xq', '2', '2015-06-23 22:52:10', '0');

-- ----------------------------
-- Table structure for t_goods_type
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_type`;
CREATE TABLE `t_goods_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '类型名称',
  `number` varchar(255) DEFAULT NULL COMMENT '类型编号',
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '商品类型更新时间戳',
  `is_delete` varchar(1) DEFAULT '0' COMMENT '是否删除（0：正常,1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_goods_type
-- ----------------------------
INSERT INTO `t_goods_type` VALUES ('1', '电视', '01', '2015-06-15 23:01:22', '0');
INSERT INTO `t_goods_type` VALUES ('3', '冰箱', '02', '2015-06-15 12:32:11', '0');
INSERT INTO `t_goods_type` VALUES ('4', '电视', '1.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('5', '空调', '2.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('6', '冰箱', '3.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('7', '冰柜', '4.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('8', '洗衣机', '5.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('9', '烟机/灶具', '6.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('10', '消毒柜/洗碗机', '7.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('11', '电压力锅', '8.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('12', '电饭煲', '9.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('13', '微波炉', '10.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('14', '电磁炉', '11.0', '2015-06-22 22:35:05', '0');
INSERT INTO `t_goods_type` VALUES ('15', '电视', '1', '2015-06-22 22:40:33', '0');

-- ----------------------------
-- Table structure for t_goods_type_picture
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_type_picture`;
CREATE TABLE `t_goods_type_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_type_number` varchar(255) DEFAULT NULL COMMENT '商品类型编号',
  `url` varchar(255) DEFAULT NULL COMMENT '图片url',
  `pic_name` varchar(255) DEFAULT NULL COMMENT '图片文件名',
  `pic_type` varchar(255) DEFAULT NULL COMMENT '图片类型(现在暂时为空)',
  `pic_index` int(11) DEFAULT NULL COMMENT '图片序号',
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间戳',
  `is_delete` varchar(1) DEFAULT '0' COMMENT '是否删除（0：正常,1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_goods_type_picture
-- ----------------------------

-- ----------------------------
-- Table structure for t_loginlog
-- ----------------------------
DROP TABLE IF EXISTS `t_loginlog`;
CREATE TABLE `t_loginlog` (
  `sessionId` varchar(32) NOT NULL,
  `dlms` varchar(200) DEFAULT NULL,
  `dltime` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `loDate` datetime DEFAULT NULL,
  `u_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`sessionId`),
  KEY `FKCB4D6CD5BE3B04A0` (`u_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_loginlog
-- ----------------------------
INSERT INTO `t_loginlog` VALUES ('070B1CA9BAEF5357389ECA745512047C', '正常', '2015-06-18 23:13:19', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('0D933C8AE3B91B9D41FAC6CD8E89A02C', '正常', '2015-06-22 15:35:11', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('1384FD95C91ED9701E59BDBBEAD178BE', '正常', '2015-06-20 20:59:41', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('147B1C68B0768D67BC23BDF5D7892658', '正常', '2015-06-22 15:00:05', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('1A4F7C4C84A0528F01F6E20083A47685', '正常', '2015-06-22 21:53:00', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('1F258F1ED0C078D137127A6C6F1F64C0', '注销', '2015-06-23 22:06:36', '127.0.0.1', '2015-06-23 22:07:58', '1');
INSERT INTO `t_loginlog` VALUES ('206F54F3ADB38D67987A1B50488D58B8', '正常', '2015-06-20 21:48:05', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('20EC4EE6412B188CFDE5EFC9424CACCD', '正常', '2015-06-24 22:09:36', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('24E09B5F43C77236E2473D1D09EC6091', '正常', '2015-06-18 20:17:45', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('25D8591D5C6DCCF74FBFB6F6AB1EE65D', '正常', '2015-06-22 16:11:55', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('25EE2C9B5B18659C47A28A6C91064743', '正常', '2015-06-23 22:48:58', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('26D5FE1E3A8A5D1E4E8B7D4745DF9158', '正常', '2015-06-22 17:20:52', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('27B8D8D6A2D1AC5F6E219BF107571056', '正常', '2015-06-22 22:26:38', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('296C37575062572E53E7ABCD46FE37A0', '正常', '2015-06-22 18:34:31', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('2CB772EC777452063BC9FB21CD7DE3F2', '正常', '2015-06-20 13:48:53', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('2D35F33C13094974D0D615D86452303E', '正常', '2015-06-19 23:46:09', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('2E0429CE9FA052D3A5E3BA1921B8C263', '正常', '2015-06-20 21:02:19', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('312332E44D4725A5CAED85FF054D221D', '正常', '2015-06-18 21:13:40', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('3367DF5AE688C3B3A8A9B3CC8283BBA8', '正常', '2015-06-24 21:13:43', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('3840389C8A117AE2765EB63520CEE117', '正常', '2015-06-20 00:37:58', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('397A0BA3B869F3198806F4EECF6E4576', '正常', '2015-06-18 00:05:11', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('4061FE56EE0A44D9C39444491C85F96E', '正常', '2015-06-20 22:31:43', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('414D63B5AD7AFEB4F56F192DA96FB528', '正常', '2015-06-24 22:01:54', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('537AAE0FDD9CFB0753B85502B8D29ED1', '正常', '2015-06-20 16:44:30', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('53D73F486C7ACF2B1BB2693B7C13806F', '正常', '2015-06-22 17:22:39', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('53EB17CE48D998EF13B4A9FCC8A4A135', '正常', '2015-06-22 16:07:41', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('5563929B8EF5B3DE01F2CBEA4DADC755', '正常', '2015-06-18 20:59:32', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('562B39F3329F8B5B2AF91DCEA0001F6C', '正常', '2015-06-20 21:52:43', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('5C5504B90FD1E31E8B9C569286A8BFE0', '正常', '2015-06-23 21:13:08', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('64C95490FBF3612288B8F62081634F7B', '正常', '2015-06-22 22:40:20', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('6FA148F387EB870B492AF0F156A6772F', '正常', '2015-06-20 22:33:14', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('774C2E77A5CC532EF77EC612941C7183', '正常', '2015-06-20 21:53:14', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('7924E1DA8D29FEA3DFD6B69383998C2F', '正常', '2015-06-21 00:38:07', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('7EC44BB56A82F7F3DCFB6E36C77B1CDC', '正常', '2015-06-18 21:13:53', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('83E6D25C8A3E3EBFD6DFD58C532E3B0E', '正常', '2015-06-24 21:44:42', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('87F096F8636A38C23B4DCFA449985922', '正常', '2015-06-22 11:45:45', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('89F06494885BD8F07F16C97F4B523C39', '正常', '2015-06-22 11:37:20', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('89F55F07575F2F6D1C85042BD0866934', '正常', '2015-06-18 21:18:19', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('8A8F39143EC1774462E04C562320A7A8', '正常', '2015-06-18 23:34:52', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('93F4FB7531F0881CD5B7A582EC3D0B03', '正常', '2015-06-21 00:22:13', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('95C1DA4B1584CE8E61C5617CC89EE7D7', '正常', '2015-06-19 23:35:31', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('9749A3BDDB4CE676E65CD00A7970561E', '正常', '2015-06-22 17:17:57', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('9D48E1FB3FF89C46ECB792FE412D2DC5', '正常', '2015-06-17 23:52:42', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('9D51864357C4D9B0102B1C97D42B0A4E', '正常', '2015-06-20 21:16:02', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('A40B3931AA063CF52AFB03DB3AC9E272', '正常', '2015-06-20 14:27:25', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('AC6DD7AF69C5D736E98CABB49277E631', '正常', '2015-06-19 22:46:35', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('AE1767C435DFB24B9CEF78094041347D', '正常', '2015-06-24 21:57:03', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('BC60A34E355306BE0FAB01C90E506CB9', '正常', '2015-06-17 23:56:23', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('C19D47EB993347388C13D4174F694136', '正常', '2015-06-22 16:15:06', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('C4D89EC4699A2B126EAA117742C34A84', '正常', '2015-06-18 22:03:53', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('C572BFC6F5F0CF2E37B19CBC541A51E8', '正常', '2015-06-18 23:51:57', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('D6660A0E5CF5D38974FDDD557572997B', '正常', '2015-06-23 22:08:00', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('D67118740D3580448EB3E1FBADFDBA61', '正常', '2015-06-22 20:39:27', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('DA2C3FE63A6980EC170362439C4843F0', '正常', '2015-06-22 21:29:08', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('DDB495AAE869591356D6815AE44045CC', '正常', '2015-06-21 00:31:27', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('EBAFA6B3C39668079773321CE9F1071C', '正常', '2015-06-22 14:39:34', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('EBC059826B7B7B60F7B8D5C442A6699A', '正常', '2015-06-22 22:34:51', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('EC51646C8F92916D7F72DD45CBD898EA', '正常', '2015-06-22 16:35:57', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('EDFDC181F7DE4169E15B83178DF84827', '正常', '2015-06-20 21:53:55', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('EE339826D70F484ECD01950A37C205BF', '正常', '2015-06-20 00:56:59', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('EEE2EBCB9CC48E3DCC0001B82B8D0732', '正常', '2015-06-22 12:16:47', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('F6CDC3740C3FBB657E116391E7665404', '正常', '2015-06-24 22:14:39', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('F6F84368A526176D657EA347D05DE597', '正常', '2015-06-18 23:59:49', '127.0.0.1', null, '1');
INSERT INTO `t_loginlog` VALUES ('F7C099414304D33E533C6499CAA23FA2', '正常', '2015-06-20 12:04:30', '127.0.0.1', null, '1');

-- ----------------------------
-- Table structure for t_moudle
-- ----------------------------
DROP TABLE IF EXISTS `t_moudle`;
CREATE TABLE `t_moudle` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_pid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `mark` varchar(30) DEFAULT NULL,
  `islast` varchar(1) NOT NULL,
  `openType` int(1) NOT NULL DEFAULT '0' COMMENT '0:内页打开,1:IFrame方式打开(外部系统),2:IFrame方式打开(本系统)',
  `orderindex` int(11) NOT NULL,
  `dbstate` varchar(1) NOT NULL,
  PRIMARY KEY (`m_id`),
  KEY `FK3D305F35B1254A57` (`m_pid`) USING BTREE,
  CONSTRAINT `t_moudle_ibfk_1` FOREIGN KEY (`m_pid`) REFERENCES `t_moudle` (`m_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_moudle
-- ----------------------------
INSERT INTO `t_moudle` VALUES ('1', null, '系统维护', '', 'manager_menu', '0', '0', '99', '0');
INSERT INTO `t_moudle` VALUES ('2', '1', '权限管理', '', 'rightManage', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('3', '1', '日志管理', '', 'logManage', '0', '0', '2', '0');
INSERT INTO `t_moudle` VALUES ('4', '1', '系统管理', '', 'systemManage', '0', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('5', '2', '角色管理', 'Main/QxRole/main', 'rolemg', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('6', '5', '角色添加', 'Main/QxRole/add', 'qxRoleadd', '1', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('7', '5', '角色修改', 'Main/QxRole/update', 'qxRoleupdate', '1', '0', '2', '0');
INSERT INTO `t_moudle` VALUES ('8', '5', '角色删除', 'Main/QxRole/delete', 'qxRoledelete', '1', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('9', '5', '权限分配', 'Main/QxRole/perset', 'qxRoleperset', '1', '0', '4', '0');
INSERT INTO `t_moudle` VALUES ('11', '2', '用户管理', 'Main/QxUser/main', 'usermg', '0', '0', '2', '0');
INSERT INTO `t_moudle` VALUES ('12', '11', '用户添加', 'Main/QxUser/add', 'qxUseradd', '1', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('13', '11', '用户修改', 'Main/QxUser/update', 'qxUserupdate', '1', '0', '2', '0');
INSERT INTO `t_moudle` VALUES ('14', '11', '用户删除', 'Main/QxUser/delete', 'qxUserdelete', '1', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('15', '11', '用户查看', 'Main/QxUser/view', 'qxUserview', '1', '0', '5', '0');
INSERT INTO `t_moudle` VALUES ('16', '11', '角色分配', 'Main/QxUser/roleset', 'qxUserroleset', '1', '0', '4', '0');
INSERT INTO `t_moudle` VALUES ('17', '3', '登录日志', 'Main/Loglogin/main', 'loglogin', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('19', '4', '功能模块', 'Main/Moudle/main', 'moudle', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('20', '19', '模块设置', 'Main/Moudle/saveupdate', 'saveupdate', '1', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('25', '2', '部门管理', 'Main/QxDept/main', 'deptmg', '0', '0', '3', '1');
INSERT INTO `t_moudle` VALUES ('26', '2', '职位管理', 'Main/QxPosition/main', 'positionmg', '0', '0', '4', '1');
INSERT INTO `t_moudle` VALUES ('27', '4', '公共信息', 'Main/Common/main', 'common', '0', '0', '2', '0');
INSERT INTO `t_moudle` VALUES ('28', '25', '部门添加', 'Main/QxDept/add', 'qxDeptadd', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('29', '25', '部门修改', 'Main/QxDept/update', 'qxDeptupdate', '0', '0', '2', '0');
INSERT INTO `t_moudle` VALUES ('30', '25', '部门删除', 'Main/QxDept/delete', 'qxDeptdelete', '0', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('31', '26', '职位添加', 'Main/QxPosition/add', 'qxPositionadd', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('32', '26', '职位修改', 'Main/QxPosition/update', 'qxPositionupdate', '0', '0', '2', '0');
INSERT INTO `t_moudle` VALUES ('33', '26', '职位删除', 'Main/QxPosition/del', 'qxPositiondel', '0', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('34', '27', '公共信息添加', 'Main/common/add', 'common_add', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('35', '27', '公共信息修改', 'Main/common/update', 'common_update', '0', '0', '2', '0');
INSERT INTO `t_moudle` VALUES ('36', '27', '公共信息删除', 'Main/common/delete', 'common_delete', '0', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('37', '27', '信息目录添加', 'Main/common/dir_add', 'common_dir_add', '0', '0', '4', '0');
INSERT INTO `t_moudle` VALUES ('38', '27', '信息目录修改', 'Main/common/dir_update', 'common_dir_update', '0', '0', '5', '0');
INSERT INTO `t_moudle` VALUES ('39', '27', '信息目录删除', 'Main/common/dir_delete', 'common_dir_delete', '0', '0', '6', '0');
INSERT INTO `t_moudle` VALUES ('40', '27', '列表显示', 'Main/common/main/list', 'common_list', '0', '0', '7', '0');
INSERT INTO `t_moudle` VALUES ('41', null, '业务管理', '', 'business_menu', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('42', '41', '商品信息', '', 'goods_menu', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('43', '42', '商品类型', 'Main/GoodsType/main', 'GoodsType', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('44', '42', '商品', 'Main/Goods/main', 'Goods', '0', '0', '11', '0');
INSERT INTO `t_moudle` VALUES ('45', '43', '添加商品类型', 'Main/GoodsType/add', 'addGoodsType', '1', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('46', '43', '修改商品类型', 'Main/GoodsType/update', 'updateGoodsType', '1', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('47', '43', '删除商品类型', 'Main/GoodsType/delete', 'deleteGoodsType', '1', '0', '5', '0');
INSERT INTO `t_moudle` VALUES ('48', '44', '添加商品', 'Main/Goods/add', 'addGoods', '1', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('49', '44', '修改商品', 'Main/Goods/update', 'updateGoods', '1', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('50', '44', '删除商品', 'Main/Goods/delete', 'deleteGoods', '1', '0', '5', '0');
INSERT INTO `t_moudle` VALUES ('51', '44', '批量添加商品', 'Main/Goods/batchAddGoods', 'batchAddGoods', '0', '0', '7', '0');
INSERT INTO `t_moudle` VALUES ('52', '44', '批量修改商品', 'Main/Goods/batchUpdateGoods', 'batchUpdateGoods', '0', '0', '9', '0');
INSERT INTO `t_moudle` VALUES ('53', '44', '批量删除商品', 'Main/Goods/batchDeleteGoods', 'batchDeleteGoods', '0', '0', '11', '0');
INSERT INTO `t_moudle` VALUES ('54', '41', '模板下载', '', 'template_menu', '0', '0', '21', '0');
INSERT INTO `t_moudle` VALUES ('55', '54', '模板下载', 'Main/Template/main', 'Template', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('56', '42', '品牌', 'Main/Brand/main', 'Brand', '0', '0', '5', '0');
INSERT INTO `t_moudle` VALUES ('57', '56', '添加品牌', 'Main/Brand/add', 'addBrand', '1', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('58', '56', '修改品牌', 'Main/Brand/update', 'updateBrand', '1', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('59', '56', '删除品牌', 'Main/Brand/delete', 'deleteBrand', '1', '0', '5', '0');
INSERT INTO `t_moudle` VALUES ('60', '41', '订单信息', '', 'order_menu', '0', '0', '11', '0');
INSERT INTO `t_moudle` VALUES ('61', '60', '订单管理', 'Main/Order/main', 'Order', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('62', '61', '添加订单', 'Main/Order/add', 'addOrder', '1', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('63', '61', '修改订单', 'Main/Order/update', 'updateOrder', '1', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('64', '61', '删除订单', 'Main/Order/delete', 'deleteOrder', '1', '0', '5', '0');
INSERT INTO `t_moudle` VALUES ('65', '61', '取消订单', 'Main/Order/cancel', 'cancelOrder', '0', '0', '7', '0');
INSERT INTO `t_moudle` VALUES ('66', '61', '改订单为已付款', 'Main/Order/paid', 'paidOrder', '0', '0', '9', '0');
INSERT INTO `t_moudle` VALUES ('67', '61', '将订单改为已发货', 'Main/Order/delivered', 'deliveredOrder', '0', '0', '11', '0');
INSERT INTO `t_moudle` VALUES ('68', '61', '将订单改为已收货', 'Main/Order/received', 'receivedOrder', '0', '0', '13', '0');
INSERT INTO `t_moudle` VALUES ('69', '61', '将订单改为已完成', 'Main/Order/finished', 'finishedOrder', '0', '0', '15', '0');
INSERT INTO `t_moudle` VALUES ('70', '61', '订单详情', 'Main/Order/detail', 'orderDetail', '0', '0', '17', '0');
INSERT INTO `t_moudle` VALUES ('71', '41', '销售信息管理', '', 'sales_menu', '0', '0', '15', '0');
INSERT INTO `t_moudle` VALUES ('72', '71', '分品牌类型统计', 'Main/BrandTypeStatistics/main', 'BrandTypeStatistics', '0', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('80', '44', '批量添加商品图片', 'Main/Goods/batchImageUpload', 'batchImageUpload', '0', '0', '13', '0');
INSERT INTO `t_moudle` VALUES ('81', '42', '区域价格', 'Main/Goods/regionPrice', 'RegionPrice', '0', '0', '13', '0');
INSERT INTO `t_moudle` VALUES ('82', '42', '区域代码', 'Main/Goods/regionCode', 'RegionCode', '0', '0', '15', '0');
INSERT INTO `t_moudle` VALUES ('90', '41', '业务日志', '', 'buss_log_menu', '0', '0', '13', '0');
INSERT INTO `t_moudle` VALUES ('91', '90', '错误反馈信息下载', 'Main/BizLog/errInfoDownload', 'errInfoDownload', '0', '0', '17', '0');
INSERT INTO `t_moudle` VALUES ('92', '81', '添加区域价格', 'Main/Goods/regionPadd', 'addRegionPrice', '1', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('93', '81', '修改区域价格', 'Main/Goods/regionPupdate', 'updateRegionPrice', '1', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('94', '81', '删除区域价格', 'Main/Goods/regionPdelete', 'deleteRegionPrice', '1', '0', '5', '0');
INSERT INTO `t_moudle` VALUES ('95', '82', '添加区域代码', 'Main/Goods/regionadd', 'addRegionCode', '1', '0', '1', '0');
INSERT INTO `t_moudle` VALUES ('96', '82', '修改区域代码', 'Main/Goods/regionupdate', 'updateRegionCode', '1', '0', '3', '0');
INSERT INTO `t_moudle` VALUES ('97', '82', '删除区域代码', 'Main/Goods/regiondelete', 'deleteRegionCode', '1', '0', '5', '0');
INSERT INTO `t_moudle` VALUES ('98', '44', '浏览商品图片', 'Main/Goods/goodsPictureView', 'goodsPictureView', '0', '0', '15', '0');
INSERT INTO `t_moudle` VALUES ('99', '44', '上传商品图片', 'Main/Goods/goodsPictureUpload', 'goodsPictureUpload', '0', '0', '17', '0');
INSERT INTO `t_moudle` VALUES ('100', '44', '删除商品图片', 'Main/Goods/goodsPictureDelete', 'goodsPictureDelete', '0', '0', '21', '0');
INSERT INTO `t_moudle` VALUES ('101', '44', '修改商品图片', 'Main/Goods/goodsPictureUpdate', 'goodsPictureUpdate', '0', '0', '19', '0');
INSERT INTO `t_moudle` VALUES ('102', '43', '浏览商品类型图片', 'Main/GoodsType/goodsTypePictureView', 'goodsTypePictureView', '0', '0', '7', '0');
INSERT INTO `t_moudle` VALUES ('103', '43', '上传商品类型图片', 'Main/GoodsType/goodsTypePictureUpload', 'goodsTypePictureUpload', '0', '0', '9', '0');
INSERT INTO `t_moudle` VALUES ('104', '43', '删除商品类型图片', 'Main/GoodsType/goodsTypePictureDelete', 'goodsTypePictureDelete', '0', '0', '11', '0');
INSERT INTO `t_moudle` VALUES ('105', '44', '浏览特殊图片', 'Main/Goods/specialPictureView', 'specialPictureView', '0', '0', '23', '0');
INSERT INTO `t_moudle` VALUES ('106', '44', '上传特殊图片', 'Main/Goods/specialPictureUpload', 'specialPictureUpload', '0', '0', '25', '0');
INSERT INTO `t_moudle` VALUES ('107', '44', '修改特殊图片', 'Main/Goods/specialPictureUpdate', 'specialPictureUpdate', '0', '0', '27', '0');
INSERT INTO `t_moudle` VALUES ('108', '44', '删除特殊图片', 'Main/Goods/specialPictureDelete', 'specialPictureDelete', '0', '0', '29', '0');

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(255) DEFAULT NULL COMMENT '流水号',
  `customer_name` varchar(255) DEFAULT NULL COMMENT '客户姓名',
  `customer_contacts` varchar(255) DEFAULT NULL COMMENT '客户联系方式',
  `receiver_address` varchar(255) DEFAULT NULL COMMENT '收货地址',
  `status` int(2) DEFAULT '1' COMMENT '订单状态,1：未提交、2：已提交、3：已发货、4：已收货、5：已完成',
  `salesman_id` int(11) DEFAULT NULL COMMENT '销售员用户ID',
  `salesman_name` varchar(255) DEFAULT NULL COMMENT '销售员姓名',
  `salesman_staff_id` varchar(255) DEFAULT NULL COMMENT '销售员工号',
  `create_time` datetime DEFAULT NULL COMMENT '订单形成时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `number` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `paid` char(1) DEFAULT '0' COMMENT '已付款，1是已付款，0是未付款',
  `amount` double(9,2) DEFAULT NULL COMMENT '订单金额合计',
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '订单更新时间戳',
  `canceled` varchar(1) DEFAULT '0' COMMENT '是否删除（0：正常,1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES ('2', '201505112', '客户一', '13856885988', '深圳市南山区高新科技园北区腾讯大厦一楼', '5', '3', '张三', '00003', '2015-05-11 23:22:30', '2015-05-11 23:49:42', '1000001713253128', '0', null, '2015-06-16 12:31:11', '0');

-- ----------------------------
-- Table structure for t_order_item
-- ----------------------------
DROP TABLE IF EXISTS `t_order_item`;
CREATE TABLE `t_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `transaction_value` double(9,2) DEFAULT NULL COMMENT '商品成交价',
  `goods_number` varchar(255) DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `goods_count` int(5) DEFAULT NULL COMMENT '商品个数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` int(11) DEFAULT NULL COMMENT '创建人ID',
  `brand_number` varchar(255) DEFAULT NULL COMMENT '所属品牌编号',
  `brand_name` varchar(255) DEFAULT NULL COMMENT '所属品牌名称',
  `good_type_number` varchar(255) DEFAULT NULL COMMENT '所属商品类型编号',
  `good_type_name` varchar(255) DEFAULT NULL COMMENT '所属商品类型名称',
  `commission` double(9,2) DEFAULT NULL COMMENT '商品成本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_order_item
-- ----------------------------
INSERT INTO `t_order_item` VALUES ('1', '2', '4799.00', '0101101001', '创维酷开(coocaa)K50J 50英寸智能酷开系统 八核', '50', '2015-05-12 22:15:40', '3', '0101', '酷开(coocaa)', '01', '电视', '970.00');
INSERT INTO `t_order_item` VALUES ('2', '2', '1999.00', '0203301001', '美的（Midea） BCD-206TM(E) 206升 三门冰箱（闪白银）', '20', '2015-05-12 22:18:14', '4', '0301', '美的(Midea)', '02', '冰箱', '700.00');

-- ----------------------------
-- Table structure for t_order_total
-- ----------------------------
DROP TABLE IF EXISTS `t_order_total`;
CREATE TABLE `t_order_total` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` date DEFAULT NULL COMMENT '日期',
  `total` int(10) DEFAULT NULL COMMENT '订单数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_order_total
-- ----------------------------
INSERT INTO `t_order_total` VALUES ('1', '2015-05-10', '1');
INSERT INTO `t_order_total` VALUES ('2', '2015-05-11', '2');

-- ----------------------------
-- Table structure for t_position
-- ----------------------------
DROP TABLE IF EXISTS `t_position`;
CREATE TABLE `t_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(20) DEFAULT NULL COMMENT '职位序号',
  `pname` varchar(20) DEFAULT NULL COMMENT '职位名称',
  `status` char(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_position
-- ----------------------------
INSERT INTO `t_position` VALUES ('1', '11', '总经理', '1');
INSERT INTO `t_position` VALUES ('2', '12', '副总经理', '1');
INSERT INTO `t_position` VALUES ('10', '13', '职员', '1');

-- ----------------------------
-- Table structure for t_region_code
-- ----------------------------
DROP TABLE IF EXISTS `t_region_code`;
CREATE TABLE `t_region_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) DEFAULT NULL COMMENT '地区码',
  `descript` varchar(30) DEFAULT NULL COMMENT '地区描述',
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '地区编码更新时间戳',
  `is_delete` varchar(1) DEFAULT '0' COMMENT '是否删除（0：正常,1：删除）',
  PRIMARY KEY (`id`),
  KEY `FKA9D77387BE38BBBB` (`code`,`descript`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_region_code
-- ----------------------------
INSERT INTO `t_region_code` VALUES ('1', '420100', '武汉市', '2015-06-15 23:11:21', '0');
INSERT INTO `t_region_code` VALUES ('2', '420500', '宜昌市', '2015-06-05 13:11:21', '0');
INSERT INTO `t_region_code` VALUES ('3', '420600', '襄阳市', '2015-06-12 03:11:21', '0');
INSERT INTO `t_region_code` VALUES ('4', '421000', '广州市', '2015-06-19 22:52:22', '0');

-- ----------------------------
-- Table structure for t_region_price
-- ----------------------------
DROP TABLE IF EXISTS `t_region_price`;
CREATE TABLE `t_region_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_number` varchar(255) DEFAULT NULL COMMENT '商品编号',
  `r_code` varchar(6) DEFAULT NULL COMMENT '地区码',
  `price` double(9,2) DEFAULT NULL COMMENT '标价',
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '地区价格更新时间戳',
  `is_delete` varchar(1) DEFAULT '0' COMMENT '是否删除（0：正常,1：删除）',
  PRIMARY KEY (`id`),
  KEY `FKA9D77387BE38AAAA` (`goods_number`,`r_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_region_price
-- ----------------------------
INSERT INTO `t_region_price` VALUES ('1', '1', '420100', '200.20', '2015-06-15 23:11:21', '0');
INSERT INTO `t_region_price` VALUES ('2', '7', '420500', '700.10', '2015-06-14 13:11:21', '0');
INSERT INTO `t_region_price` VALUES ('3', '2000', '420600', '5001.10', '2015-06-20 12:42:06', '0');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cmlimit` varchar(9999) DEFAULT NULL,
  `dbstate` varchar(1) DEFAULT NULL,
  `ms` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `xtlimit` varchar(9999) DEFAULT NULL,
  `xh` int(11) NOT NULL,
  `dblimit` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,_21', '0', '超级管理员', '超级管理员', '_,_41,_42,_43,_45,_46,_47,_102,_103,_104,_56,_57,_58,_59,_44,_48,_49,_50,_51,_52,_53,_80,_98,_99,_101,_100,_105,_106,_107,_108,_81,_92,_93,_94,_82,_95,_96,_97,_60,_61,_62,_63,_64,_65,_66,_67,_68,_69,_70,_90,_91,_71,_72,_54,_55,_1,_2,_5,_6,_7,_8,_9,_11,_12,_13,_14,_16,_15,_3,_17,_4,_19,_20,_27,_34,_35,_36,_37,_38,_39,_40,', '99', '_6,_7,_8,_9');
INSERT INTO `t_role` VALUES ('2', null, '0', '销售人员', '普通用户', '', '1', '_6,_7,_8');
INSERT INTO `t_role` VALUES ('3', null, '0', '公司管理人员', '管理员', '_,_41,_54,_55,', '2', '_6,_7,_8,_9');
INSERT INTO `t_role` VALUES ('4', null, '0', '系统管理员', '系统管理员', '_,_41,_42,_43,_45,_46,_47,_44,_81,_82,_48,_49,_50,_51,_52,_53,_80,_54,_55,_1,_2,_11,_12,_13,_14,_16,_15,_90,_91', '3', null);

-- ----------------------------
-- Table structure for t_special_picture
-- ----------------------------
DROP TABLE IF EXISTS `t_special_picture`;
CREATE TABLE `t_special_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_number` varchar(255) DEFAULT NULL COMMENT '商品编号',
  `url` varchar(255) DEFAULT NULL COMMENT '图片url',
  `pic_name` varchar(255) DEFAULT NULL COMMENT '图片文件名',
  `pic_type` varchar(255) DEFAULT NULL COMMENT '图片类型(热门：ht,推荐:rd)',
  `pic_index` int(11) DEFAULT NULL COMMENT '图片序号',
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间戳',
  `is_delete` varchar(1) DEFAULT '0' COMMENT '是否删除（0：正常,1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_special_picture
-- ----------------------------
INSERT INTO `t_special_picture` VALUES ('5', '0203301001', 'File/images/special/0203301001_ht_3.jpg', '0203301001_ht_3.jpg', 'ht', '3', '2015-06-24 22:17:23', '0');
INSERT INTO `t_special_picture` VALUES ('6', '0101101001', 'File/images/special/0101101001_ht_2.jpg', '0101101001_ht_2.jpg', 'ht', '2', '2015-06-24 22:16:20', '0');
INSERT INTO `t_special_picture` VALUES ('7', '0101101001', 'File/images/special/0101101001_rd_1.jpg', '0101101001_rd_1.jpg', 'rd', '1', '2015-06-24 22:16:46', '0');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cmlimit` varchar(9999) DEFAULT NULL,
  `dbstate` varchar(1) DEFAULT NULL COMMENT '状态',
  `dlid` varchar(25) NOT NULL COMMENT '登录名',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `pwd` varchar(255) DEFAULT NULL COMMENT '密码',
  `usable` char(1) DEFAULT '1' COMMENT '是否可用，1可用，0禁用',
  `xtlimit` varchar(9999) DEFAULT NULL,
  `d_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `pid` int(11) DEFAULT NULL COMMENT '职位ID',
  `lo` char(1) NOT NULL DEFAULT '1' COMMENT '在职状态',
  `zwxx` varchar(100) DEFAULT NULL COMMENT '职位信息',
  `sex` char(1) DEFAULT '1' COMMENT '性别，1男，2女',
  `staff_id` varchar(25) DEFAULT NULL COMMENT '工号',
  `bank_name` varchar(255) DEFAULT NULL COMMENT '开户行',
  `bank_account` varchar(255) DEFAULT NULL COMMENT '银行账号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dlid` (`dlid`) USING BTREE,
  KEY `FKCB5540D6BE2B5CCB` (`d_id`) USING BTREE,
  CONSTRAINT `t_user_ibfk_2` FOREIGN KEY (`d_id`) REFERENCES `t_department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', '_1,_56,_57,_237,_58,_59,_238,_60,_61,_62,_63,_64,_342,_98,_99,_241,_100,_101,_102,_103,_104,_105,_138,_139,_242,_147,_148,_343,_149,_344,_345,_150,_151,_163,_164,_165,_166,_167,_145,_245,_152,_153,_154,_168,_146,_246,_161,_162,_171,_247,_172,_173,_174,_175,_140,_243,_155,_156,_157,_169,_141,_244,_158,_159,_160,_170,_176,_178,_248,_182,_183,_184,_185,_186,_187,_188,_189,_190,_191,_192,_193,_194,_285,_286,_287,_288,_289,_319,_321,_322,_323,_324,_325,_326,_327,_179,_301,_249,_195,_196,_197,_282,_283,_284,_296,_290,_291,_292,_293,_294,_295,_180,_250,_201,_309,_202,_203,_346,_347,_181,_251,_308,_274,_275,_276,_277,_278,_320,_328,_493,_494,_177,_198,_252,_204,_302,_205,_300,_299,_298,_206,_297,_213,_214,_215,_216,_217,_218,_199,_253,_207,_303,_208,_209,_200,_254,_210,_304,_211,_212,_260,_264,_265,_305,_266,_310,_311,_261,_267,_268,_306,_269,_262,_270,_271,_307,_272,_263,_273,_2,_106,_107,_219,_108,_109,_110,_111,_112,_113,_220,_114,_115,_116,_117,_118,_119,_120,_221,_121,_122,_123,_124,_125,_126,_127,_128,_129,_222,_130,_223,_131,_132,_224,_133,_134,_225,_136,_142,_135,_226,_137,_143,_144,', '0', 'admin', '超级管理员', '1Xp7WjeICh8=', '1', '_1,_56,_57,_237,_58,_59,_238,_60,_61,_62,_63,_64,_342,_98,_99,_241,_100,_101,_102,_103,_104,_105,_138,_139,_242,_147,_148,_343,_149,_150,_151,_163,_164,_165,_166,_167,_145,_245,_152,_153,_154,_168,_146,_246,_161,_162,_171,_247,_172,_173,_174,_175,_140,_243,_155,_156,_157,_169,_141,_244,_158,_159,_160,_170,_176,_178,_248,_182,_183,_184,_185,_186,_187,_188,_189,_190,_191,_192,_193,_194,_285,_286,_287,_288,_289,_179,_301,_249,_195,_196,_197,_282,_283,_284,_296,_290,_291,_292,_293,_294,_295,_180,_250,_201,_309,_202,_203,_347,_181,_251,_274,_275,_276,_277,_278,_493,_494,_177,_198,_252,_204,_302,_205,_300,_299,_298,_206,_297,_213,_214,_215,_216,_217,_218,_199,_253,_207,_208,_209,_200,_254,_210,_211,_212,_260,_264,_265,_266,_261,_267,_268,_269,_262,_270,_271,_272,_263,_273,_354,_355,_356,_362,_363,_364,_365,_366,_367,_375,_376,_377,_378,_379,_380,_381,_382,_383,_384,_385,_386,_387,_388,_389,_390,_391,_392,_393,_394,_395,_396,_397,_398,_399,_400,_401,_402,_403,_404,_405,_406,_407,_408,_409,_410,_411,_412,_413,_414,_415,_416,_417,_418,_419,_420,_421,_422,_368,_423,_369,_370,_424,_371,_425,_426,_372,_373,_427,_374,_434,_435,_436,_437,_438,_439,_440,_441,_442,_443,_444,_445,_446,_447,_448,_449,_450,_451,_357,_358,_359,_360,_361,_428,_429,_430,_431,_432,_433,_2,_106,_107,_219,_108,_109,_110,_111,_112,_113,_220,_114,_115,_116,_117,_118,_119,_120,_221,_121,_122,_123,_124,_125,_126,_127,_128,_129,_222,_130,_223,_131,_132,_224,_133,_134,_225,_136,_142,_135,_226,_137,_143,_144,_3,_4,_5,_6,_7,_8,_9,_227,_10,_11,_12,_228,_13,_14,_229,_15,_16,_17,_18,_19,_20,_21,_22,_230,_23,_24,_25,_26,_27,_28,_29,_231,_30,_31,_32,_33,_34,_35,_36,_232,_37,_38,_39,_40,_41,_42,_233,_43,_44,_45,_46,_47,_48,_234,_49,_50,_51,_52,_53,_54,_235,_55,_236,_65,_66,_67,_68,_69,_255,_70,_71,_72,_73,_74,_75,_76,_77,_78,_256,_79,_80,_81,_82,_83,_84,_85,_86,_87,_258,_88,_89,_90,_91,_257,_92,_93,_94,_239,_95,_96,_97,_240,_259,', '1', '1', '1', '管理所用有户', '1', '00001', '银行', '123456');
INSERT INTO `t_user` VALUES ('2', null, '0', 'root', '系统管理员二', 'DK24VipYCcE=', '1', null, '1', '1', '1', '', '1', '00002', '银行', '123456');
INSERT INTO `t_user` VALUES ('3', null, '0', 'zhangsan', '张三', 'Hfvm3dtXkD8=', '1', null, null, null, '1', null, '2', '00003', '中国银行', '0000000000000000000');
INSERT INTO `t_user` VALUES ('4', null, '0', 'xiaoming', '小明', 'tW6/q77W688=', '0', null, null, null, '1', null, '1', '0004', '中国工商银行', '622212345678');

-- ----------------------------
-- Table structure for t_userroles
-- ----------------------------
DROP TABLE IF EXISTS `t_userroles`;
CREATE TABLE `t_userroles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbstate` varchar(1) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `r_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA9D77387BE383C2E` (`r_id`) USING BTREE,
  KEY `FKA9D77387BE3B04A0` (`u_id`) USING BTREE,
  CONSTRAINT `t_userroles_ibfk_1` FOREIGN KEY (`r_id`) REFERENCES `t_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_userroles
-- ----------------------------
INSERT INTO `t_userroles` VALUES ('1', '0', '1', '1', '1');
INSERT INTO `t_userroles` VALUES ('2', '0', '1', '1', '2');
INSERT INTO `t_userroles` VALUES ('3', '0', '1', '2', '3');
