/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50610
Source Host           : localhost:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2015-05-23 21:58:27
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `t_order`
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(255) DEFAULT NULL COMMENT '流水号',
  `customer_name` varchar(255) DEFAULT NULL COMMENT '客户姓名',
  `customer_contacts` varchar(255) DEFAULT NULL COMMENT '客户联系方式',
  `receiver_address` varchar(255) DEFAULT NULL COMMENT '收货地址',
  `status` int(2) DEFAULT '1' COMMENT '订单状态,1：未提交、2：已提交、3：已发货、4：已收货、5：已完成',
  `salesman_id` int(11) DEFAULT NULL COMMENT '销售员用户ID',
  `salesman_name` varchar(255) DEFAULT NULL COMMENT '销售员姓名',
  `salesman_staff_id` varchar(255) DEFAULT NULL COMMENT '销售员工号',
  `create_time` datetime DEFAULT NULL COMMENT '订单形成时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `number` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `client_number` varchar(255) DEFAULT NULL COMMENT '客户端订单编号',
  `paid` char(1) DEFAULT '0' COMMENT '已付款，1是已付款，0是未付款',
  `amount` double(9,2) DEFAULT NULL COMMENT '订单金额合计',
  `canceled` char(1) DEFAULT '0' COMMENT '是否作废，1是，0不是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES ('2', '201505112', '客户一', '13856885988', '深圳市南山区高新科技园北区腾讯大厦一楼', '5', '1', '超级管理员', '00001', '2015-05-11 23:22:30', '2015-05-11 23:49:42', '1000001713253128', null, '0', null, '0');
