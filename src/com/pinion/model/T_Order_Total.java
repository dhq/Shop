package com.pinion.model;

import com.jfinal.plugin.activerecord.Model;

import java.util.List;

/**
 * 订单流水数分天数统计
 */
public class T_Order_Total extends Model<T_Order_Total> {
    public static final T_Order_Total dao = new T_Order_Total();

    public List<T_Order_Total> list(){
        return dao.find("SELECT a.* FROM t_order_total a ORDER BY a.day DESC");
    }

    /**
     * 根据日期查找订单数
     * @param day 日期字符串
     * @return
     */
    public T_Order_Total getByDay(String day){
        String sql = "SELECT * FROM t_order_total WHERE " +
                "day='" + day+"'";
        return dao.findFirst(sql);
    }

}
