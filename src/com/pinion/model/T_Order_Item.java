package com.pinion.model;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

import java.util.List;

/**
 * 订单商品Model
 */
public class T_Order_Item extends Model<T_Order_Item> {
    public static final T_Order_Item dao = new T_Order_Item();

    /**
     * 根据订单ID查找订单商品
     * @param orderId 订单ID
     * @return
     */
    public List<T_Order_Item> listByOrderId(int orderId){
        return dao.find("SELECT a.* FROM t_order_item a WHERE a.order_id ="+orderId);
    }

    /**
     * 根据订单编号查找订单商品
     * @param orderNumber 订单编号
     * @return
     */
    public List<T_Order_Item> listByOrderNumber(String orderNumber){
        return dao.find("SELECT a.* FROM t_order_item a,t_order b WHERE a.order_id=b.id AND a.number ="+orderNumber);
    }

    /**
     * 根据订单ID统计订单商品记录条数
     * @param orderId 订单ID
     * @return 订单商品记录条数
     */
    public Long countByOrderId(Integer orderId){
        String sql = "SELECT COUNT(id) FROM t_order_item WHERE 1=1";
        if(null != orderId){
            sql = sql +" AND order_id ="+orderId;
        }
        return Db.queryLong(sql);
    }


    /**
     * 按日期统计总销售额或总原价
     * @param date_start 开始日期
     * @param date_end 结束日期
     * @param brandId 品牌ID
     * @param goodsType 商品类型ID
     * @param realPayPrice 是统计原价还是统计销售额
     * @return
     */
    public double count(String date_start, String date_end, Integer brandId, Integer goodsType, int realPayPrice) {
        //TODO 统计
        return 0;
    }

    /**
     * 按日期统计总销售量
     * @param date_start 开始日期
     * @param date_end 结束日期
     * @param brandId 品牌ID
     * @param goodsType 商品类型ID
     * @return
     */
    public long countQuantity(String date_start, String date_end, Integer brandId, Integer goodsType) {
        //TODO 统计
        return 0;
    }
}
