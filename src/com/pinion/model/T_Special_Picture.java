package com.pinion.model;

import com.jfinal.plugin.activerecord.Model;

import java.util.List;


public class T_Special_Picture extends Model<T_Special_Picture> {
    public static final T_Special_Picture dao = new T_Special_Picture();

    public List<T_Special_Picture> list() {
        return dao.find("SELECT a.* FROM T_Special_Picture a WHERE a.is_delete = '0'");
    }

    /**
     * 查询该商品所有特殊图片
     *
     * @param number 商品编号
     */
    public List<T_Special_Picture> getByGoodNumber(String number) {
        String sql = "SELECT * FROM T_Special_Picture WHERE is_delete = '0' AND goods_number = ?";
        return dao.find(sql, number);
    }

    /**
     * 查询是否存在相同url的特殊图片
     *
     * @param url 图片url
     */
    public boolean hasSameUrl(String url) {
        String sql = "SELECT * FROM T_Special_Picture WHERE is_delete = '0' AND url = ?";
        return dao.find(sql, url).size() > 0;
    }

    /**
     * 查询是否存在相同文件名的特殊图片
     *
     * @param picName 图片文件名
     */
    public T_Special_Picture getByName(String picName) {
        String sql = "SELECT * FROM T_Special_Picture WHERE is_delete = '0' AND pic_name=?";
        return dao.findFirst(sql, picName);
    }

    /**
     * 根据图片类型获取最新pic_index
     * @param picType 图片类型
     * @return 最新pic_index
     */
    public int getPicNewIndexByPicType(String picType) {
        String sql = "SELECT * FROM T_Special_Picture WHERE is_delete = '0' AND pic_type = ? ORDER BY pic_index desc";
        T_Special_Picture picture = dao.findFirst(sql, picType);
        return picture == null ? 1 : picture.getInt("pic_index") + 1;
    }
}

