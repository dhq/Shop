package com.pinion.model;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

import java.util.HashMap;
import java.util.List;

/**
 * 商品Model
 */
public class T_Goods extends Model<T_Goods> {
    public static final T_Goods dao = new T_Goods();

    public List<T_Goods> list(){
        return dao.find("SELECT a.* FROM t_goods a");
    }

    /**
     * 根据编号查找商品
     * @param number 商品编号
     * @return
     */
    public T_Goods getByNumber(String number){
        String sql = "SELECT * FROM t_goods WHERE " +
                "number='" + number+"'";
        return dao.findFirst(sql);
    }

    public HashMap<Integer,String> getNameMap(){
        HashMap<Integer,String> map= null;
        List<T_Goods> areas = dao.find("SELECT a.* FROM t_goods a" );
        if(areas.size()>0){
            map = new HashMap<Integer,String>();
            for(T_Goods o : areas){
                map.put(o.getInt("id"), o.getStr("name"));
            }
        }
        return map;
    }

    /**
     * 查询是否存在相同编号的商品
     * @param id 商品ID
     * @param number 商品编号
     * @return
     */
    public List<T_Goods> list(int id,String number){
        String sql = "SELECT * FROM t_goods WHERE " +
                "number='" + number + "' AND id <> " + id + "";
        return dao.find(sql);
    }

    /**
     * 查询是否存在相同编号的商品
     * @param number 商品编号
     * @return
     */
    public List<T_Goods> list(String number){
        String sql = "SELECT * FROM t_goods WHERE " +
                "number='" + number+"'";
        return dao.find(sql);
    }

    /**
     * 根据类型ID统计商品个数
     * @param typeId 类型ID
     * @return 商品个数
     */
    public Long countByType(Integer typeId){
        String sql = "SELECT COUNT(id) FROM t_goods WHERE 1=1";
        if(null != typeId){
            sql = sql +" AND type_id ="+typeId;
        }
        return Db.queryLong(sql);
    }

    /**
     * 根据品牌ID统计商品个数
     * @param brandId 品牌ID
     * @return 商品个数
     */
    public Long countByBrand(Integer brandId){
        String sql = "SELECT COUNT(id) FROM t_goods WHERE 1=1";
        if(null != brandId){
            sql = sql +" AND brand_id ="+brandId;
        }
        return Db.queryLong(sql);
    }

    public Page<T_Goods> page(int pageSize,int pageNumber,Integer type,Integer brand){
        String select = "SELECT * ";
        String sqlExceptSelect = "FROM t_goods WHERE 1=1 ";
        if(null != type){
            sqlExceptSelect = sqlExceptSelect + " AND type_id = " + type;
        }
        if(null != brand){
            sqlExceptSelect = sqlExceptSelect + " AND brand_id = " + brand;
        }
        Page<T_Goods> page = T_Goods.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
        return page;
    }

    /**
     * 根据商品类型ID更新商品类型信息
     * @param typeId 商品类型ID
     * @param typeName 商品类型名称
     * @param typeNumber 商品类型编号
     */
    public boolean updateTypeInfo(int typeId,String typeName,String typeNumber){
        String sql = "UPDATE t_goods SET type_name=?,type_number=? WHERE type_id=?";
        Db.update(sql,new Object[]{typeName,typeNumber,typeId});
        return true;
    }

    /**
     * 根据商品品牌ID更新商品品牌信息
     * @param brandId 商品品牌ID
     * @param brandName 商品品牌名称
     * @param brandNumber 商品品牌编号
     */
    public boolean updateBrandInfo(int brandId,String brandName,String brandNumber){
        String sql = "UPDATE t_goods SET brand=?,brand_number=? WHERE brand_id=?";
        Db.update(sql,new Object[]{brandName,brandNumber,brandId});
        return true;
    }
}
