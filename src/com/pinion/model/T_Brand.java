package com.pinion.model;

import com.jfinal.plugin.activerecord.Model;

import java.util.HashMap;
import java.util.List;

/**
 * 品牌Model
 */
public class T_Brand extends Model<T_Brand> {
    public static final T_Brand dao = new T_Brand();

    public List<T_Brand> list(){
        return dao.find("SELECT a.* FROM t_brand a");
    }

    /**
     * 根据名称查找品牌
     * @param name 品牌名称
     * @return
     */
    public T_Brand getByName(String name){
        String sql = "SELECT * FROM t_brand WHERE " +
                "name='" + name+"'";
        return dao.findFirst(sql);
    }

    /**
     * 根据编号查找品牌
     * @param number 品牌编号
     * @return
     */
    public T_Brand getByNumber(String number){
        String sql = "SELECT * FROM t_brand WHERE " +
                "number='" + number+"'";
        return dao.findFirst(sql);
    }

    public HashMap<Integer,String> getNameMap(){
        HashMap<Integer,String> map= null;
        List<T_Brand> areas = dao.find("SELECT a.* FROM t_brand a" );
        if(areas.size()>0){
            map = new HashMap<Integer,String>();
            for(T_Brand o : areas){
                map.put(o.getInt("id"), o.getStr("name"));
            }
        }
        return map;
    }

    /**
     * 查询是否存在相同名字的品牌
     * @param id 品牌ID
     * @param number 品牌编号
     * @return
     */
    public List<T_Brand> list(int id,String number){
        String sql = "SELECT * FROM t_brand WHERE " +
                "number='" + number + "' and id <> " + id + "";
        return dao.find(sql);
    }

    /**
     * 查询是否存在相同名字的品牌
     * @param number 品牌编号
     * @return
     */
    public List<T_Brand> list(String number){
        String sql = "SELECT * FROM t_brand WHERE " +
                "number='" + number+"'";
        return dao.find(sql);
    }
}
