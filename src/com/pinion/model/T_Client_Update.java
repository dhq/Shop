package com.pinion.model;

import com.jfinal.plugin.activerecord.Model;

import java.util.List;

/**
 * 客户端用户更新表
 */
public class T_Client_Update extends Model<T_Client_Update> {
    public static final T_Client_Update dao = new T_Client_Update();

    public List<T_Client_Update> list() {
        return dao.find("SELECT a.* FROM T_Client_Update a");
    }

    /**
     * 获取客户端更新情况
     *
     * @param user_id    用户id
     * @param updateType 更新类型
     * @return
     */
    public T_Client_Update getClientUpdate(Integer user_id, Integer updateType) {
        return dao.findFirst("SELECT * FROM t_client_update WHERE user_id=? AND update_type=?", user_id, updateType);
    }
}
