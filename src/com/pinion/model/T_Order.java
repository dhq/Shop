package com.pinion.model;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品订单Model
 */
public class T_Order extends Model<T_Order> {
    public static final T_Order dao = new T_Order();

    List<T_Order_Item> items = new ArrayList<T_Order_Item>();

    public void setItems(List<T_Order_Item> items) {
        this.items = items;
    }

    /**
     * 查询订单列表
     * @param paid 是否付款
     * @param canceled 是否作废（取消）
     * @return
     */
    public List<T_Order> list(String paid,String canceled){
        String sql = "SELECT a.* FROM t_order a WHERE 1=1 ";
        if(StrKit.notBlank(paid)){
            sql += " AND a.paid = '"+paid+"'";
        }
        if(StrKit.notBlank(canceled)){
            sql += " AND a.canceled = '"+canceled+"'";
        }
        return dao.find(sql);
    }

    /**
     * 根据编号查找订单
     * @param number 订单编号
     * @return
     */
    public T_Order getByNumber(String number){
        String sql = "SELECT * FROM t_order WHERE " +
                "number='" + number+"'";
        return dao.findFirst(sql);
    }

    /**
     * 订单分页
     * @param pageSize 每页记录条数
     * @param pageNumber 页码
     * @param paid 是否付款
     * @param canceled 是否取消
     * @param status 订单状态
     * @param salesmanId 销售员用户ID
     * @return
     */
    public Page<T_Order> page(int pageSize,int pageNumber, String paid,String canceled,Integer status,Integer salesmanId){
        String select = "SELECT * ";
        String sqlExceptSelect = "FROM t_order a WHERE 1=1 ";
        if(StrKit.notBlank(paid)){
            sqlExceptSelect += " AND a.paid = '"+paid+"'";
        }
        if(StrKit.notBlank(canceled)){
            sqlExceptSelect += " AND a.canceled = '"+canceled+"'";
        }
        if(null != status){
            sqlExceptSelect += " AND a.status = "+status;
        }
        if(null != salesmanId){
            sqlExceptSelect += " AND a.salesman_id = "+salesmanId;
        }
        Page<T_Order> page = T_Order.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
        return page;
    }

}
