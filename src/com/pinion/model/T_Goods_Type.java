package com.pinion.model;

import com.jfinal.plugin.activerecord.Model;

import java.util.HashMap;
import java.util.List;

/**
 * 商品类型Model
 */
public class T_Goods_Type extends Model<T_Goods_Type> {
    public static final T_Goods_Type dao = new T_Goods_Type();

    public List<T_Goods_Type> list(){
        return dao.find("SELECT a.* FROM t_goods_type a");
    }

    /**
     * 根据名称查找商品类型
     * @param name 商品类型名称
     * @return
     */
    public T_Goods_Type getByName(String name){
        String sql = "SELECT * FROM t_goods_type WHERE " +
                "name='" + name+"'";
        return dao.findFirst(sql);
    }

    /**
     * 根据编号查找商品类型
     * @param number 商品类型编号
     * @return
     */
    public T_Goods_Type getByNumber(String number){
        String sql = "SELECT * FROM t_goods_type WHERE " +
                "number='" + number+"'";
        return dao.findFirst(sql);
    }

    public HashMap<Integer,String> getNameMap(){
        HashMap<Integer,String> map= null;
        List<T_Goods_Type> areas = dao.find("SELECT a.* FROM t_goods_type a" );
        if(areas.size()>0){
            map = new HashMap<Integer,String>();
            for(T_Goods_Type o : areas){
                map.put(o.getInt("id"), o.getStr("name"));
            }
        }
        return map;
    }

    /**
     * 查询是否存在相同名字的商品类型
     * @param id 商品类型ID
     * @param number 商品类型编号
     * @return
     */
    public List<T_Goods_Type> list(int id,String number){
        String sql = "SELECT * FROM t_goods_type WHERE " +
                "number='" + number + "' and id <> " + id + "";
        return dao.find(sql);
    }

    /**
     * 查询是否存在相同编号的商品类型
     * @param number 商品类型编号
     * @return
     */
    public List<T_Goods_Type> list(String number){
        String sql = "SELECT * FROM t_goods_type WHERE " +
                "number='" + number+"'";
        return dao.find(sql);
    }
}
