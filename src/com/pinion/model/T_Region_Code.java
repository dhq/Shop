package com.pinion.model;

import com.jfinal.plugin.activerecord.Model;

import java.util.List;

public class T_Region_Code extends Model<T_Region_Code> {
    public static final T_Region_Code dao = new T_Region_Code();

    public List<T_Region_Code> list() {
        return dao.find("SELECT a.* FROM T_Region_Code a WHERE a.is_delete = '0'");
    }

    /**
     * 根据区域码查询区域详细信息
     *
     * @param code 区域码
     */
    public T_Region_Code getByCode(String code) {
        return dao.findFirst("SELECT * FROM T_Region_Code WHERE is_delete = '0' AND code=?", code);
    }

    /**
     * 查询是否存在相同地区码的区域编码
     * @param id 区域编码
     * @param code 地区码
     * @return
     */
    public List<T_Region_Code> list(int id,String code){
        String sql = "SELECT * FROM t_region_code WHERE is_delete = '0' AND " +
                "code='" + code + "' AND id <> " + id + "";
        return dao.find(sql);
    }
}
