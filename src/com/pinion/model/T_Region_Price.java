package com.pinion.model;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

import java.util.List;

public class T_Region_Price extends Model<T_Region_Price> {
    public static final T_Region_Price dao = new T_Region_Price();

    public List<T_Region_Price> list() {
        return dao.find("SELECT a.* FROM T_Region_Price a WHERE a.is_delete = '0'");
    }

    /**
     * 根据商品id查询所有价格记录
     *
     * @param goods_number 商品编号
     */
    public List<T_Region_Price> getByCode(int goods_number) {
        return dao.find("SELECT * FROM T_Region_Price WHERE is_delete = '0' AND goods_number=?", goods_number);
    }

    /**
     * 根据商品id查询与区域码查询唯一的价格记录
     *
     * @param goods_number 商品编号
     * @param r_code       区域码
     */
    public List<T_Region_Price> getByNumAndCode(int goods_number, String r_code) {
        return dao.find("SELECT * FROM T_Region_Price WHERE is_delete = '0' AND goods_number=? AND r_code=?", goods_number, r_code);
    }

    /**
     * 根据区域编码的地区码统计区域价格个数
     * @param code 地区码
     * @return 区域价格个数
     */
    public Long countByCode(String code){
        String sql = "SELECT COUNT(id) FROM T_Region_Price WHERE is_delete = '0'";
        if(null != code){
            sql = sql + " AND r_code =" + code;
        }
        return Db.queryLong(sql);
    }

    /**
     * 根据 旧r_code更新 新r_code
     * @param oldCode
     * @param newCode
     */
    public boolean updateCode(String oldCode, String newCode) {
        String sql = "UPDATE T_Region_Price SET r_code=? WHERE is_delete = '0' AND r_code = ?";
        Db.update(sql, new Object[] {newCode, oldCode});
        return true;
    }
}
