package com.pinion.export;

import com.pinion.util.ExportExcel;
import org.apache.poi.hssf.usermodel.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * 导出导入失败的商品信息
 */
public class ExportFailureGoods extends ExportExcel {

    @Override
    protected void setContent(Collection dataSet, String pattern, HSSFWorkbook workbook, HSSFSheet sheet, HSSFCellStyle style2, HSSFPatriarch patriarch) {
        HSSFRow row;
        Iterator<List<String>> it = dataSet.iterator();
        int index = 1;

        while (it.hasNext()) {
            index++;
            row = sheet.createRow(index);
            List<String> o = it.next();

            for(int j=0;j<o.size();j++){
                setContentCell(workbook, style2, row, o.get(j),j);
            }
        }
    }

}
