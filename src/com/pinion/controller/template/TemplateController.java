package com.pinion.controller.template;

import com.base.config.Constant;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.zh_new.annotation.RouteBind;
import com.zh_new.controller.BaseController;

import java.io.File;

/**
 * 模板下载
 */
@RouteBind(path = "/Main/Template", viewPath = "/Template/Template")
public class TemplateController extends BaseController {

    private String navTabId = "Template";

    private String rootpath = Constant.PATH_WEBROOT + Constant.PATH_FILE; // 得到目录的根路径

    /**
     * 商品类型主页
     */
    public void main() {
        render("main.jsp");
    }


    /** 文件-下载 */
    @ClearInterceptor(ClearLayer.ALL)
    public void download() {
        String url = rootpath + "/商品导入模板.xlsx";
        renderFile(new File(url));
    }
}
