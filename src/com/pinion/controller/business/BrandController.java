package com.pinion.controller.business;

import com.base.config.Constant;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.pinion.model.T_Brand;
import com.pinion.model.T_Goods;
import com.zh_new.annotation.RouteBind;
import com.zh_new.controller.BaseController;

import java.sql.SQLException;
import java.util.List;

@RouteBind(path = "/Main/Brand", viewPath = "/Business/Brand")
public class BrandController extends BaseController {

    private static final Logger log = Logger.getLogger(BrandController.class);

    private String navTabId = "Brand";

    /**
     * 品牌主页
     */
    public void main() {
        String select = "SELECT *";
        int pageSize = Constant.PAGE_SIZE;
        int pageNumber = 1;
        try {
            if (null != getPara("numPerPage") && !"".equals(getPara("numPerPage").trim())) {
                pageSize = getParaToInt("numPerPage");
            }
            if (pageSize <= 0) {
                throw new Exception();
            }
            if (null != getPara("pageNum") && !"".equals(getPara("pageNum").trim())) {
                pageNumber = getParaToInt("pageNum");
            }
            if (pageNumber <= 0) {
                throw new Exception();
            }
        } catch (Exception e) {
            log.error("您提交的查询参数有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的查询参数有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        String sqlExceptSelect = "FROM t_brand";
        try {
            Page<T_Brand> page = T_Brand.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
            setAttr("page", page);
            render("main.jsp");
        } catch (Exception e) {
            log.error("服务器存在错误，数据读取失败！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "服务器存在错误，数据读取失败！", "", "", "", "");
        }
    }

    /**
     * 品牌添加页面
     */
    public void addip() {
        render("add.jsp");
    }

    /**
     * 品牌添加处理
     */
    public void add() {
        if (null == getPara("t_Brand.number") || 0 == getPara("t_Brand.number").trim().length()) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        List<T_Brand> list = T_Brand.dao.list(getPara("t_Brand.number"));
        if (null != list && 0 < list.size()) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，品牌编号已经存在，不能重复！", "", "", "", "");
            return;
        }
        try {
            if (getModel(T_Brand.class).save()) {
                toDwzJson(Constant.STATUS_CODE_OK, "品牌添加完成！", navTabId, "", "closeCurrent", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }


    /**
     * 品牌修改页面
     */
    public void updateip() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Brand model = T_Brand.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，品牌不存在！", "", "", "", "");
            return;
        }
        setAttr("t_Brand", model);
        render("update.jsp");
    }

    /**
     * 品牌修改处理
     */
    public void update() {
        Integer id;
        try {
            id = getParaToInt("t_Brand.id");
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Brand model = T_Brand.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，品牌不存在！", "", "", "", "");
            return;
        }
        if (!model.getStr("number").equals(getPara("t_Brand.number"))) {
            List<T_Brand> list = T_Brand.dao.list(id, getPara("t_Brand.number"));
            if (null != list && 0 < list.size()) {
                toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，品牌编号已经存在，不能重复！", "", "", "", "");
                return;
            }
        }
        try {
            final T_Brand brand = getModel(T_Brand.class);
            Boolean succeed = Db.tx(new IAtom() {
                public boolean run() throws SQLException {
                    boolean value1;
                    boolean value2 = true;
                    if (value1 = brand.update()) {
                        value2 = T_Goods.dao.updateBrandInfo(brand.getInt("id"), brand.getStr("name"), brand.getStr("number"));
                    }
                    return value1 && value2;
                }
            });
            if (succeed) {
                toDwzJson(Constant.STATUS_CODE_OK, "品牌修改完成！", navTabId, "", "closeCurrent", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }

    /**
     * 品牌删除处理
     */
    public void delete() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Brand model = T_Brand.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，品牌不存在！", "", "", "", "");
            return;
        }

        Long count = T_Goods.dao.countByBrand(id);
        if (count > 0) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "品牌已经被引用，不能删除！", "", "", "", "");
            return;
        }
        try {
            if (model.delete()) {
                toDwzJson(Constant.STATUS_CODE_OK, "品牌删除完成！", navTabId, "", "", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }

}
