package com.pinion.controller.business;

import com.base.config.Constant;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.kit.PathKit;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Page;
import com.pinion.model.log.T_Error_Info_log;
import com.zh_new.annotation.RouteBind;
import com.zh_new.controller.BaseController;

import java.io.File;

@RouteBind(path = "/Main/BizLog", viewPath = "/Business/BizLog")
public class BizLogController extends BaseController {

    private static final Logger log = Logger.getLogger(BizLogController.class);

    private String navTabId = "BizLog";

    String webRoot = PathKit.getWebRootPath().replace(File.separator, "/");

    public void errInfoDownload() {
        String select = "SELECT *";
        int pageSize = Constant.PAGE_SIZE;
        int pageNumber = 1;
        try {
            if (null != getPara("numPerPage") && !"".equals(getPara("numPerPage").trim())) {
                pageSize = getParaToInt("numPerPage");
            }
            if (pageSize <= 0) {
                throw new Exception();
            }
            if (null != getPara("pageNum") && !"".equals(getPara("pageNum").trim())) {
                pageNumber = getParaToInt("pageNum");
            }
            if (pageNumber <= 0) {
                throw new Exception();
            }
        } catch (Exception e) {
            log.error("您提交的查询参数有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的查询参数有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        String sqlExceptSelect = "FROM t_error_info_log";
        try {
            Page<T_Error_Info_log> page = T_Error_Info_log.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
            setAttr("page", page);
            render("errInfoDownload.jsp");
        } catch (Exception e) {
            log.error("服务器存在错误，数据读取失败！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "服务器存在错误，数据读取失败！", "", "", "", "");
        }
    }

    @ClearInterceptor(ClearLayer.ALL)
    public void errDownload() {
        int log_id = getParaToInt("id");
        String xlsPath = T_Error_Info_log.dao.findById(log_id).getStr("url");
        File errorXls = new File(webRoot + "/" + xlsPath);

        try {
            if(errorXls.exists()) {
                renderFile(errorXls);
            } else {
                new Exception("不存在路径为：" + xlsPath + "的文件！");
            }
        } catch(Exception e) {
            log.error("不存在路径为：" + xlsPath + "的文件！", e);
        }
    }

}
