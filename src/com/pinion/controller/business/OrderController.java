package com.pinion.controller.business;

import com.base.config.Constant;
import com.base.model.LoginModel;
import com.base.model.authority.T_User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.pinion.model.T_Order;
import com.pinion.model.T_Order_Item;
import com.pinion.util.BusinessConstant;
import com.pinion.util.OrderUtil;
import com.zh_new.annotation.RouteBind;
import com.zh_new.controller.BaseController;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RouteBind(path = "/Main/Order", viewPath = "/Business/Order")
public class OrderController extends BaseController {

    private static final Logger log = Logger.getLogger(OrderController.class);

    private String navTabId = "Order";

    /**
     * 订单主页
     */
    public void main() {
        int pageSize = Constant.PAGE_SIZE;
        int pageNumber = 1;
        String paid = getPara("paid");
        String canceled = getPara("canceled");
        Integer status = getParaToInt("status");
        Integer salesmanId = getParaToInt("salesmanId");
        if (StrKit.isBlank(paid)) {
            paid = BusinessConstant.ORDER_PAID_NO;
        }
        if (StrKit.isBlank(canceled)) {
            canceled = BusinessConstant.ORDER_CANCELED_NO;
        }
        try {
            if (null != getPara("numPerPage") && !"".equals(getPara("numPerPage").trim())) {
                pageSize = getParaToInt("numPerPage");
            }
            if (pageSize <= 0) {
                throw new Exception();
            }
            if (null != getPara("pageNum") && !"".equals(getPara("pageNum").trim())) {
                pageNumber = getParaToInt("pageNum");
            }
            if (pageNumber <= 0) {
                throw new Exception();
            }
        } catch (Exception e) {
            log.error("您提交的查询参数有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的查询参数有误，请检查后重新提交！", "", "", "", "");
            return;
        }

        try {
            setAttr("page", T_Order.dao.page(pageSize, pageNumber, paid, canceled, status, salesmanId));
            setAttr("paid", paid);
            setAttr("canceled", canceled);
            setAttr("status", status);
            setAttr("salesmanId", salesmanId);
            setAttr("salesmanList", T_User.dao.getSalesmanList());
            render("main.jsp");
        } catch (Exception e) {
            log.error("服务器存在错误，数据读取失败！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "服务器存在错误，数据读取失败！", "", "", "", "");
        }
    }

    /**
     * 订单添加页面
     */
    public void addip() {
        //获取订单号
        String number = OrderUtil.getOrderNumberByUUId();
        setAttr("orderNumber", number);
        render("add.jsp");
    }

    /**
     * 订单添加处理
     */
    public void add() {
        try {
            T_Order model = getModel(T_Order.class);
            //获取流水号
            model.set("serial_number", OrderUtil.getOrderSerialNumber());
            LoginModel loginModel = (LoginModel) getSessionAttr("loginModel");
            model.set("salesman_id", loginModel.getUserId());
            model.set("salesman_name", loginModel.getUserName());
            model.set("salesman_staff_id", loginModel.getUserStaffId());
            model.set("create_time", new Date());
            if (model.save()) {
                toDwzJson(Constant.STATUS_CODE_OK, "订单添加完成！", navTabId, "", "closeCurrent", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }


    /**
     * 订单修改页面
     */
    public void updateip() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Order model = T_Order.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，订单不存在！", "", "", "", "");
            return;
        }
        setAttr("order", model);
        render("update.jsp");
    }

    /**
     * 订单修改处理
     */
    public void update() {
        Integer id;
        try {
            id = getParaToInt("t_Order.id");
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Order order = T_Order.dao.findById(id);
        if (null == order) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，订单不存在！", "", "", "", "");
            return;
        }
        if (order.getInt("status") == BusinessConstant.ORDER_STATE_FINISHED) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "该订单已完成，不可修改！", "", "", "", "");
            return;
        }
        try {
            T_Order model = getModel(T_Order.class);
            model.set("update_time", new Date());
            if (model.update()) {
                toDwzJson(Constant.STATUS_CODE_OK, "订单修改完成！", navTabId, "", "closeCurrent", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }

    /**
     * 订单删除处理
     */
    public void delete() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Order model = T_Order.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，订单不存在！", "", "", "", "");
            return;
        }

        Long count = T_Order_Item.dao.countByOrderId(id);
        if (count > 0) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "订单已经被引用，不能删除！", "", "", "", "");
            return;
        }
        try {
            if (model.delete()) {
                toDwzJson(Constant.STATUS_CODE_OK, "订单删除完成！", navTabId, "", "", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }

    /**
     * 查看订单详情
     */
    public void detail() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Order model = T_Order.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，订单不存在！", "", "", "", "");
            return;
        }
        List<T_Order_Item> itemList = T_Order_Item.dao.listByOrderId(id);
        setAttr("itemList", itemList);
        setAttr("order", model);
        render("detail.jsp");
    }

    /**
     * 订单取消处理
     */
    public void cancel() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Order model = T_Order.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，订单不存在！", "", "", "", "");
            return;
        }
        if (model.getInt("status") > BusinessConstant.ORDER_STATE_UNCOMMITTED) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "订单提交后不可取消！", "", "", "", "");
            return;
        }
        try {
            model.set("canceled", BusinessConstant.ORDER_CANCELED_YES);
            model.set("update_time", new Date());
            if (model.update()) {
                toDwzJson(Constant.STATUS_CODE_OK, "订单取消完成！", navTabId, "", "", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }

    /**
     * 订单付款状态改为已付款
     */
    public void paid() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Order model = T_Order.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，订单不存在！", "", "", "", "");
            return;
        }
        try {
            model.set("paid", BusinessConstant.ORDER_PAID_YES);
            model.set("update_time", new Date());
            if (model.update()) {
                toDwzJson(Constant.STATUS_CODE_OK, "订单付款状态改为已付款！", navTabId, "", "", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }

    /**
     * 订单状态改为已发货
     */
    public void delivered() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Order model = T_Order.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，订单不存在！", "", "", "", "");
            return;
        }
        if (model.getInt("status") == BusinessConstant.ORDER_STATE_FINISHED) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "该订单已完成，不可修改！", "", "", "", "");
            return;
        }
        try {
            model.set("status", BusinessConstant.ORDER_STATE_DELIVERED);
            model.set("update_time", new Date());
            if (model.update()) {
                toDwzJson(Constant.STATUS_CODE_OK, "订单状态改为已发货！", navTabId, "", "", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }


    /**
     * 订单状态改为已收货
     */
    public void received() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Order model = T_Order.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，订单不存在！", "", "", "", "");
            return;
        }
        if (model.getInt("status") == BusinessConstant.ORDER_STATE_FINISHED) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "该订单已完成，不可修改！", "", "", "", "");
            return;
        }
        try {
            model.set("status", BusinessConstant.ORDER_STATE_RECEIVED);
            model.set("update_time", new Date());
            if (model.update()) {
                toDwzJson(Constant.STATUS_CODE_OK, "订单状态改为已收货！", navTabId, "", "", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }

    /**
     * 订单状态改为已完成
     */
    public void finished() {
        Integer id;
        try {
            id = getParaToInt();
        } catch (Exception e) {
            log.error("您提交的数据有误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，请检查后重新提交！", "", "", "", "");
            return;
        }
        T_Order model = T_Order.dao.findById(id);
        if (null == model) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "您提交的数据有误，订单不存在！", "", "", "", "");
            return;
        }
        if (model.getInt("status") == BusinessConstant.ORDER_STATE_FINISHED) {
            toDwzJson(Constant.STATUS_CODE_ERROR, "该订单已完成！", "", "", "", "");
            return;
        }
        try {
            model.set("status", BusinessConstant.ORDER_STATE_FINISHED);
            model.set("update_time", new Date());
            if (model.update()) {
                toDwzJson(Constant.STATUS_CODE_OK, "订单状态改为已完成！", navTabId, "", "", "");
            } else {
                toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
            }
        } catch (Exception e) {
            log.error("数据处理存在错误，请检查后重新提交！", e);
            toDwzJson(Constant.STATUS_CODE_ERROR, "数据处理存在错误，请检查后重新提交！", "", "", "", "");
        }
    }



    //与客户端交互开始

    /**
     * 客户端创建初始订单
     */
    @ClearInterceptor(ClearLayer.ALL)
    public void clientAdd(){
        //获取客户端传过来的信息
        String inJsonStr = HttpKit.readIncommingRequestData(getRequest());
        try {
            //解析成Map
            Map<String, Object> temp = new ObjectMapper().readValue(inJsonStr, Map.class);

        } catch (IOException e) {
            log.error("", e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
