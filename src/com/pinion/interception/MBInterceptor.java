package com.pinion.interception;

import com.base.config.Constant;
import com.base.model.log.T_Loginlog;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

/**
 * 移动端调用API-拦截器
 */
public class MBInterceptor implements Interceptor {
    public void intercept(ActionInvocation ai) {
        String authId = ai.getController().getPara("auth_id");
        T_Loginlog loginlog = getAuthObj(authId);
        if (null != loginlog) {
            ai.getController().setAttr("auth", true);
            ai.getController().setAttr("auth_user_id", loginlog.get("u_id"));
            ai.invoke();
        } else {
            ai.getController().setAttr("auth", false);
            ai.getController().setAttr("message", "用户身份检验失败!");
            ai.getController().renderJson();
        }
    }

    private T_Loginlog getAuthObj(String authId) {
        T_Loginlog loginlog = T_Loginlog.dao.getBySession(authId);
        //判断是否记录
        if (null == loginlog) {
            return null;
        }
        //判断是否登录超时
        if (loginlog.isLoginTimeOut(authId, Constant.CLIENT_MB)) {
            return null;
        }
        return loginlog;
    }
}