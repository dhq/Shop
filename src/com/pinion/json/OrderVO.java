package com.pinion.json;

import com.base.model.authority.T_User;
import com.pinion.model.T_Order;
import com.pinion.model.T_Order_Item;
import com.pinion.util.OrderUtil;

import java.sql.Timestamp;
import java.util.List;
import java.util.Date;

public class OrderVO {
    private List<OrderItemVO> items;

    private Integer id;
    private String serial_number;
    private String customer_name;
    private String customer_contacts;
    private String receiver_address;
    private Integer status;
    private Integer salesman_id;
    private String salesman_name;
    private String salesman_staff_id;
    private String number;
    private String paid;
    private Double amount;

    private Timestamp create_time;
    private Timestamp update_time;
    private Timestamp last_date;
    private String is_delete;

    public void genFromObj(T_Order order) {

//        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        this.id = order.getInt("id");
        this.serial_number = order.get("serial_number");
        this.customer_name = order.get("customer_name");
        this.customer_contacts = order.get("customer_contacts");
        this.receiver_address = order.get("receiver_address");
        this.status = order.get("status");
        this.salesman_id = order.get("salesman_id");
        this.salesman_name = order.get("salesman_name");
        this.salesman_staff_id = order.get("salesman_staff_id");
        this.number = order.get("number");
        this.paid = order.get("paid");
        this.amount = order.get("amount");

//        this.create_time = null != order.get("create_time") ? sdf.format(order.get("create_time")) : "";
//        this.update_time = null != order.get("update_time") ? sdf.format(order.get("update_time")) : "";
//        this.last_date = null != order.get("last_date") ? sdf.format(order.get("last_date")) : "";
        this.create_time = order.get("create_time");
        this.update_time = order.get("update_time");
        this.last_date = order.get("last_date");
        this.is_delete = order.get("canceled");
    }

    public T_Order setToObj() {
        T_Order order = new T_Order();
        order.set("serial_number", OrderUtil.getOrderSerialNumber());
        order.set("customer_name", this.customer_name);
        order.set("customer_contacts", this.customer_contacts);

        order.set("receiver_address", this.receiver_address);
        order.set("status", this.status);

        order.set("salesman_staff_id", this.salesman_staff_id);//销售员工号
        T_User user = T_User.dao.findByStaffId(this.salesman_staff_id);
        order.set("salesman_id", user.get("id"));
        order.set("salesman_name", user.get("name"));
        order.set("create_time", new Date());
        order.set("number", OrderUtil.getOrderNumberByUUId());
        order.set("paid", this.paid);
        order.set("amount", this.amount);
        order.set("canceled", this.is_delete);

        return order;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public Timestamp getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Timestamp update_time) {
        this.update_time = update_time;
    }

    public Timestamp getLast_date() {
        return last_date;
    }

    public void setLast_date(Timestamp last_date) {
        this.last_date = last_date;
    }

    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    public List<OrderItemVO> getItems() {
        return items;
    }

    public void setItems(List<OrderItemVO> items) {
        this.items = items;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_contacts() {
        return customer_contacts;
    }

    public void setCustomer_contacts(String customer_contacts) {
        this.customer_contacts = customer_contacts;
    }

    public String getReceiver_address() {
        return receiver_address;
    }

    public void setReceiver_address(String receiver_address) {
        this.receiver_address = receiver_address;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSalesman_id() {
        return salesman_id;
    }

    public void setSalesman_id(Integer salesman_id) {
        this.salesman_id = salesman_id;
    }

    public String getSalesman_name() {
        return salesman_name;
    }

    public void setSalesman_name(String salesman_name) {
        this.salesman_name = salesman_name;
    }

    public String getSalesman_staff_id() {
        return salesman_staff_id;
    }

    public void setSalesman_staff_id(String salesman_staff_id) {
        this.salesman_staff_id = salesman_staff_id;
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


    public String getCanceled() {
        return is_delete;
    }

    public void setCanceled(String canceled) {
        this.is_delete = canceled;
    }
}
