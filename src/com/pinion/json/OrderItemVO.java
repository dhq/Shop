package com.pinion.json;

import com.pinion.model.T_Order_Item;

import java.util.Date;

public class OrderItemVO {
    private Integer id;
    private Integer order_id;
    private double transaction_value;
    private String goods_number;
    private String goods_name;
    private Integer goods_count;
    private Date create_time;
    private Integer create_user;
    private String brand_number;
    private String brand_name;
    private String good_type_number;
    private String good_type_name;
    private double commission;

    public void genFromObj(T_Order_Item orderItem) {
        this.id = orderItem.getInt("id");
        this.order_id = orderItem.getInt("order_id");
        this.transaction_value = orderItem.getDouble("transaction_value");
        this.goods_number = orderItem.get("goods_number");
        this.goods_name = orderItem.get("goods_name");
        this.goods_count = orderItem.getInt("goods_count");
        this.create_time = orderItem.get("create_time");
        this.create_user = orderItem.getInt("create_user");
        this.brand_number = orderItem.get("brand_number");
        this.brand_name = orderItem.get("brand_name");
        this.good_type_number = orderItem.get("good_type_number");
        this.good_type_name = orderItem.get("good_type_name");
        this.commission = orderItem.get("commission");
    }

    public T_Order_Item setToObj(Integer order_id, Integer user_id) {
        T_Order_Item orderItem = new T_Order_Item();
        orderItem.set("order_id", order_id);
        orderItem.set("transaction_value", this.transaction_value);
        orderItem.set("goods_number", this.goods_number);
        orderItem.set("goods_name", this.goods_name);
        orderItem.set("goods_count", this.goods_count);
        orderItem.set("create_time", new Date());
        orderItem.set("create_user", user_id);
        orderItem.set("brand_number", this.brand_number);
        orderItem.set("brand_name", this.brand_name);
        orderItem.set("good_type_number", this.good_type_number);
        orderItem.set("good_type_name", this.good_type_name);
        orderItem.set("commission", this.commission);
        return orderItem;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    public double getTransaction_value() {
        return transaction_value;
    }

    public void setTransaction_value(double transaction_value) {
        this.transaction_value = transaction_value;
    }

    public String getGoods_number() {
        return goods_number;
    }

    public void setGoods_number(String goods_number) {
        this.goods_number = goods_number;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public Integer getGoods_count() {
        return goods_count;
    }

    public void setGoods_count(Integer goods_count) {
        this.goods_count = goods_count;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Integer getCreate_user() {
        return create_user;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    public String getBrand_number() {
        return brand_number;
    }

    public void setBrand_number(String brand_number) {
        this.brand_number = brand_number;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getGood_type_number() {
        return good_type_number;
    }

    public void setGood_type_number(String good_type_number) {
        this.good_type_number = good_type_number;
    }

    public String getGood_type_name() {
        return good_type_name;
    }

    public void setGood_type_name(String good_type_name) {
        this.good_type_name = good_type_name;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }
}
