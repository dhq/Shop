package com.pinion.json;

/**
 * 显示列[品牌名, 总销售额，总提成]
 */
public class BrandStatisticVO {
    String brandName;   //品牌名
    Double sale;       //品牌总销售额
    Double commission;  //品牌总提成

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Double getSale() {
        return sale;
    }

    public void setSale(Double sale) {
        this.sale = sale;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        commission = commission;
    }
}
