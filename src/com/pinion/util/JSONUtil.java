package com.pinion.util;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;

/**
 * JSON工具类
 */
public class JSONUtil {
    public static Object fromJsonToJava(JSONObject json, Class pojo) throws Exception {

        // 首先得到pojo所定义的字段
        Field[] fields = pojo.getDeclaredFields();
        // 根据传入的Class动态生成pojo对象
        Object obj = pojo.newInstance();
        for (Field field : fields) {
            // 设置字段可访问（必须，否则报错）
            field.setAccessible(true);
            // 得到字段的属性名
            String name = field.getName().toLowerCase();
            // 这一段的作用是如果字段在JSONObject中不存在会抛出异常，如果出异常，则跳过。
            try {
                json.get(name);
            } catch (Exception ex) {
                continue;
            }

            if (json.get(name) != null && !"".equals(json.getString(name))) {
                //TODO:Test
//                System.out.println(json.getString(name));
//                System.out.println(null == json.getString(name));

                // 根据字段的类型将值转化为相应的类型，并设置到生成的对象中。
                if (field.getType().equals(Long.class) || field.getType().equals(long.class)) {
                    field.set(obj, Long.parseLong(json.getString(name)));
                } else if (field.getType().equals(String.class)) {
                    field.set(obj, json.getString(name));
                } else if (field.getType().equals(Double.class) || field.getType().equals(double.class)) {
                    field.set(obj, null == json.getString(name) ? null : Double.parseDouble(json.getString(name)));
                } else if (field.getType().equals(Integer.class) || field.getType().equals(int.class)) {
                    field.set(obj, null == json.getString(name) ? null : Integer.parseInt(json.getString(name)));
                } else if (field.getType().equals(java.util.Date.class)) {
                    field.set(obj, new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(json.getString(name)).getTime()));
                } else if (field.getType().equals(java.sql.Timestamp.class)) {
                    field.set(obj, Timestamp.valueOf(json.getString(name)));
                } else {
                    continue;
                }
            }
        }
        return obj;
    }

    public static JSONObject transObject(JSONObject o1){
        JSONObject o2=new JSONObject();
        Iterator it = o1.keys();
        while (it.hasNext()) {
            String key = (String) it.next();
            Object object = o1.get(key);
            if(object.getClass().toString().endsWith("String")){
                o2.accumulate(key.toLowerCase(), object);
            }else if(object.getClass().toString().endsWith("JSONObject")){
                o2.accumulate(key.toLowerCase(), JSONUtil.transObject((JSONObject)object));
            }else if(object.getClass().toString().endsWith("JSONArray")){
                o2.accumulate(key.toLowerCase(), JSONUtil.transArray(o1.getJSONArray(key)));
            }
        }
        return o2;
    }
    public static JSONArray transArray(JSONArray o1){
        JSONArray o2 = new JSONArray();
        for (int i = 0; i < o1.size(); i++) {
            Object jArray=o1.getJSONObject(i);
            if(jArray.getClass().toString().endsWith("JSONObject")){
                o2.add(JSONUtil.transObject((JSONObject)jArray));
            }else if(jArray.getClass().toString().endsWith("JSONArray")){
                o2.add(JSONUtil.transArray((JSONArray) jArray));
            }
        }
        return o2;
    }
}
