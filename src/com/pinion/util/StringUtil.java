package com.pinion.util;

import com.jfinal.kit.StrKit;

import java.util.*;

public class StringUtil {
/**
 * 对字符串各种操作的工具类
 */
	/**通过  , 拆分字符串数组*/
	public static String[] splitString(String str){
		String[] result=str.split(",|、");
		return result;
	}
	
	/**将字符串数据转换为int*/
	public static Integer[] splitInteger(String str){
		String[] result = str.split(",");
		Integer[] ids = new Integer[result.length];
		for (int i = 0; i < result.length; i++) {
			ids[i]=Integer.parseInt(result[i]);
		}
		return ids;
	}
	
	/**将字符串，与过来的Int对比，存在为true*/
	public static boolean isLive(String str,Integer target){
		Integer[] targets = splitInteger(str);
		for(Integer son:targets){
			if(son==target){
				return true;
			}
		}
		return false;
	}
	
	/**将字符串全部转换为Double，返回是否转换成功*/
	public static String[] changeSuss(String[] jcjg,String exceptString){
		for(int i=0;i<jcjg.length;i++){
			try {
				if(jcjg[i]!=null&&!"".equals(jcjg[i])){
					if(!jcjg[i].equals(exceptString)){
						Double.parseDouble(jcjg[i]);
					}else{
						//当要实现结果要--时，替换为10000
						jcjg[i] = "10000";
					}
				}
			} catch (Exception e) {
				return null;
			}
		}
		return jcjg;
	}
	
	/**合并字符串，通过,分隔*/
	public static String combineString(String[] strs){
		StringBuffer result=new StringBuffer();
		for(String str:strs){
			result.append(","+str);
		}
		result = result.deleteCharAt(0);
		return result.toString();
		
	}
	
	/**列表合并*/
	public static String combineList(List<String> strs){
		StringBuffer result=new StringBuffer();
		for(String str:strs){
			result.append(","+str);
		}
		if(strs.size()!=0){
			result.append(",");
		}
		return result.toString();
	}
	
	
	/**列表合并(list)*/
	public static String combineListInt(List<Integer> strs){
		StringBuffer result=new StringBuffer();
		for(int i = 0;i<strs.size();i++){
			if(i!=0){
				result.append(",");
			}
			result.append(strs.get(i));
		}
		return result.toString();
	}
	
	/**列表合并'result'格式*/
	public static String combineListQuotes(List<String> strs){
		StringBuffer result=new StringBuffer();
		for(int i = 0;i<strs.size();i++){
			result.append(",'"+strs.get(i)+"'");
		}
		result.deleteCharAt(0);
		return result.toString();
	}
	
	/**列表合并(set)*/
	public static String combineSetInt(Set<Integer> strs){
		StringBuffer result=new StringBuffer();
		boolean flag = false;
		for(Integer str:strs){
			if(flag){
				result.append(",");
			}
			result.append(str);
			flag = true;
		}
		return result.toString();
	}
	
	/**列表合并(set)*/
	public static String combineSetString(Set<String> strs){
		StringBuffer result=new StringBuffer();
		for(String str:strs){
			result.append(",");
			result.append(str);
		}
		result.deleteCharAt(0);
		return result.toString();
	}
	
	/**列表合并(set)*/
	public static Set<String> setTester(Set<String> tester,String names){
		if(StrKit.isBlank(names)){
			return  tester;
		}
		String[] name = names.split(",");
		for(int i=0;i<name.length;i++){
			tester.add(name[i]);
		}
		return tester;
	}
	
	
	/**将字符串后两位的数字部分进行相加  augend被加数 addend加数*/
	public static String AddOperation(String augend,int addend){
		String result = null;
		String augStr = augend.substring(0,augend.length()-2);
		String augCount=augend.substring(augend.length()-2);
		int resultStr=Integer.parseInt(augCount)+addend;
		if(resultStr<10){
			result = augStr+"0"+resultStr;
		}else{
			result = augStr+resultStr;
		}
		return result;
	}
	
	/**字符串最后两位是否为数字*/
	public static boolean isDigit(String str){
		String augCount=str.substring(str.length()-2);
		try {
			Integer.parseInt(augCount);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**判断数据中是否有重复的值*/
	public static boolean checkRepeat(Integer[] array){
		ArrayList<Integer> temp = new ArrayList<Integer>();
		for(Integer arr : array){
			//把0去掉
			if(arr!=0){
				temp.add(arr);
			}
		}
		if(temp.size()==0){
			//人员数组为空
			return false;
		}
		Set<Integer> set = new HashSet<Integer>();
		for(Integer user : temp){
			set.add(user);
		}
		if(set.size() == temp.size()){
			return false;
		}else{
			return true;
		}
	}
	
	/**将人员数组中 去0*/
	public static ArrayList<Integer> getUserOutZero(Integer[] array){
		ArrayList<Integer> temp = new ArrayList<Integer>();
		for(Integer arr : array){
			//把0去掉
			if(arr!=0){
				temp.add(arr);
			}
		}
		return temp;
	}
	
	/**两个list里的值是否相同   不同为true 相同为false*/
	public static boolean isDifferent(ArrayList<Integer> users,ArrayList<Integer> oczrys){
		if(users.size()!=oczrys.size()){
			return true;
		}
		boolean flag = false;
		for (Integer user:users) {
			for (Integer oczry:oczrys) {
				if(user==oczry){
					flag=false;
					break;
				}
				flag=true;
			}
			if(flag){break;}
		}
		return flag;
	}
	
	/**根据当前的字符串的后两个数据和count运算，返加结果*/
	public static String getLastbp(String lastbp){
		String result = null;
		int resultStr=Integer.parseInt(lastbp);
		if(resultStr<10){
			result = "0"+resultStr;
		}else{
			result = ""+resultStr;
		}
		return result;
	}
	
	/**根据两个样品编号，得出count数量*/
	public static Integer getCount(String str,String lastbp){
		String augCount=str.substring(str.length()-2);
		Integer result=Integer.parseInt(lastbp)-Integer.parseInt(augCount)+1;
		return result;
	}
	
	/**根据新编号*/
	public static String getNewNo(String str){
		String result = "";
		try{
			String augNo = str.substring(0,str.length()-3);
			String augCount=str.substring(str.length()-3);
			Integer count=Integer.parseInt(augCount)+1;
			if(count<10){
				result =augNo+ "00"+count;
			}else if(count<100){
				result =augNo+ "0"+count;
			}else{
				result =augNo+ ""+count;
			}
		}catch(Exception e){
			result = str;
		}
		return result;
	}
	
	
	
	/**去掉相同的字符串*/
	public static HashSet<String> distinctSet(String[] strs){
		HashSet<String> climit = new HashSet<String>();
		// 把重复的权限去掉
		for (String c : strs) { 
			climit.add(c);
		}
		return climit;
	}
	
	/**去掉多个字符串数组里的重复项*/
	public static String distinctStr(List<String> strs){
		StringBuffer buffer = new StringBuffer("");
		for (String string : strs) {
			String[] strings = string.split(",");
			for (int i = 0; i < strings.length; i++) {
				if (buffer.substring(0).indexOf(strings[i] + ",") == -1) {
					buffer.append(strings[i] + ",");
				}
			}
		}
		return buffer.substring(0);
	}
	
	/**[括号后面的不要*/
	public static String indexMiddle(String str){
		int indexOf = str.indexOf("[");
		if(indexOf!=-1){
			str = str.substring(0,indexOf);
		}
		return str;
	}
	
	/**生成一个唯一标识*/
	public static String getUUID(){
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	
	/**处理填充数据*/
	public static String fixData(Object data,String replaceStr){
		return data==null?replaceStr:data.toString();
	}
	
	/**处理填充数据*/
	public static String fixData(Object data){
		return data==null?"":data.toString();
	}
	/**判定是否为null,如果是则返回 ''*/
	public static String isNull(String str){
		if(str==null){
			return "";
		}
		return str;
	}
	
	public static Integer getJudg(String compare,String value,String upper,String lower)throws Exception{
		Integer judg = 1;
		if("0".equals(compare)){
			if(Float.parseFloat(value)>Float.parseFloat(upper)){
				judg = 0;
			}
		}else if("1".equals(compare)){
			if(Float.parseFloat(value)<Float.parseFloat(lower)){
				judg = 0;
			}
		}else {
			if(Float.parseFloat(value)>Float.parseFloat(upper)||Float.parseFloat(value)<Float.parseFloat(lower)){
				judg = 0;
			}
		}
		return judg;
	}
	
	public static String LimitValue(String upper,String lower){
		String limit = "--";
		if(StrKit.notBlank(upper,lower)){
			limit = lower+"~"+upper;
		}else if(StrKit.notBlank(lower)){
			limit = "≥"+lower;
		}else if(StrKit.notBlank(upper)){
			limit = "≤"+upper;
		}
		return limit;
	}
	
	public static String isBlank(String name,String id){
		if(StrKit.isBlank(name)){
			id = null;
		}
		return id;
	}
	
}
