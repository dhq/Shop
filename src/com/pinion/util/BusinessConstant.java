package com.pinion.util;

/**
 * 业务常量类
 */
public class BusinessConstant {

    public final static String OA_ID = "1";                 //企业OA应用ID

    public final static String ORDER_PAID_YES = "1";        //订单已付款
    public final static String ORDER_PAID_NO = "0";         //订单未付款

    public final static String ORDER_CANCELED_YES = "1";    //订单已作废（取消）
    public final static String ORDER_CANCELED_NO = "0";     //订单未作废（未取消）

    public final static int ORDER_STATE_UNCOMMITTED = 1;    //未提交
    public final static int ORDER_STATE_COMMITTED = 2;      //已提交
    public final static int ORDER_STATE_DELIVERED = 3;      //已发货
    public final static int ORDER_STATE_RECEIVED = 4;       //已收货
    public final static int ORDER_STATE_FINISHED = 5;       //已完成

    public final static int ROLE_ID_SALESMAN = 2;           //销售员角色ID

    /**
     * 统计常量
     */

    public static final String QUARTER_FIRST = "01";
    public static final String QUARTER_SECOND = "04";
    public static final String QUARTER_THIRD = "07";
    public static final String QUARTER_FOURTH = "10";
    public static final int YEAR_TYPE = 0;                                      //按年
    public static final int MONTH_TYPE = 12;                                    //按月
    public static final int QUARTER_FIRST_TYPE = 13;                            //按季度
    public static final int QUARTER_SECOND_TYPE = 14;
    public static final int QUARTER_THIRD_TYPE = 15;
    public static final int QUARTER_FOURTH_TYPE = 16;

    public static final int ORIGINAL_PRICE = 1;             //原价
    public static final int REAL_PAY_PRICE = 2;             //实付价
}
