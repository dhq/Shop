package com.pinion.util;

import com.base.config.Constant;
import com.pinion.model.T_Order_Total;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 订单工具类
 */
public class OrderUtil {
    /**
     * 根据服务器ID和UUID的哈希编码生成16位订单号
     * 服务器ID要保证是一位数
     * @return
     */
    public static String getOrderNumberByUUId(){
        int hashCode = UUID.randomUUID().toString().hashCode();
        if(hashCode < 0){
            hashCode = -hashCode;
        }
        //%015d
        // 0 代表不足位数前边补0
        // 15 代表长度为15位
        // d 代表参数为正整数
        return Constant.SEVER_MACHINE_ID+String.format("%015d",hashCode);
    }

    /**
     * 获取订单流水号
     * 流水号格式：日期+订单顺序号，201505101
     * @return 订单流水号
     */
    public static synchronized String getOrderSerialNumber(){
        Date now = new Date();
        String serialNumber = new SimpleDateFormat(Constant.DATE_PATTERN_SHORT_NO_RAIL).format(now);
        String today =  new SimpleDateFormat(Constant.DATE_PATTERN_SHORT).format(now);
        T_Order_Total orderTotal = T_Order_Total.dao.getByDay(today);
        if(null == orderTotal){
            orderTotal = new T_Order_Total();
            orderTotal.set("day",today);
            orderTotal.set("total",1);
            serialNumber += 1;
            orderTotal.save();
        }else{
            int total = orderTotal.getInt("total") + 1;
            serialNumber += total;
            orderTotal.set("total",total);
            orderTotal.update();
        }
        return serialNumber;
    }
}
