package com.base.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.base.controller.login.LoginController;
import com.base.model.LoginModel;

public class SessionListener implements HttpSessionListener {

	public void sessionCreated(HttpSessionEvent sessionEvent) {
		
	}

	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		LoginModel loginModel = (LoginModel)sessionEvent.getSession().getAttribute("loginModel");
		if (loginModel != null) {
			LoginController.userMap.remove(loginModel.getDlId());
			sessionEvent.getSession().invalidate();
		}
	}

}
