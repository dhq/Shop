package com.base.controller.login;

import com.jfinal.aop.ClearInterceptor;
import com.zh_new.annotation.RouteBind;
import com.zh_new.controller.BaseController;

@ClearInterceptor
@RouteBind(path = "/")
public class MainController extends BaseController {

	public void index() {
		
		redirect("/Login/index");
		return;
	}
}
