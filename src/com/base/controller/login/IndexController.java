package com.base.controller.login;

import com.base.model.LoginModel;
import com.base.model.log.T_Loginlog;
import com.base.model.settings.T_Moudle;
import com.zh_new.annotation.RouteBind;
import com.zh_new.controller.BaseController;
import com.zh_new.util.Permit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RouteBind(path = "/Main", viewPath = "Index/Main")
public class IndexController extends BaseController {

	/** 主界面 */
	public void index() {
		List<T_Moudle> moudles = new T_Moudle().getBaseMoudles();
		LoginModel loginModel = (LoginModel) getSessionAttr("loginModel");
		List<T_Moudle> userMoudles = new ArrayList<T_Moudle>();
		if (null != moudles && 0 < moudles.size()) {
			for (T_Moudle moudle : moudles) {
				if (Permit.haspermit(moudle.getInt("m_id"), loginModel.getLimit())) {
					userMoudles.add(moudle);
				}
			}
			setAttr("moudles", userMoudles);
		}
		Integer userId = loginModel.getUserId();
		setAttr("loginLog", T_Loginlog.dao.getLast(userId));
		render("main.jsp");
	}

	public void daohang() {
		if (null != getPara() && !"".equals(getPara())) {
			Integer mid;
			try {
				mid = getParaToInt();
			} catch (Exception e) {
				render("daohang.jsp");
				return;
			}
			T_Moudle pMoudle = T_Moudle.dao.findById(mid);
			if (null != pMoudle) {
				LoginModel loginModel = (LoginModel) getSessionAttr("loginModel");
				List<T_Moudle> userMoudles = new ArrayList<T_Moudle>();
				List<T_Moudle> childMoudles = new ArrayList<T_Moudle>();
                Map<Integer,List<T_Moudle>> childMoudleMap = new HashMap<Integer, List<T_Moudle>>();
                if (null != pMoudle.getMoudles() && 0 < pMoudle.getMoudles().size()) {
                    for (T_Moudle moudle : pMoudle.getMoudles()) {
                        Integer m_id = moudle.getInt("m_id");
                        if (Permit.haspermit(m_id, loginModel.getLimit())) {
                            userMoudles.add(moudle);
                            if (null != moudle.getMoudles() && 0 < moudle.getMoudles().size()) {
                                for (T_Moudle smoudle : moudle.getMoudles()) {
                                    if (Permit.haspermit(smoudle.getInt("m_id"), loginModel.getLimit())) {
                                        childMoudles.add(smoudle);
                                    }
                                }
                                childMoudleMap.put(m_id,childMoudles);
                                childMoudles = new ArrayList<T_Moudle>();
                            }
                        }
                    }
                    setAttr("moudles", userMoudles);
                    setAttr("childMoudleMap", childMoudleMap);
                }
			}
		}
		render("daohang.jsp");
	}
}
