package com.base.controller.authority;

import com.base.model.authority.T_Department;
import com.base.validator.DeptValidator;
import com.jfinal.aop.Before;
import com.jfinal.core.ActionKey;
import com.zh_new.annotation.RouteBind;
import com.zh_new.controller.BaseController;

import java.util.List;

@RouteBind(path = "Main/QxDept", viewPath = "System/Authority/Department")
public class DeptController extends BaseController {

    private static String navTabId = "deptBox";

    public void main() {
        Integer mpId = 0;
        if (null != getPara("mpId")) {
            try {
                mpId = getParaToInt("mpId");
            } catch (Exception e) {
                toDwzJson(300, "您提交的数据有误，请检查后重新提交！", "", "", "");
                return;
            }
        }
        List<T_Department> rootmoudles = T_Department.dao.getpdepts();
        StringBuffer buffer = new StringBuffer("");
        if (null != rootmoudles && 0 < rootmoudles.size()) {
            buffer.append("<ul>");
            for (T_Department moudle : rootmoudles) {
                buffer.append("<li><a href=\"Main/QxDept/main/list/" + moudle.getInt("id") + "\" target=\"ajax\" rel=\"" + navTabId + "\">" + moudle.getStr("fname") + "</a>");
                intoMoudles(moudle, buffer);
                buffer.append("</li>");
            }
            buffer.append("</ul>");
        }
        setAttr("moudles", buffer.substring(0));
        setAttr("mpId", mpId);
        render("dept_main.jsp");
    }

    public void intoMoudles(T_Department moudle, StringBuffer buffer) {
        if (null != moudle.getdepts() && 0 < moudle.getdepts().size()) {
            buffer.append("<ul>");
            for (T_Department smoudle : moudle.getdepts()) {
                buffer.append("<li><a href=\"Main/QxDept/main/list/" + smoudle.getInt("id") + "\" target=\"ajax\" rel=\"" + navTabId + "\">" + smoudle.getStr("fname") + "</a>");
                intoMoudles(smoudle, buffer);
                buffer.append("</li>");
            }
            buffer.append("</ul>");
        }
    }

    @ActionKey("/Main/QxDept/main/list")
    public void list() {
        Integer moudleId;
        try {
            moudleId = getParaToInt();
        } catch (Exception e) {
            toDwzJson(300, "您提交的数据有误，请检查后重新提交！", "", "", "");
            return;
        }
        if (0 > moudleId) {
            toDwzJson(300, "您提交的数据有误，请检查后重新提交！", "", "", "");
            return;
        }
        if (0 == moudleId) {
            List<T_Department> moudles = new T_Department().getAllDepts();
            setAttr("moudles", moudles);
        } else {
            T_Department moudle = T_Department.dao.findById(moudleId);
            if (null == moudle) {
                toDwzJson(300, "您提交的数据有误，请检查后重新提交！", "", "", "");
                return;
            }
            if (!"0".equals(moudle.getStr("dbstate"))) {
                toDwzJson(300, "您提交的数据有误，该模块功能已禁用！", "", "", "");
                return;
            }
            List<T_Department> moudles = moudle.getdepts();
            setAttr("moudle", moudle);
            setAttr("moudles", moudles);
        }
        setAttr("moudleId", moudleId);
        render("dept_list.jsp");
    }

    public void addip() {
        int organId;
        try {
            organId = getParaToInt();
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        if (0 != organId) {
            T_Department organ = T_Department.dao.findById(organId);
            if (null == organ) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，部门信息不存在！\"}");
                return;
            }
            setAttr("organ", organ);
        }
        setAttr("organId", organId);
        render("dept_add.jsp");
    }

    @Before(DeptValidator.class)
    public void add() {
        int organId;
        try {
            organId = getParaToInt();
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        T_Department organnew = new T_Department();
        if (0 != organId) {
            T_Department organ = T_Department.dao.findById(organId);
            if (null == organ) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，部门信息不存在！\"}");
                return;
            }
            organnew.set("d_pid", organId);
        }
        if (organId == 0) {
            organId = 1;
        }
        organnew.set("dbstate", getPara("dbstate")).set("dlvl", getPara("dlvl")).set("fname", getPara("fname").trim()).set("sname", getPara("sname").trim()).set("d_pid", organId);
        // System.out.println("organId : " + organId);
        try {
            if (organnew.save()) {
                toDwzJson(200, "部门信息添加完成！", "deptmg", "", "closeCurrent");
            } else {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
            }
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
        }
    }

    public void updateip() {
        int organId;
        try {
            organId = getParaToInt();
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        if (0 == organId) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }

        T_Department organ = T_Department.dao.findById(organId);
        if (null == organ) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，部门信息不存在！\"}");
            return;
        }
        setAttr("organ", organ);
        render("dept_update.jsp");
    }

    @Before(DeptValidator.class)
    public void update() {
        if (null == getPara("id") || "".equals(getPara("id").trim())) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        int organId;
        try {
            organId = getParaToInt("id");
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        if (0 == organId) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        T_Department organ = T_Department.dao.findById(organId);
        if (null == organ) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，部门信息不存在！\"}");
            return;
        }
        if (null == organ.getInt("d_pID")) {
            organId = 0;
        } else {
            organId = organ.getInt("d_pID");
        }
        organ.set("dbstate", getPara("dbstate")).set("dlvl", getPara("dlvl")).set("fname", getPara("fname").trim()).set("sname", getPara("sname").trim());
        try {
            if (organ.update()) {
                toDwzJson(200, "部门信息修改完成！", "deptmg", "", "closeCurrent");
            } else {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
            }
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
        }
    }

    public void delete() {
        int organId;
        try {
            organId = getParaToInt();
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        if (0 == organId) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }

        T_Department organ = T_Department.dao.findById(organId);
        if (null == organ) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，部门信息不存在！\"}");
            return;
        }
        T_Department organ_sum = T_Department.dao.findFirst("select * from t_department where d_pid=" + organId);
        if (null != organ_sum) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"部门信息已经被引用，部门信息不能够删除！\"}");
            return;
        }
        try {
            if (organ.delete()) {
                toDwzJson(200, "部门信息删除成功！", "deptmg", "", "");
            } else {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"部门信息已经被引用或者数据错误，部门信息删除失败！\"}");
            }
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"部门信息已经被引用或者数据错误，部门信息删除失败！\"}");
        }
    }
}
