package com.base.controller.authority;

import java.util.List;

import com.base.config.Constant;
import com.base.model.authority.T_Position;
import com.base.model.authority.T_User;
import com.jfinal.plugin.activerecord.Page;
import com.zh_new.annotation.RouteBind;
import com.zh_new.controller.BaseController;

@RouteBind(path = "/Main/QxPosition", viewPath = "System/Authority/Position")
public class PositionController extends BaseController {

	public void main() {
		String select = "select id, pid, pname, status";
		int pageSize = Constant.PAGE_SIZE;
		int pageNumber = 1;
		try {
			if (null != getPara("numPerPage") && !"".equals(getPara("numPerPage").trim())) {
				pageSize = getParaToInt("numPerPage");
			}
			if (pageSize <= 0) {
				throw new Exception();
			}
			if (null != getPara("pageNum") && !"".equals(getPara("pageNum").trim())) {
				pageNumber = getParaToInt("pageNum");
			}
			if (pageNumber <= 0) {
				throw new Exception();
			}
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的查询参数有误，请检查后重新提交！\"}");
			return;
		}
		String sqlExceptSelect = "from T_Position  order by pid";
		try {
			Page<T_Position> positionPage = T_Position.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
			setAttr("positionPage", positionPage);
			render("list.jsp");
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"服务器存在错误，数据读取失败！\"}");
		}
	}

	public void addip() {
		render("add.jsp");
	}

	public void add() {
		int pid;
		try {
			pid = getParaToInt("t_Position.pid");
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", \"message\":\"您提交的数据有误，请检查后重新提交2！\"}");
			return;
		}
		List<T_Position> position_id = T_Position.dao.find("select * from T_Position where pid='" + pid + "'");
		if (null != position_id && 0 < position_id.size()) {
			renderJson("{\"statusCode\":\"300\", \"message\":\"您提交的数据有误，该职位序号已经存在！\"}");
			return;
		}
		List<T_Position> position_name = T_Position.dao.find("select * from T_Position where pname='" + getPara("t_Position.pname").trim() + "'");
		if (null != position_name && 0 < position_name.size()) {
			renderJson("{\"statusCode\":\"300\", \"message\":\"您提交的数据有误，该职位名称已经存在！\"}");
			return;
		}
		try {
			T_Position position = getModel(T_Position.class);
			if (position.save()) {
				renderJson("{\"statusCode\":\"200\", \"message\":\"职位信息添加完成！\", " + "\"navTabId\":\"positionmg\", \"forwardUrl\":\"\", " + "\"callbackType\":\"closeCurrent\"}");
			} else {
				renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交4！\"}");
			}
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交5！\"}");
		}
	}

	public void updateip() {
		Integer id;
		try {
			id = getParaToInt();
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！23\"}");
			return;
		}
		T_Position position = T_Position.dao.findFirst("select * from T_Position where id=" + id);
		if (null == position) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，职位信息不存在！\"}");
			return;
		}
		setAttr("position", position);
		render("update.jsp");
	}

	public void update() {
		int id = 0;
		try {
			id = getParaToInt("t_Position.id");
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", \"message\":\"您提交的数据有误，无法获取职位id，请检查后重新提交！\"}");
			return;
		}
		T_Position position1 = T_Position.dao.findById(id);
		if (null == position1) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，职位信息不存在！\"}");
			return;
		}
		/**
		 * 查询职位序号是否重复
		 */
		if (!position1.getStr("pid").equals(getPara("t_Position.pid"))) {
			List<T_Position> position_pid = T_Position.dao.find("select * from T_Position where pid='" + getPara("t_Position.pid") + "'");
			if (null != position_pid && 0 < position_pid.size()) {
				renderJson("{\"statusCode\":\"300\", \"message\":\"您提交的数据有误，职位序号已经存在！\"}");
				return;
			}
		}
		/**
		 * 查询职位名称是否重复
		 */
		if (!position1.getStr("pname").equals(getPara("t_Position.pname"))) {
			List<T_Position> position_name = T_Position.dao.find("select * from T_Position where pname='" + getPara("t_Position.pname") + "'");
			if (null != position_name && 0 < position_name.size()) {
				renderJson("{\"statusCode\":\"300\", \"message\":\"您提交的数据有误，职位名称已经存在！\"}");
				return;
			}
		}
		try {
			T_Position position = getModel(T_Position.class);
			if (position.update()) {
				renderJson("{\"statusCode\":\"200\", \"message\":\"职位信息修改完成！\", " + "\"navTabId\":\"positionmg\", \"forwardUrl\":\"\", " + "\"callbackType\":\"closeCurrent\"}");
			} else {
				renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交1！\"}");
			}
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交2！\"}");
		}
	}

	public void del() {
		Integer id = getParaToInt();
		T_Position position = T_Position.dao.findById(id);
		List<T_User> user = T_User.dao.find("select * from t_user where pid=" + id);
		if (null != user && 0 < user.size()) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"职位信息已经被用户引用，不能够删除！\"}");
			return;
		}
		try {
			if (position.delete()) {
				renderJson("{\"statusCode\":\"200\", \"message\":\"职位信息删除完成！\", " + "\"navTabId\":\"positionmg\", \"forwardUrl\":\"\"}");
			} else {
				renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
			}
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
		}
	}

}
