package com.base.controller.log;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.base.config.Constant;
import com.base.model.log.T_Loginlog;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.zh_new.annotation.RouteBind;

@RouteBind(path = "/Main/Loglogin", viewPath = "System/Log/Loginlog")
public class LoginLogController extends Controller {
	
	public void main(){
		String select = "select t.ip, u.dlid, u.name, t.dltime, t.loDate, t.dlms";
		int pageSize = Constant.PAGE_SIZE;
		int pageNumber = 1;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String sdate = "";
		String edate = "";
		try {
			Date start = null, end = null;
			if (null!=getPara("sdate") && !"".equals(getPara("sdate").trim())) {
				start = format.parse(getPara("sdate").trim());
				sdate = getPara("sdate").trim();
			}
			if (null!=getPara("edate") && !"".equals(getPara("edate").trim())) {
				end = format.parse(getPara("edate").trim());
				edate = getPara("edate").trim();
			}
			if (null!=start && null!=end && start.after(end) ) {
				throw new Exception();
			}
			if (null!=getPara("numPerPage") && !"".equals(getPara("numPerPage").trim())) {
				pageSize = getParaToInt("numPerPage");
			}
			if (0 >= pageSize) {
				throw new Exception();
			}
			if (null!=getPara("pageNum") && !"".equals(getPara("pageNum").trim())) {
				pageNumber = getParaToInt("pageNum");
			}
			if (0 >= pageNumber) {
				throw new Exception();
			}
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的查询参数有误，请检查后重新提交！\"}");
			return;
		}
		String sqlExceptSelect = "from t_loginlog t left join t_user u on t.u_id=u.id where 1=1 ";
		if (!"".equals(sdate)) {
			sqlExceptSelect +=" and t.dltime>='" + sdate + " 00:00:00'";
			setAttr("sdate", sdate);
		} 
		if (!"".equals(edate)) {
			sqlExceptSelect +=" and t.dltime<='" + edate + " 59:59:59'";
			setAttr("edate", edate);
		} 
		
		sqlExceptSelect +=" order by t.dltime desc";
		try {
			Page<T_Loginlog> dljlPage = T_Loginlog.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
			setAttr("dljlPage", dljlPage);
			render("login_main.jsp");
		} catch (Exception e) {
			renderJson("{\"statusCode\":\"300\", " + "\"message\":\"服务器存在错误，数据读取失败！\"}");
		}
	}
	
}
