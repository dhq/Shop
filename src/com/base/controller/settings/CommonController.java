package com.base.controller.settings;

import java.util.List;

import com.base.config.Constant;
import com.base.model.settings.T_Common;
import com.base.model.settings.T_Common_Detail;
import com.jfinal.core.ActionKey;
import com.zh_new.annotation.RouteBind;
import com.zh_new.controller.BaseController;

@RouteBind(path = "Main/Common", viewPath = "System/Settings/Common")
public class CommonController extends BaseController {

	private String boxID = "common_Box";
	private String menuId = "common";

	public void main() {
		Integer cid = 1;
		if (null != getPara("cid")) {
			try {
				cid = getParaToInt("cid");
			} catch (Exception e) {
				toDwzJson(300, Constant.EXCEPTION, "", "", "");
				return;
			}
		}
		List<T_Common> commons = T_Common.dao.getNoFirstList();
		StringBuffer buffer = new StringBuffer("");
		if (null != commons && 0 < commons.size()) {
			buffer.append("<ul>");
			for (T_Common common : commons) {
				buffer.append("<li><a href=\"Main/Common/main/list/" + common.getInt("id") + "\" target=\"ajax\" rel=\"" + boxID + "\">" + common.getStr("name") + "</a></li>");
			}
			buffer.append("</ul>");
		}
		setAttr("commons", buffer.substring(0));
		setAttr("cid", cid);
		setAttr("boxID", boxID);
		render("main.jsp");
	}

	@ActionKey("/Main/Common/main/list")
	public void list() {
		Integer cid;
		try {
			cid = getParaToInt(0);
		} catch (Exception e) {
			toDwzJson(300, Constant.EXCEPTION, "", "", "");
			return;
		}
		setAttr("boxID", boxID);
		if (1 == cid) {
			setAttr("commons", T_Common.dao.getNoFirstList());
			render("dir_list.jsp");
		} else {
			T_Common common = T_Common.dao.findById(cid);
			if (null == common) {
				toDwzJson(300, Constant.EXCEPTION, "", "", "");
				return;
			}
			if (common.getInt("status") == 0) {
				toDwzJson(300, Constant.DISABLE, "", "", "");
				return;
			}
			setAttr("details", T_Common_Detail.dao.getListByCid(cid));
			setAttr("common", common);
			render("detail_list.jsp");
		}
	}

	/*********************** 公共信息详细信息 ************************/
	/** 添加 */
	public void addip() {
		int cid = getParaToInt(0);
		T_Common common = T_Common.dao.findById(cid);
		setAttr("common", common);
		render("detail_add.jsp");
	}

	public void add() {
		T_Common_Detail model = getModel(T_Common_Detail.class);
		model.save();
		toDwzJson(200, Constant.SUCCESS, "", boxID, "closeCurrent");
	}

	/** 修改 */
	public void updateip() {
		Integer id = getParaToInt();
		T_Common_Detail common = T_Common_Detail.dao.findById(id);
		String commonname = (String) T_Common.dao.findById(common.getInt("cid")).getStr("name");
		setAttr("common", common);
		setAttr("commonname", commonname);
		render("detail_update.jsp");
	}

	public void update() {
		T_Common_Detail model = getModel(T_Common_Detail.class);
		model.update();
		toDwzJson(200, Constant.SUCCESS, "", boxID, "closeCurrent");
	}

	/** 删除 */
	public void delete() {
		Integer id = getParaToInt(0);
		T_Common_Detail.dao.deleteById(id);
		toDwzJson(200, Constant.SUCCESS, "", boxID, "");
	}

	/******************* 公共信息类型 *************/
	public void dir_addip() {
		render("dir_add.jsp");
	}

	public void dir_add() {
		T_Common model = getModel(T_Common.class);
		model.save();
		toDwzJson(200, Constant.SUCCESS, menuId, "", "closeCurrent");
	}

	public void dir_updateip() {
		Integer id = getParaToInt();
		T_Common common = T_Common.dao.findById(id);
		setAttr("common", common);
		render("dir_update.jsp");
	}

	public void dir_update() {
		T_Common model = getModel(T_Common.class);
		model.update();
		toDwzJson(200, Constant.SUCCESS, menuId, "", "closeCurrent");
	}

	public void dir_delete() {
		Integer id = getParaToInt(0);
		T_Common.dao.deleteById(id);
		toDwzJson(200, Constant.SUCCESS, menuId, "", "");
	}
	
}
