package com.base.controller.settings;

import com.base.model.settings.T_Moudle;
import com.base.validator.MoudleValidator;
import com.jfinal.aop.Before;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zh_new.annotation.RouteBind;

import java.util.List;

@RouteBind(path = "/Main/Moudle", viewPath = "System/Settings/Moudle")
public class MoudleController extends Controller {

    public void main() {
        Integer mpId = 0;
        if (null != getPara("mpId")) {
            try {
                mpId = getParaToInt("mpId");
            } catch (Exception e) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
                return;
            }
        }
        List<T_Moudle> rootmoudles = T_Moudle.dao.getBaseMoudles();
        StringBuffer buffer = new StringBuffer("");
        if (null != rootmoudles && 0 < rootmoudles.size()) {
            buffer.append("<ul>");
            for (T_Moudle moudle : rootmoudles) {
                buffer.append("<li><a href=\"Main/Moudle/main/list/" + moudle.getInt("m_id") + "\" target=\"ajax\" rel=\"jbsxBox\">" + moudle.getStr("name") + "</a>");
                intoMoudles(moudle, buffer);
                buffer.append("</li>");
            }
            buffer.append("</ul>");
        }
        setAttr("moudles", buffer.substring(0));
        setAttr("mpId", mpId);
        render("moudle_main.jsp");
    }

    public void intoMoudles(T_Moudle moudle, StringBuffer buffer) {
        if (null != moudle.getMoudles() && 0 < moudle.getMoudles().size()) {
            buffer.append("<ul>");
            for (T_Moudle smoudle : moudle.getMoudles()) {
                buffer.append("<li><a href=\"Main/Moudle/main/list/" + smoudle.getInt("m_id") + "\" target=\"ajax\" rel=\"jbsxBox\">" + smoudle.getStr("name") + "</a>");
                intoMoudles(smoudle, buffer);
                buffer.append("</li>");
            }
            buffer.append("</ul>");
        }
    }

    @ActionKey("/Main/Moudle/main/list")
    public void list() {
        boolean flag = false;
        Integer moudleId;
        try {
            moudleId = getParaToInt();
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        if (0 > moudleId) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        if (0 == moudleId) {
            List<T_Moudle> moudles = new T_Moudle().getAllpMoudles();
            setAttr("moudles", moudles);
        } else {
            T_Moudle moudle = T_Moudle.dao.findById(moudleId);
            if (null == moudle) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
                return;
            }
            if (!"0".equals(moudle.getStr("dbstate"))) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，该模块功能已禁用！\"}");
                return;
            }
            List<T_Moudle> moudles = moudle.getChildMoudles();
            if (moudle.getStr("address") != null && !"".equals(moudle.getStr("address"))) {
                flag = true;
            }
            setAttr("moudle", moudle);
            setAttr("moudles", moudles);
        }
        setAttr("flag", flag);
        setAttr("moudleId", moudleId);
        render("moudle_list.jsp");
    }

    public void saveupdateip() {
        Integer mpId = 0;
        if (null != getPara("mpId")) {
            try {
                mpId = getParaToInt("mpId");
            } catch (Exception e) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
                return;
            }
        }
        if (null == getPara()) {
            setAttr("mpId", mpId);
            render("moudle_add.jsp");
            return;
        }
        int moudleId;
        try {
            moudleId = getParaToInt();
        } catch (Exception e) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
            return;
        }
        T_Moudle moudle = T_Moudle.dao.findById(moudleId);
        if (null == moudle) {
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，模块信息不存在！\"}");
            return;
        }
        setAttr("md", moudle);
        render("moudle_update.jsp");
    }

    @Before(MoudleValidator.class)
    public void saveupdate() {
        Integer mId;
        if (null == getPara("m_id") || 0 == getPara("m_id").trim().length()) {
            Integer m_pid = 0;
            try {
                if (null == getParaToInt("m_pid")) {
                    throw new Exception();
                }
                m_pid = getParaToInt("m_pid");
            } catch (Exception e) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
                return;
            }
            if (0 != m_pid) {
                if (null == T_Moudle.dao.findById(m_pid)) {
                    renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
                    return;
                }
            }
            List<T_Moudle> moudles = T_Moudle.dao.find("select * from t_moudle where " + "mark='" + getPara("mark").trim() + "'");
            if (null != moudles && 0 < moudles.size()) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，模块标识已存在！\"}");
                return;
            }
            T_Moudle moudle = new T_Moudle();
            moudle.set("name", getPara("name").trim()).set("mark", getPara("mark").trim()).set("address", getPara("address")).set("orderindex", getParaToInt("orderindex"))
                    .set("dbstate", getPara("dbstate")).set("openType", getParaToInt("openType")).set("islast", "0");
            if (0 != m_pid) {
                moudle.set("m_pid", m_pid);
            }
            try {
                if (moudle.save()) {
                    renderJson("{\"statusCode\":\"200\", \"message\":\"模块信息添加完成！\", " + "\"navTabId\":\"moudle\", \"forwardUrl\":\"Main/Moudle/main?mpId=" + m_pid + "\", "
                            + "\"callbackType\":\"closeCurrent\"}");
                } else {
                    renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
                }
            } catch (Exception e) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
            }
        } else {
            try {
                mId = getParaToInt("m_id");
            } catch (Exception e) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，请检查后重新提交！\"}");
                return;
            }
            T_Moudle moudle = T_Moudle.dao.findById(mId);
            if (null == moudle) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，模块信息不存在！\"}");
                return;
            }
            List<T_Moudle> moudles = T_Moudle.dao.find("select * from t_moudle where " + "mark='" + getPara("mark").trim() + "' and m_id<>" + mId);
            if (null != moudles && 0 < moudles.size()) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，模块标识已存在！\"}");
                return;
            }
            moudle.set("name", getPara("name").trim()).set("mark", getPara("mark").trim()).set("address", getPara("address")).set("orderindex", getParaToInt("orderindex"))
                    .set("dbstate", getPara("dbstate")).set("openType", getParaToInt("openType"));
            int mpId;
            if (null != moudle.get("m_pid")) {
                mpId = moudle.getInt("m_pid");
            } else {
                mpId = 0;
            }
            try {
                if (moudle.update()) {
                    renderJson("{\"statusCode\":\"200\", \"message\":\"模块信息修改完成！\", " + "\"navTabId\":\"moudle\", \"forwardUrl\":\"Main/Moudle/main?mpId=" + mpId + "\", "
                            + "\"callbackType\":\"closeCurrent\"}");
                } else {
                    renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
                }
            } catch (Exception e) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
            }
        }
    }

    /** 添加方法 */
    @ClearInterceptor(ClearLayer.ALL)
    public void addFunc() {
        try {
            Integer id = getParaToInt(0);
            Integer flag = getParaToInt(1);
            T_Moudle moudle = T_Moudle.dao.findById(id);
            String address = moudle.getStr("address").substring(0, moudle.getStr("address").length() - 4);
            String name = moudle.getStr("name");
            String mark = moudle.getStr("mark");
            List<T_Moudle> moudles = T_Moudle.dao.find("select * from t_moudle where " + "mark='" + mark.trim() + "_add'");
            if (null != moudles && 0 < moudles.size()) {
                renderJson("{\"statusCode\":\"300\", " + "\"message\":\"您提交的数据有误，模块标识已存在！\"}");
                return;
            }
            new T_Moudle().set("name", "添加" + name).set("m_pid", id).set("address",address + "add").set("mark",  "add" + mark).set("islast", "1").set("openType",
                    0).set("orderindex", 1)
                    .set("dbstate", "0").save();
            new T_Moudle().set("name", "修改" + name).set("m_pid", id).set("address", address + "update").set("mark", "update" + mark).set("islast", "1").set("openType",
                    0).set("orderindex", 3)
                    .set("dbstate", "0").save();
            new T_Moudle().set("name",  "删除" + name).set("m_pid", id).set("address",  address + "delete").set("mark", "delete" + mark).set("islast", "1").set("openType",
                    0).set("orderindex", 5)
                    .set("dbstate", "0").save();
            if (flag == 1) {
                new T_Moudle().set("name","批量删除" + name).set("m_pid", id).set("address", address + "deletes").set("mark", "deletes" + mark).set("islast", "1").set("openType", 0)
                        .set("orderindex", 7).set("dbstate", "0").save();
            }
            renderJson("{\"statusCode\":\"200\", \"message\":\"模块信息修改完成！\", " + "\"dlid\":\"jbsxBox\", \"forwardUrl\":\"Main/Moudle/main?mpId=" + id + "\"}");
        } catch (Exception e) {
            e.printStackTrace();
            renderJson("{\"statusCode\":\"300\", " + "\"message\":\"数据处理存在错误，请检查后重新提交！\"}");
        }
    }
}
