package com.base.validator;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class UserAddValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		validateRegex("dlid", "^[A-Za-z0-9]+$", "dlidMsg", "请输入英文字母和数字组合的登录ID！");
		validateRequiredString("name", "nameMsg", "请输入用户名称！");
		validateRequiredString("pwd", "pwdMsg", "请输入用户密码！");
		validateRequiredString("qrpwd", "qrpwdMsg", "请输入确认密码！");
		validateEqualField("pwd", "qrpwd", "pwdcMsg", "请保持用户密码与确认密码一致！");
		validateRegex("usable", "(true|false)", "usableMsg", "请输入选择用户是否启用！");
		validateRegex("lo", "(1|0)", "loMsg", "请输入选择用户是否离职！");
		validateLong("district.id", "deptidMsg", "请输入所属部门信息！");	
	}

	@Override
	protected void handleError(Controller c) {
		c.renderJson("{\"statusCode\":\"300\", \"message\":\"您提交的数据有误，请检查后重新提交\"}");
	}

}
