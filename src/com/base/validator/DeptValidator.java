package com.base.validator;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;
public class DeptValidator extends Validator{
	@Override
	protected void validate(Controller c) {
		validateRequired("fname", "nameMsg", "请输入部门信息！");
		validateRequired("sname", "ucodeMsg", "请输入部门简称信息！");
	}

	@Override
	protected void handleError(Controller c) {
		c.renderJson("{\"statusCode\":\"300\", \"message\":\"您提交的数据有误，请检查后重新提交\"}");
	}
}
