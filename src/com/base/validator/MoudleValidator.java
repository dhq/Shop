package com.base.validator;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class MoudleValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		// TODO Auto-generated method stub
		validateRegex("mark", "^[A-Za-z0-9_]+$", "markMsg", "请输入英文字母和数字组合的模块标识！");
		validateRequiredString("name", "nameMsg", "请输入模块名称！");
		validateRegex("dbstate", "(0|1)", "usableMsg", "请输入选择用户是否启用！");
		validateRegex("openType", "(0|1|2)", "openTypeMsg", "请输入选择用户是否启用！");
		validateLong("orderindex", "orderindexMsg", "请输入顺序索引信息！");
	}

	@Override
	protected void handleError(Controller c) {
		// TODO Auto-generated method stub
		c.renderJson("{\"statusCode\":\"300\", \"message\":\"您提交的数据有误，请检查后重新提交\"}");
	}

}
