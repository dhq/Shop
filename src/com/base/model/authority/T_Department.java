package com.base.model.authority;

import com.jfinal.plugin.activerecord.Model;
import com.zh_new.annotation.TableBind;

import java.math.BigDecimal;
import java.util.List;

@TableBind(name = "t_department")
public class T_Department extends Model<T_Department> {
    private static final long serialVersionUID = 1L;
    public static final T_Department dao = new T_Department();

    /** 获取所有部门列表 */
    public List<T_Department> getAllDepts() {
        return dao.find("select * from t_department where dbstate='0' order by id");
    }

    /** 得到根组织 */
    public List<T_Department> getAllpdepts() {
        return dao.find("select * from t_department where d_pid = 1 order by id ");
    }

    /** 得到根组织 */
    public List<T_Department> getpdepts() {
        return dao.find("select * from t_department where d_pid = 0 and dbstate='0' order by id ");
    }

    /** 得到当前组织 */
    public List<T_Department> getdepts() {
        return dao.find("select * from t_department where d_pid=? and dbstate='0' " + " order by id ", get("id"));
    }

    /** 得到子节点 */
    public List<T_Department> getChild(BigDecimal id) {
        return T_Department.dao.find("select * from t_department where d_pid=?", id);
    }

    /** 得到子节点 */
    public List<T_Department> getChild1(Integer id) {
        return T_Department.dao.find("select * from t_department where d_pid=? and dbstate='0' ", id);
    }

    public List<T_Department> getChild() {
        return T_Department.dao.find("select * from t_department where d_pid =1 order by id");
    }

    /** 获取有人员的部门 */
    public List<T_Department> getUserDepart() {
        String sql = "SELECT * from t_department where id in(SELECT d_id from t_user)";
        return dao.find(sql);
    }

    /** 获取用户所在部门 */
    public String getUserDe(String username) {
        T_Department temp = T_Department.dao.findFirst("select t2.sname as deparment from t_user t1 LEFT JOIN t_department t2 on t1.d_id=t2.id where t1.name= '" + username + "'");
        return temp.getStr("deparment");
    }

    /**
     * 获取上级部门的ID
     *
     * @param curdept
     * @return
     */
    public static String getupDept(String curdept) {
        return dao.findFirst("select d_pid from t_department where id = ?", curdept).getStr("d_pid");
    }

    public static String getUpdeptidsByDeptid(String deparment_id) {
        String result = "";
        String tempid = "";
        try {
            tempid = getupDept(deparment_id);
            if (!tempid.equals("1") && !tempid.equals("")) {
                result = "'" + tempid + "'";
                tempid = getupDept(tempid);
                if (!tempid.equals("1") && !tempid.equals("")) {
                    result += ",'" + tempid + "'";
                    tempid = getupDept(tempid);
                    if (!tempid.equals("1") && !tempid.equals("")) {
                        result += ",'" + tempid + "'";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void getchilddept(T_Department pdept, List<T_Department> alldepts, List<String[]> deptstrs) {
        alldepts.add(pdept);
        String[] depts = new String[2];
        StringBuffer buffer = new StringBuffer("");
        for (int i = 0; i < pdept.getInt("dlvl"); i++) {
            buffer.append("---");
        }
        depts[0] = pdept.getInt("id") + "";
        depts[1] = buffer.substring(0) + pdept.getStr("sname");
        deptstrs.add(depts);
        List<T_Department> childdepts = T_Department.dao.find("select * from t_department where dbstate='0' " + "and d_pid=" + pdept.getInt("id"));
        if (null != childdepts && 0 < childdepts.size()) {
            for (T_Department dept : childdepts) {
                getchilddept(dept, alldepts, deptstrs);
            }
        }
    }

    /** 根据deptids 得到dept */
    public List<T_Department> findDepts(String deptStr) {
        String sql = "select * from t_department where id in (" + deptStr + ") and dbstate='0'";
        return dao.find(sql);
    }

    /** 根据deptids 得到deptnames */
    public String findDeptnames(String deptStr) {
        String result = "";
        String sql = "select * from t_department where id in (" + deptStr + ") and dbstate='0'";
        List<T_Department> depts = dao.find(sql);
        for (T_Department dept : depts) {
            result += "," + dept.getStr("fname");
        }
        if (!result.equals(""))
            result = result.substring(1);
        return result;
    }

}
