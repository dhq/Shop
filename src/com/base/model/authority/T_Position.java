package com.base.model.authority;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class T_Position extends Model<T_Position> {

	private static final long serialVersionUID = 1L;
	public static final T_Position dao = new T_Position();

	public List<T_Position> getAllPositions() {
		return T_Position.dao.find("select * from t_position where status='1' order by id");
	}

	public String getPositionName(Object position_id) {
		T_Position p = T_Position.dao.findById(position_id);
		return p.getStr("pname");
	}

	// 根据用户账号，查出该用户所属职位的序号
	public Integer getPositionPid(Object dlid) {
		Integer xh = null;
		T_User user = T_User.dao.findByDlid(dlid);
		if (user != null) {
			Integer pid = user.getInt("pid");
			if (pid != null) {
				xh = T_Position.dao.findById(pid).getInt("pid");
			}
		}
		return xh;
	}

	// 根据用户账号，查出该用户所属职位
	public String getPosition(Object dlid) {
		String position_name = "";
		T_User user = T_User.dao.findByDlid(dlid);
		if (user != null) {
			Integer pid = user.getInt("pid");
			if (pid != null) {
				position_name = T_Position.dao.getPositionName(pid);
			}
		}
		return position_name;
	}

	/** 根据positionids 得到position */
	public List<T_Position> findPositions(String positionStr) {
		String sql = "select * from t_position where id in (" + positionStr + ") and status='1'";
		return dao.find(sql);
	}

	/** 根据positionids 得到positionnames */
	public String findPositionnames(String positionStr) {
		String result = "";
		String sql = "select * from t_position where id in (" + positionStr + ") and status='1'";
		List<T_Position> positions = dao.find(sql);
		for (T_Position position : positions) {
			result += "," + position.getStr("pname");
		}
		if (!result.equals(""))
			result = result.substring(1);
		return result;
	}

}
