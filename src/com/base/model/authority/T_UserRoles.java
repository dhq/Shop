package com.base.model.authority;

import com.jfinal.plugin.activerecord.Model;

public class T_UserRoles extends Model<T_UserRoles> {
	
	private static final long serialVersionUID = 1L;
	public static final T_UserRoles dao = new T_UserRoles();

	public T_Role getRole() {
		return T_Role.dao.findById(getInt("r_id"));
	}

	public T_Role getUser() {
		return T_Role.dao.findById(getInt("u_id"));
	}

	public Integer getRoleID(int userid) {
		return T_UserRoles.dao.findFirst("select * from t_userroles where u_id = ?", userid).getInt("r_id");
	}
}
