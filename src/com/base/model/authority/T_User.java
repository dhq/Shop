package com.base.model.authority;

import com.base.config.Constant;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Model;
import com.pinion.util.BusinessConstant;

import java.util.HashMap;
import java.util.List;

public class T_User extends Model<T_User> {
	
	private static final long serialVersionUID = 1L;
	public static final T_User dao = new T_User();

    /** 获取所有可用的某一角色的用户列表 */
    public List<T_User> getListByRole(int r_id) {
        return dao.find("select * from t_user a, t_userroles b where usable=1 and a.id=b.u_id and r_id=?", r_id);
    }

	/** 获取所有可用的用户列表 */
	public List<T_User> getList() {
		return dao.find("select * from t_user where usable=1");
	}

	/** 用户角色列表 */
	public List<T_UserRoles> getUserRoles() {
		return T_UserRoles.dao.find("select * from t_userroles where uid=?", get("id"));
	}

	/**
	 * 获取用户权限列表（获取格式：“1,2,3...”）
	 * 
	 *            当前用户
	 * @return String
	 */
	public String getXtlimit() {
		String xtlimit = getStr("xtlimit");
		if (StrKit.notBlank(xtlimit)) {
			xtlimit = xtlimit.replace("_", "");
			xtlimit = xtlimit.substring(0, xtlimit.length() - 1);
		}
		return xtlimit;
	}

	public T_User exists(String dlid) {
		T_User user = T_User.dao.findFirst("select * from t_user where dlid='" + dlid + "'");
		if (null == user) {
			return null;
		} else {
			return user;
		}
	}

	public T_Department getDept() {
		return T_Department.dao.findById(get("d_id"));
	}

	public List<T_UserRoles> getRoles() {
		return T_UserRoles.dao.find("select * from t_userroles where u_id=" + getInt("id"));
	}

	public List<T_UserRoles> getAllRoleInfo() {
		return T_UserRoles.dao.find("select ur.id, ur.priority, r.name from t_userroles ur left join " + "t_role r on ur.r_id=r.id where ur.u_id=" + getInt("id") + " order by ur.id desc");
	}

	public List<T_Role> getSetRoles() {
		return T_Role.dao.find("select * from t_role r where r.dbstate='0' " + "and r.id not in (select ur.r_id from t_userroles " + "ur where ur.u_id=" + getInt("id") + " and ur.dbstate='0')");
	}

	public T_UserRoles getRolesUser(int userid) {
		return T_UserRoles.dao.findFirst("select * from t_userroles where u_id=" + userid);
	}

	public HashMap<String, Integer> getHashMapUser() {
		HashMap<String, Integer> hm = null;
		List<T_User> users = T_User.dao.getList();
		if (users.size() > 0) {
			hm = new HashMap<String, Integer>();
			for (T_User user : users) {
				hm.put(user.getStr("name"), user.getInt("id"));
			}
		}
		return hm;
	}

	public HashMap<Integer, T_User> getUserHm() {
		HashMap<Integer, T_User> hm = null;
		List<T_User> users = T_User.dao.getList();
		if (users.size() > 0) {
			hm = new HashMap<Integer, T_User>();
			for (T_User user : users) {
				hm.put(user.getInt("id"), user);
			}
		}
		return hm;
	}

	public HashMap<Integer, String> getUserHmName() {
		HashMap<Integer, String> hm = null;
		List<T_User> users = T_User.dao.getList();
		if (users.size() > 0) {
			hm = new HashMap<Integer, String>();
			for (T_User user : users) {
				hm.put(user.getInt("id"), user.getStr("name"));
			}
		}
		return hm;
	}

	public T_User findByDlid(Object dlid) {
		return T_User.dao.findFirst("select * from t_user where dlid=?", dlid);
	}

    public T_User findByStaffId(String staffId) {
        return T_User.dao.findFirst("SELECT * FROM t_user WHERE staff_id=?", staffId);
    }

	public List<T_User> getUserByName(String name) {
		StringBuffer sql = new StringBuffer("select * from t_user where usable=1");
		if (name != null && !"".equals(name)) {
			sql.append(" and name like '%" + name + "%'");
		}
		return dao.find(sql.toString());
	}

	public String getUserName(Object object) {
		T_User user = dao.findFirst("select * from t_user where dlid = ?", object);
		return user == null ? "" : user.getStr("name");
	}

	public String getUserNameById(Object object) {
		T_User user = dao.findFirst("select * from t_user where id = ?", object);
		return user == null ? "" : user.getStr("name");
	}

	/** 根据userids 得到user */
	public List<T_User> findUsers(String userStr) {
		String sql = "select * from t_user where id in (" + userStr + ") and usable=1";
		return dao.find(sql);
	}

	/** 根据userids 得到usernames */
	public String findUsernames(String userStr) {
		String result = "";
		String sql = "select * from t_user where id in (" + userStr + ") and usable = 1";
		List<T_User> users = dao.find(sql);
		for (T_User user : users) {
			result += "," + user.getStr("name");
		}
		if (!result.equals(""))
			result = result.substring(1);
		return result;
	}

	/**
	 * hashMapById(String value)
	 */
	public HashMap<String, String> hashMapByIds(String value) {
		List<T_User> models = getList();
		HashMap<String, String> hm = null;
		if (models != null) {
			hm = new HashMap<String, String>();
			for (T_User model : models) {
				hm.put(String.valueOf(model.getInt("id")), model.getStr(value));
			}
		}
		return hm;
	}
	
	public HashMap<Integer,String> getNameMap(){
        List<T_User> list = dao.find("select * from t_user");
        HashMap<Integer,String> map = new HashMap<Integer, String>();
        if(list.size()>0){
            for(T_User o : list){
                map.put(o.getInt("id"), o.getStr("name"));
            }
        }
        return map;
    }

    public List<T_User> getSalesmanList(){
        List<T_User> list = dao.find("select u.id,u.name from t_user u,t_userroles ur where u.id=ur.u_id AND ur.r_id ="+ BusinessConstant.ROLE_ID_SALESMAN);
        return list;
    }

    /**
     * 获取领导列表
     * @return
     */
    public List<T_User> getLeaderList(){
        String sql = "select * from t_user where pid ="+ Constant.POSITION_ID_MINISTER + " or pid ="+Constant.POSITION_ID_VICE_MINISTER
                + " ORDER BY id asc";
        return dao.find(sql);
    }

}
