package com.base.model.authority;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class T_Role extends Model<T_Role> {

	private static final long serialVersionUID = 1L;
	public static final T_Role dao = new T_Role();

	public List<T_Role> getAllRoles() {
		return T_Role.dao.find("select * from t_role where dbstate='0' order by id");
	}
}
