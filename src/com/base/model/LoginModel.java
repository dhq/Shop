package com.base.model;

import java.io.Serializable;

public class LoginModel implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer userId;
    private String dlId;
    private String userName;
    private String userStaffId;
    private String limit;
    private String dblimit;
    private String sessionId;
    private Integer departmentId;
    private String depsname;
    private String lo;
    private Integer positionId;  //职位ID
    private String roleIds;

    public String getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDlId() {
        return dlId;
    }

    public void setDlId(String dlId) {
        this.dlId = dlId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserStaffId() {
        return userStaffId;
    }

    public void setUserStaffId(String userStaffId) {
        this.userStaffId = userStaffId;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getDblimit() {
        return dblimit;
    }

    public void setDblimit(String dblimit) {
        this.dblimit = dblimit;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepsname() {
        return depsname;
    }

    public void setDepsname(String depsname) {
        this.depsname = depsname;
    }

    public String getLo() {
        return lo;
    }

    public void setLo(String lo) {
        this.lo = lo;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }
}
