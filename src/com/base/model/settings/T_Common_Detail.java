package com.base.model.settings;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class T_Common_Detail extends Model<T_Common_Detail> {

	private static final long serialVersionUID = 1L;
	public static T_Common_Detail dao = new T_Common_Detail();

	/** 根据类型ID获取业务字典列表 */
	public List<T_Common> getListByCid(Integer id) {
		String sql = "select * from t_common_detail where cid=?";
		return T_Common.dao.find(sql, id);
	}

	/** 根据remark获取业务字典列表 */
	public List<T_Common_Detail> getListByRemark(String remark) {
		String sql = "select * from t_common_detail where remark in (select id from t_common where remark=?)";
		return dao.find(sql, remark);
	}

	/** 根据T_Common的key获取 */
	public List<T_Common_Detail> getListByKey(String key) {
		String typesql = "select * from t_common t where t.key = '" + key + "'";
		String sql = "select * from t_common_detail where cid = " + T_Common.dao.findFirst(typesql).getInt("id");
		return T_Common_Detail.dao.find(sql);
	}

	/** 获取所有字典列表 */
	public List<T_Common_Detail> getList() {
		String sql = "select * from t_common_detail";
		return T_Common_Detail.dao.find(sql);
	}

}
