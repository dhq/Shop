package com.base.model.settings;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.zh_new.annotation.TableBind;

@TableBind(name = "t_moudle", pk = "m_id")
public class T_Moudle extends Model<T_Moudle> {

	private static final long serialVersionUID = 1L;
	public static final T_Moudle dao = new T_Moudle();

	public List<T_Moudle> getBaseMoudles() {
		return T_Moudle.dao.find("select * from t_moudle  where " + "m_pid is null and dbstate='0' order by orderindex");
	}

	public List<T_Moudle> getAllpMoudles() {
		return T_Moudle.dao.find("select * from t_moudle  where " + "m_pid is null order by orderindex");
	}

	public T_Moudle getT_Moudle() {
		return T_Moudle.dao.findById(get("m_pid"));
	}

	public List<T_Moudle> getMoudles() {
		return T_Moudle.dao.find("select * from t_moudle where m_pid=? and dbstate='0' " + "order by orderindex", get("m_id"));
	}

	public List<T_Moudle> getChildMoudles() {
		return T_Moudle.dao.find("select * from t_moudle where m_pid=? " + "order by orderindex", get("m_id"));
	}

	public List<T_Moudle> getAllMoudles() {
		return T_Moudle.dao.find("select * from t_moudle where dbstate='0' order by m_pid, orderindex");
	}

	/** 获取顶级模块(过滤当前用户权限) */
	public List<T_Moudle> getBaseMoudles(String xtlimit) {
		return T_Moudle.dao.find("select * from t_moudle  where " + "m_pid is null and m_id in(" + xtlimit + ") and dbstate='0' order by m_pid,orderindex");
	}

	/** 根据父级ID获取该ID下的所有模块(过滤当前用户权限) */
	public List<T_Moudle> getMoudlesByPid(Integer pid, String xtlimit) {
		return T_Moudle.dao.find("select * from t_moudle  where m_pid=? and m_id in(" + xtlimit + ")  and dbstate='0' order by m_pid, orderindex", pid);
	}

	/**
	 * 获取当前用户权限的子模块
	 * 
	 * @param moudles
	 *            上级模块列表
	 * @param user
	 *            当前用户
	 * @return List<T_Moudle>
	 */
	public List<T_Moudle> getChildMoudles(List<T_Moudle> moudles, String xtlimit) {
		StringBuffer str = new StringBuffer();
		for (T_Moudle moudle : moudles) {
			str.append(moudle.get("m_id") + ",");
		}
		str.deleteCharAt(str.length() - 1);
		return T_Moudle.dao.find("select * from t_moudle where m_pid in(" + str + ") and m_id in(" + xtlimit + ") and dbstate='0' order by m_pid, orderindex");
	}
}
