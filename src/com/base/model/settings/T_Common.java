package com.base.model.settings;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class T_Common extends Model<T_Common> {

	private static final long serialVersionUID = 1L;
	public static T_Common dao = new T_Common();

	public List<T_Common> getAllTypes() {
		return T_Common.dao.find("select * from t_common order by id");
	}

	public List<T_Common> getCommons() {
		return T_Common.dao.find("select * from t_common_detail where cid=? order by id", getInt("id"));
	}

    public List<T_Common> getNoFirstList() {
        return dao.find("select * from t_common where id <> 1");
    }

    public List<T_Common> getListByCid(String cid) {
        return dao.find("select * from t_common where cid = " + cid);
    }
}
