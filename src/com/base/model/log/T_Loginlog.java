package com.base.model.log;

import com.base.config.Constant;
import com.jfinal.plugin.activerecord.Model;
import com.zh_new.annotation.TableBind;

import java.util.Date;
import java.util.List;

@TableBind(name = "t_loginlog", pk = "sessionId")
public class T_Loginlog extends Model<T_Loginlog> {
    private static final long serialVersionUID = 1L;
    public static final T_Loginlog dao = new T_Loginlog();

    /**
     * 获取上一次登录记录
     */
    public T_Loginlog getLast(Integer userId) {
        String sql = "select * from t_loginlog where u_id=? order by dltime desc";
        List<T_Loginlog> list = dao.find(sql, userId);
        if (list.size() > 1) {
            return list.get(1);
        }
        return null;
    }

    /**
     * 根据seesionId获取记录
     *
     * @param sessionId Session_Id
     * @return T_Loginlog
     */
    public T_Loginlog getBySession(String sessionId) {
        return dao.findFirst("SELECT * FROM t_loginlog WHERE sessionid=?", sessionId);
    }

    /**
     * 判断用户是否登录超时
     *
     * @param auth_id   Session_Id
     * @param loginType 登录类型
     * @return
     */
    public boolean isLoginTimeOut(String auth_id, int loginType) {
        T_Loginlog oldLoginlog = T_Loginlog.dao.getBySession(auth_id);
        //扩展
        if (Constant.CLIENT_MB == loginType) {
            long dlTime = oldLoginlog.getDate("dltime").getTime();
            long curTime = new Date().getTime();
            //如果当前用户登录超时
            System.out.println("-->超时时间：" + (long) (Math.abs(dlTime - curTime) * 0.001));
            return (long) (Math.abs(dlTime - curTime) * 0.001) > Constant.LOGIN_TIMEOUT_MB;
        } else if (Constant.CLIENT_WEB == loginType) {
            return false;
        } else {
            return false;
        }
    }
}
