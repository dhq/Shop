package com.base.config;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.base.interception.PermitInterceptor;
//import com.jacob.com.ComThread;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.render.ViewType;
import com.zh_new.plugin.AutoTableBindPlugin;
import com.zh_new.plugin.MyRoutesUtil;
import com.zh_new.plugin.TableNameStyle;
import com.zh_new.util.DateUtils;

/**
 * API引导式配置
 */
public class JFWebConfig extends JFinalConfig {
	
	/** 配置常量 */
	public void configConstant(Constants me) {
		me.setDevMode(Constant.AUTO_RELOAD_SWITCH);
		me.setEncoding("UTF-8");
		me.setViewType(ViewType.JSP);
		me.setBaseViewPath("/WEB-INF/jsp/");
		me.setUploadedFileSaveDirectory(Constant.UPLOAD_DEFAULT_PATH);
        loadPropertyFile("config.txt");
	}

	/** 配置路由 */
	public void configRoute(Routes me) {
		MyRoutesUtil.add(me);
	}

	/** 配置插件 */
	public void configPlugin(Plugins me) {
		//loadPropertyFile("classes/config.properties");
		WallFilter wall = new WallFilter();
		wall.setDbType(getProperty("jdbc.dbType"));
		DruidPlugin druidPlugin = new DruidPlugin(getProperty("jdbc.url"), getProperty("jdbc.username"), getProperty("jdbc.password"), getProperty("jdbc.driverClassName"));
		druidPlugin.addFilter(new StatFilter());
		druidPlugin.addFilter(wall);
		me.add(druidPlugin);
		AutoTableBindPlugin autoTableBindPlugin = new AutoTableBindPlugin(druidPlugin, TableNameStyle.LOWER);
		autoTableBindPlugin.setShowSql(getPropertyToBoolean("jdbc.sql"));
		autoTableBindPlugin.setContainerFactory(new CaseInsensitiveContainerFactory());
		me.add(autoTableBindPlugin);
	}

	/** 配置全局拦截器 */
	public void configInterceptor(Interceptors me) {
		me.add(new PermitInterceptor());
	}

	/** 配置处理器 */
	public void configHandler(Handlers me) {
		DruidStatViewHandler dvh = new DruidStatViewHandler("/druid");
		me.add(dvh);
	}

	/** 系统启动后回调 */
	public void afterJFinalStart(){
		//ComThread.startMainSTA();//初始化word主线程
		loadPropertyFile("parameter.txt");
		Constant.MY_MESSAGE_TIPS = getProperty("my_message_tips");
		Constant.PROJECT_NAME = getProperty("project_name");

		Constant.SYSTEM_TITLE = getProperty("system_title");
		Constant.SYSTEM_VERSION_YEAR = getProperty("system_version_year");
		Constant.SYSTEM_VERSION_COMPANY = getProperty("system_version_company");
		Constant.SYSTEM_COMPANY_WEBSITE = getProperty("system_company_website");

		Constant.REPORT_TIME_HOUR = getPropertyToInt("report_time_hour");
		Constant.REPORT_AUTO_SWITCH = getPropertyToBoolean("report_auto_switch");

		Constant.AUTO_RELOAD_SWITCH = getPropertyToBoolean("auto_reload_switch");
		Constant.ACTION_REPORT_SWITCH = getPropertyToBoolean("action_report_switch");

		Constant.SEVER_MACHINE_ID = getPropertyToInt("sever_machine_id");
	}

	/** 系统关闭前回调 */
	public void beforeJFinalStop(){
		//ComThread.quitMainSTA();//关闭word主线程
		System.out.println("系统准备关闭……  时间："+DateUtils.getNowTime());
	}

}
