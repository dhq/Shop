/**
 * 常量
 * <br>
 * 常量：1.配置文件初始化 2.默认初始化<br>
 * 普通常量：JFinalConfig.afterJFinalStart()，实现配置文件初始化<br>
 * 服务器常量：LoginController.login()，实现配置文件初始化<br>
 * SystemInit类，实现页面调用常量初始化。
 */
package com.base.config;

import com.jfinal.kit.PathKit;

import java.io.File;

public class Constant {

    public final static String CONFIG_FILE_NAME = "config.txt";


    /**
     * 角色
     */
    public static int SUPER_USER = 1;
    public static int ADMIN_USER = 3;
    public static int SELLER_USER = 2;

    /**
     * 服务器
     */
    public static String SERVER_NAME = "";
    public static String SERVER_PORT = "";

    /**
     * 项目名称
     */
    public static String PROJECT_NAME = "";

    /**
     * 系统版本信息
     */
    public static String SYSTEM_TITLE = "";
    public static String SYSTEM_VERSION_YEAR = "";
    public static String SYSTEM_VERSION_COMPANY = "";
    public static String SYSTEM_COMPANY_WEBSITE = "";

    /**
     * dwz状态返回码
     */
    public final static int STATUS_CODE_OK = 200;
    public final static int STATUS_CODE_ERROR = 300;
    public final static int STATUS_CODE_TIME_OUT = 301;


    /**
     * 相关路径
     */
    public static String PATH_WEBROOT = PathKit.getWebRootPath();
    public static String PATH_TEMPLATE_UPLOAD = "Template/";
    public static String PATH_TEMPLATE = "/File/Template/";
    public static String PATH_SIGNATURE_UPLOAD = "Signature/";
    public static String PATH_SIGNATURE = "/File/Signature/";
    public static String PATH_EMAIL_UPLOAD = "Email/";
    public static String PATH_EMAIL = "/File/Email/";
    public static String PATH_FILE_UPLOAD = "File/";
    public static String PATH_FILE = "/File/File/";
    public static String PATH_FILE_ATTACH = "Attach/";
    public static String PATH_ATTACH = "/File/Attach/";
    public static String PATH_DOCRECEIVE_UPLOAD = "DocReceive/";
    public static String PATH_DOCRECEIVE = "/File/DocReceive/";
    public static String PATH_DOCSEND_UPLOAD = "DocSend/";
    public static String PATH_DOCSEND = "/File/DocSend/";
    public static String PATH_PDF = "/File/Pdf/";
    public static String PATH_WORD = "/File/Work/";
    public static String PATH_EXCEL = "/File/Excel/";
    public static String PATH_REPORT = "/File/Report/";
    public static String PATH_IMAGE = "/File/Image/";
    public static String PATH_COMMON = "Common/";
    public static String PATH_SHOW = "";
    public static String PATH_HEADICON_UPLOAD = "Employee/HeadIcon/";
    public static String PATH_HEADICON = "/File/Employee/HeadIcon/";
    public static String PATH_CERTIFICATE_UPLOAD = "Employee/Certificate/";
    public static String PATH_CERTIFICATE = "/File/Employee/Certificate/";

    /**
     * 分页
     */
    public static Integer PAGE_SIZE = 20;
    public static Integer RUN_TIME = 30 * 60;

    /**
     * 上传
     */
    public static Integer MAX_POST_SIZE = 100 * 1024 * 1024;
    public static Integer MAX_IMAGES_UPLOAD_SIZE = 1024 * 1024 * 1024;

    /**
     * 上传失败类型
     */
    public static final String ERROR_TYPE_BG = "bg";    //批量商品
    public static final String ERROR_TYPE_BI = "bi";    //批量图片

    /**
     * 设置默认上传路径
     */
    public static String UPLOAD_DEFAULT_PATH = PathKit.getWebRootPath() + File.separator + "File" + File.separator;
    public static String UPLOAD_IMAGESZIP_PATH = PathKit.getWebRootPath() + File.separator + "File" + File.separator + "images" + File.separator + "zip" + File.separator;
    public static String UPLOAD_IMAGES_PATH = PathKit.getWebRootPath() + File.separator + "File" + File.separator + "images" + File.separator;

    /**
     * 任务管理
     */
    public static Integer REPORT_TIME_HOUR = null;
    public static boolean REPORT_AUTO_SWITCH = true;
    public static Integer REPORT_TASK = 0;

    /**
     * 显示提示
     */
    public static String MY_MESSAGE_TIPS = "";
    public final static String SUCCESS = "操作成功";
    public final static String FAILURE = "操作失败";
    public final static String DISABLE = "该功能已禁用";
    public final static String EXCEPTION = "数据处理存在错误，请检查后重新提交！！";
    public final static String REFERENCE = "数据被引用！";

    /**
     * 系统信息开关显示
     */
    public static boolean AUTO_RELOAD_SWITCH = true;
    public static boolean ACTION_REPORT_SWITCH = false;

    //集群机器ID
    public static int SEVER_MACHINE_ID = 1;

    /**
     * 文件类型
     */
    public final static String DOC = ".doc";
    public final static String PDF = ".pdf";
    public final static String XLS = ".xls";
    public final static String ZIP = ".zip";
    public final static String TXT = ".txt";

    public final static String DATE_PATTERN_NO_YEAR = "MM-dd HH:mm";   //日期时间格式
    public final static String DATE_PATTERN_LONG = "yyyy-MM-dd HH:mm:ss";   //日期时间格式
    public final static String DATE_PATTERN_MINUTE = "yyyy-MM-dd HH:mm";   //日期时间格式
    public final static String DATE_PATTERN_SHORT = "yyyy-MM-dd";          //日期格式
    public final static String DATE_PATTERN_SHORT_NO_RAIL = "yyyyMMdd";          //日期不带横杆
    public final static String DATE_PATTERN_MONTH = "yyyy-MM";             //月份格式
    public final static String TIME_PATTERN = "HH:mm:ss";

    public static final long INTERVAL_SECOND = 1000;    //一秒对应的秒数
    public static final long INTERVAL_MINUTE = 60000;   //一分钟对应的秒数
    public static final long INTERVAL_HOUR = 3600000;   //一小时对应的秒数
    public static final long INTERVAL_DAY = 86400000;   //一个天对应的秒数
    public static final long INTERVAL_WEEK = 604800000; //一个星期对应的秒数

    public static final int POSITION_ID_MINISTER = 1;           //部长职位ID
    public static final int POSITION_ID_VICE_MINISTER = 3;      //副部长职位ID

    //`status` int(2) DEFAULT '1' COMMENT '订单状态,1：未提交、2：已提交、3：已发货、4：已收货、5：已完成',
    public final static int ORDER_STATUS_UNCOMMITTED = 1;
    public final static int ORDER_STATUS_COMMITTED = 2;
    public final static int ORDER_STATUS_DELIVERED = 3;
    public final static int ORDER_STATUS_RECEIVED = 4;
    public final static int ORDER_STATUS_FINISHED = 5;

    //更新方式
    public static final int UPDATE_ALL = 0;
    public static final int UPDATE_COMMODITIES = 1;
    public static final int UPDATE_PICTURE = 2;
    public static final int UPDATE_CLASSIFICATION = 3;
    public static final int UPDATE_BRAND = 4;
    public static final int UPDATE_REGION_PRICE = 5;
    public static final int UPDATE_ORDER = 6;
    public static final int UPDATE_ORDER_ITEM = 7;
    public static final int UPDATE_REGION_CODE = 8;
    public static final int UPDATE_CLASSIFICATION_PICTURE = 9;
    public static final int UPDATE_SPRECIAL_PICTURE = 10;

    //登录超时时间
    public static final long LOGIN_TIMEOUT_MB = 60 * 30;
    public static final long LOGIN_TIMEOUT_WEB = 60 * 60 * 24;

    //客户端
    public static final int CLIENT_MB = 1;
    public static final int CLIENT_WEB = 2;

}

