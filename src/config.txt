##-------------------------------------------------
## MYSQL
##-------------------------------------------------
jdbc.driverClassName=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://127.0.0.1:3306/pinionmxshop?characterEncoding=utf-8&amp;autoReconnect=true
jdbc.username=root
jdbc.password=root
jdbc.dbType=mysql
jdbc.sql=false



devMode = true

# 是否对消息进行加密，是否对消息进行加密，对应于微信平台的消息加解密方式，false支持明文模式及兼容模式，true支持安全模式及兼容模式
encryptMessage=true
encodingAesKey=RlJm6TAsnumcpqObZAg4Bw9ykgx73lh2SrP3Dqqhh0Z