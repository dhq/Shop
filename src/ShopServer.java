import com.jfinal.core.JFinal;
import com.jfinal.kit.PathKit;

import java.io.File;

/**
 * 服务器启动管理 （该类仅做开发使用）
 */
public class ShopServer {
	public static void main(String[] args) {
		String webAppDir = PathKit.getWebRootPath().replace(File.separator, "/");
		try {
			JFinal.start(webAppDir, 8088, "/", 3);
		} catch (Exception e) { 
			e.printStackTrace(); 
		}
	}
}